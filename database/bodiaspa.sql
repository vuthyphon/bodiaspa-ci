-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 18, 2017 at 06:55 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bodiaspa`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ADD_CHSERVICE`(`PBRANCH_ID` INT(11), `PCHECKIN_ID` VARCHAR(255) CHARACTER SET UTF8, `PSERVICE_ID` INT(11), `PUNITPRICE` FLOAT, `PAMT` FLOAT)
BEGIN

  DECLARE LV_AUTO_SERVICE_ID VARCHAR(255);
  SET LV_AUTO_SERVICE_ID=CONCAT(YEAR(CURDATE()),'-',MONTH(CURDATE()),'-',DAY(CURDATE()),'-',HOUR(CURTIME()),'-',MINUTE(CURTIME()),'-',SECOND(CURTIME()),'-',PBRANCH_ID);

  IF NOT EXISTS(SELECT SERVICE_ID FROM spa_checkin_service WHERE CHECKIN_ID=PCHECKIN_ID AND SERVICE_ID=PSERVICE_ID) THEN
    INSERT INTO spa_checkin_service
    (
     	CHKSV_ID,
      CHECKIN_ID,
      SERVICE_ID,
      UNIT_PRICE,
      AMOUNT
    )
    VALUES
    (
      LV_AUTO_SERVICE_ID,
      PCHECKIN_ID,
      PSERVICE_ID,
      PUNITPRICE,
      PAMT
    );
  ELSE
  
  UPDATE spa_checkin_service
    SET AMOUNT=PAMT,
        UNIT_PRICE=PUNITPRICE
  WHERE CHECKIN_ID = PCHECKIN_ID
  AND SERVICE_ID = PSERVICE_ID;
  
  END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_CHECKIN`(`PBOOKING_ID` VARCHAR(255) CHARACTER SET UTF8, `PDATE_IN` DATE, `PTIME_IN` DATETIME, `PROOM_ID` INT(11), `PTHERAPIST` INT(11), `PBRANCH_ID` INT(11))
BEGIN
DECLARE LV_AUTO_ID VARCHAR(255);

SET LV_AUTO_ID=CONCAT(YEAR(CURDATE()),'-',MONTH(CURDATE()),'-',DAY(CURDATE()),'-',HOUR(CURTIME()),'-',MINUTE(CURTIME()),'-',SECOND(CURTIME()),'-',PBRANCH_ID);
IF EXISTS(SELECT * FROM spa_booking WHERE BOOKING_ID=PBOOKING_ID) THEN
  INSERT INTO spa_checkin
  (
    CHECKIN_ID,
    BOOKING_ID,
    DATE_IN,
    TIME_IN,
    ROOM_ID,
    THERAPIST
  )
  VALUES
  (
    LV_AUTO_ID,
    PBOOKING_ID,
    PDATE_IN,
    PTIME_IN,
    PROOM_ID,
    PTHERAPIST
  );
END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_CHECKOUT`(`PBRANCH_ID` INT(11), `PCHECKIN_ID` VARCHAR(255) CHARACTER SET UTF8, `PEXCHANGE_RATE` FLOAT, `PDISCOUNT` FLOAT, `PTOTAL` FLOAT)
BEGIN

  DECLARE LV_AUTO_CHECKOUT_ID VARCHAR(255);
  SET LV_AUTO_CHECKOUT_ID=CONCAT(YEAR(CURDATE()),'-',MONTH(CURDATE()),'-',DAY(CURDATE()),'-',HOUR(CURTIME()),'-',MINUTE(CURTIME()),'-',SECOND(CURTIME()),'-',PBRANCH_ID);

    INSERT INTO spa_checkout
    (
     	CHECKOUT_ID,
      CHECKIN_ID,
      EXCHANGE_RATE,
      DISCOUNT,
      GRAND_TOTAL
    )
    VALUES
    (
      LV_AUTO_CHECKOUT_ID,
      PCHECKIN_ID,
      PEXCHANGE_RATE,
      PDISCOUNT,
      PTOTAL
    );
   


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_USRNB_ASSIGN`(`PUS_ID` VARCHAR(255) CHARACTER SET UTF8, `PBRANCH_ID` INT(11), `PUSNAME` VARCHAR(255) CHARACTER SET UTF8)
BEGIN


IF(NOT EXISTS(SELECT * FROM spa_usrnbr WHERE US_ID = PUS_ID)) THEN
  INSERT INTO spa_usrnbr(US_ID, BRANCH_ID, ASSIGNED_DATE, ASSIGNED_BY)
  VALUES(PUS_ID, PBRANCH_ID, NOW(), PUSNAME);
ELSE
  UPDATE spa_usrnbr
    SET BRANCH_ID = PBRANCH_ID,
        ASSIGNED_DATE = NOW(),
        ASSIGNED_BY = PUSNAME
    WHERE US_ID = PUS_ID;
END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_USRNR_ASSIGN`(`PUS_ID` VARCHAR(255) CHARACTER SET UTF8, `PRULE_ID` INT(11), `PUSNAME` VARCHAR(255) CHARACTER SET UTF8)
BEGIN


IF(NOT EXISTS(SELECT * FROM spa_usrnrule WHERE US_ID = PUS_ID)) THEN
  INSERT INTO spa_usrnrule(US_ID, RULE_ID, ASSIGNED_DATE, ASSIGNED_BY)
  VALUES(PUS_ID, PRULE_ID, NOW(), PUSNAME);
ELSE
  UPDATE spa_usrnrule
    SET RULE_ID = PRULE_ID,
        ASSIGNED_DATE = NOW(),
        ASSIGNED_BY = PUSNAME
    WHERE US_ID = PUS_ID;
END IF;

END$$

--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `FN_SPA_BOOKING`(`PCUS_NAME` VARCHAR(255) CHARACTER SET UTF8, `PGENDER` VARCHAR(1) CHARACTER SET UTF8, `PTEL` VARCHAR(11) CHARACTER SET UTF8, `PAGENCY_ID` INT(11), `PNUM_OF_PEX` INT(11), `PDATE_IN` DATE, `PTIME_IN` TIME, `PHOUR` FLOAT, `PROOM_ID` INT(11), `PTHERAPIST` INT(11), `PTUK` INT(11), `PENT_TYPE` VARCHAR(10) CHARACTER SET UTF8, `PBRANCH_ID` INT(11)) RETURNS varchar(255) CHARSET utf8
BEGIN

DECLARE LV_AUTO_ID VARCHAR(255);

SET LV_AUTO_ID=CONCAT(YEAR(CURDATE()),'-',MONTH(CURDATE()),'-',DAY(CURDATE()),'-',HOUR(CURTIME()),'-',MINUTE(CURTIME()),'-',SECOND(CURTIME()),'-',PBRANCH_ID);

INSERT INTO spa_booking
(
  BOOKING_ID,
  CUS_NAME,
  GENDER,
  TEL,
  AGENCY,
  NUM_OF_PEX,
  DATE_BOOKING,
  DATE_IN,
  TIME_IN,
  TOTAL_HOUR,
  TMP_ROOM_ID,
  THERAPIST,
  TUKTUK,
  ENT_TYPE,
  BRANCH_ID
)
VALUES
(
  LV_AUTO_ID,
  PCUS_NAME,
  PGENDER,
  PTEL,
  PAGENCY_ID,
  PNUM_OF_PEX,
  NOW(),
  PDATE_IN,
  PTIME_IN,
  PHOUR,
  PROOM_ID,
  PTHERAPIST,
  PTUK,
  PENT_TYPE,
  PBRANCH_ID
);

RETURN LV_AUTO_ID; 

END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `FN_SPA_USER_INSERT`(`PFULL_NAME` VARCHAR(255) CHARACTER SET UTF8, `PSTAFF_ID` VARCHAR(255) CHARACTER SET UTF8, `PLOGINNAME` VARCHAR(255) CHARACTER SET UTF8, `PUS_PASSWORD` VARCHAR(255) CHARACTER SET UTF8) RETURNS varchar(255) CHARSET utf8
BEGIN

DECLARE LV_AUTO_ID VARCHAR(255);

SET LV_AUTO_ID=CONCAT(YEAR(CURDATE()),'-',MONTH(CURDATE()),'-',DAY(CURDATE()),'-',HOUR(CURTIME()),'-',MINUTE(CURTIME()),'-',SECOND(CURTIME()));

INSERT INTO spa_user(US_ID, FULLNAME, STAFF_ID, LOGINNAME, US_PASSWORD)
VALUES
(
  LV_AUTO_ID,
  PFULL_NAME,
  PSTAFF_ID,
  PLOGINNAME,
  PUS_PASSWORD  
);

RETURN LV_AUTO_ID; 

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `spa_agentcy`
--

CREATE TABLE IF NOT EXISTS `spa_agentcy` (
  `AGENT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `AGENT_NAME` varchar(255) DEFAULT NULL,
  `ADDRESS` varchar(255) DEFAULT NULL,
  `TEL` varchar(11) DEFAULT NULL,
  `BRANCH_ID` int(11) DEFAULT NULL,
  `D_STATUS` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`AGENT_ID`),
  KEY `FK_AGENCY_BRANCH` (`BRANCH_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `spa_agentcy`
--

INSERT INTO `spa_agentcy` (`AGENT_ID`, `AGENT_NAME`, `ADDRESS`, `TEL`, `BRANCH_ID`, `D_STATUS`) VALUES
(4, 'Capito', 'Phnom Penh\r\n                                    ', '012365623', 3, 'N'),
(5, 'Soriya', 'Phnom Penh\r\n                                    ', '012375727', 3, 'N');

-- --------------------------------------------------------

--
-- Table structure for table `spa_booking`
--

CREATE TABLE IF NOT EXISTS `spa_booking` (
  `BOOKING_ID` varchar(255) NOT NULL DEFAULT '',
  `CUS_NAME` varchar(255) DEFAULT NULL,
  `GENDER` varchar(1) DEFAULT NULL,
  `TEL` varchar(11) DEFAULT NULL,
  `AGENCY` int(11) DEFAULT NULL,
  `NUM_OF_PEX` int(11) DEFAULT NULL,
  `DATE_BOOKING` date DEFAULT NULL,
  `DATE_IN` date DEFAULT NULL,
  `TMP_ROOM_ID` int(11) DEFAULT NULL,
  `TIME_IN` varchar(255) DEFAULT '01:00:00',
  `TOTAL_HOUR` float DEFAULT '0',
  `THERAPIST` int(11) DEFAULT NULL,
  `TUKTUK` int(11) DEFAULT '0',
  `ENT_TYPE` varchar(10) NOT NULL DEFAULT 'CHECKIN',
  `BRANCH_ID` int(11) NOT NULL,
  `D_STATUS` varchar(15) DEFAULT 'PENDING',
  PRIMARY KEY (`BOOKING_ID`),
  KEY `FK_BOOKING_BRNACH` (`BRANCH_ID`),
  KEY `TMP_ROOM_ID` (`TMP_ROOM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `spa_branch`
--

CREATE TABLE IF NOT EXISTS `spa_branch` (
  `BRANCH_ID` int(11) NOT NULL AUTO_INCREMENT,
  `REGIONAL_ID` int(11) NOT NULL,
  `BRANCH_CODE` varchar(255) NOT NULL,
  `BRANCH_NAME` varchar(255) DEFAULT NULL,
  `ADDRESS` varchar(500) DEFAULT NULL,
  `ESTABLISH_DATE` date DEFAULT NULL,
  `D_STATUS` varchar(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`BRANCH_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `spa_branch`
--

INSERT INTO `spa_branch` (`BRANCH_ID`, `REGIONAL_ID`, `BRANCH_CODE`, `BRANCH_NAME`, `ADDRESS`, `ESTABLISH_DATE`, `D_STATUS`) VALUES
(3, 21, 'PP', 'Phnom Penh', '#313, str 271, phnon penh', '2016-09-28', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `spa_checkin`
--

CREATE TABLE IF NOT EXISTS `spa_checkin` (
  `CHECKIN_ID` varchar(255) NOT NULL DEFAULT '',
  `BOOKING_ID` varchar(255) DEFAULT NULL,
  `DATE_IN` date DEFAULT NULL,
  `TIME_IN` datetime DEFAULT NULL,
  `ROOM_ID` int(11) DEFAULT NULL,
  `THERAPIST` int(11) DEFAULT NULL,
  `D_STATUS` varchar(255) DEFAULT 'IN USED',
  PRIMARY KEY (`CHECKIN_ID`),
  KEY `CHECKIN_BOOKING_ID` (`BOOKING_ID`),
  KEY `CHECKIN_ROOM_ID` (`ROOM_ID`),
  KEY `spa_checkin_therapist` (`THERAPIST`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `spa_checkin_service`
--

CREATE TABLE IF NOT EXISTS `spa_checkin_service` (
  `CHKSV_ID` varchar(255) NOT NULL,
  `CHECKIN_ID` varchar(255) DEFAULT NULL,
  `SERVICE_ID` int(11) NOT NULL,
  `UNIT_PRICE` float NOT NULL DEFAULT '0',
  `AMOUNT` float NOT NULL DEFAULT '0',
  `D_STATUS` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`CHKSV_ID`),
  KEY `CHECKIN_SERVICE_CHECKIN` (`CHECKIN_ID`),
  KEY `CHECKIN_SERVICE_ID` (`SERVICE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `spa_checkout`
--

CREATE TABLE IF NOT EXISTS `spa_checkout` (
  `CHECKOUT_ID` varchar(255) NOT NULL DEFAULT '',
  `CHECKIN_ID` varchar(255) DEFAULT NULL,
  `EXCHANGE_RATE` float DEFAULT NULL,
  `DISCOUNT` float DEFAULT '0',
  `GRAND_TOTAL` float DEFAULT NULL,
  `D_STATUS` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`CHECKOUT_ID`),
  KEY `FK_CHECKOUT_CHKIN` (`CHECKIN_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `spa_data`
--

CREATE TABLE IF NOT EXISTS `spa_data` (
  `spa_id` int(11) NOT NULL AUTO_INCREMENT,
  `spa_code` varchar(255) DEFAULT NULL,
  `cus_name` varchar(255) DEFAULT NULL,
  `tel` varchar(11) DEFAULT NULL,
  `e_mail` varchar(255) DEFAULT NULL,
  `date_treatment` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `no_person` int(11) DEFAULT '0',
  `room_id` int(11) DEFAULT NULL,
  `duration` int(11) NOT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `record_type` varchar(10) DEFAULT 'BOOKING',
  `record_date` datetime DEFAULT NULL,
  `is_checkin` int(11) NOT NULL DEFAULT '0',
  `checkin_date` datetime DEFAULT NULL,
  `is_out` int(11) NOT NULL DEFAULT '0',
  `out_date` datetime DEFAULT NULL,
  `check_out_by` int(11) DEFAULT NULL,
  `discount` float NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL,
  `bookby` int(11) NOT NULL DEFAULT '0',
  `d_status` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`spa_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `spa_data`
--

INSERT INTO `spa_data` (`spa_id`, `spa_code`, `cus_name`, `tel`, `e_mail`, `date_treatment`, `start_time`, `no_person`, `room_id`, `duration`, `emp_id`, `record_type`, `record_date`, `is_checkin`, `checkin_date`, `is_out`, `out_date`, `check_out_by`, `discount`, `branch_id`, `bookby`, `d_status`) VALUES
(2, '009302', 'Tennessee', '100-934-43_', NULL, '2017-08-18', '04:14:00', 11, 4, 30, 1, 'BOOKING', '2017-08-18 00:00:00', 0, NULL, 0, NULL, NULL, 0, 3, 2016, 'D'),
(3, '009302', 'California', '099-882-888', NULL, '2017-08-19', '04:30:00', 0, 6, 60, 1, 'BOOKING', '2017-08-17 00:00:00', 0, NULL, 0, NULL, NULL, 0, 3, 2016, 'D'),
(4, '009302', 'Delaware', '094 599 009', NULL, '2017-08-17', '05:30:00', 11, 4, 0, 1, 'BOOKING', '2017-08-17 00:00:00', 0, NULL, 0, NULL, NULL, 0, 3, 3, 'N'),
(5, '009302', 'Washington', '094 599 009', NULL, '2017-08-17', '06:30:00', 11, 4, 0, 1, 'BOOKING', '2017-08-17 00:00:00', 1, '2017-08-18 23:43:00', 0, NULL, NULL, 0, 3, 3, 'N'),
(6, '1502966640', 'Texas', '099 999 066', NULL, '2017-08-18', '05:45:00', 0, 4, 0, 1, 'BOOKING', '2017-08-17 00:00:00', 0, NULL, 0, NULL, NULL, 0, 3, 3, 'N'),
(7, '1502966700', 'Delaware', '099 988 445', NULL, '2017-08-18', '05:45:00', 10, 5, 0, 1, 'BOOKING', '2017-08-17 00:00:00', 0, NULL, 0, NULL, NULL, 0, 3, 3, 'N'),
(8, '1502966880', 'Tennessee', '088 888 094', NULL, '2017-08-16', '06:15:00', 11, 3, 0, 1, 'BOOKING', '2017-08-17 00:00:00', 0, NULL, 0, NULL, NULL, 0, 3, 3, 'N'),
(9, '1502985120', 'Alabama', '099 094 044', NULL, '2017-08-09', '11:00:00', 6, NULL, 30, NULL, 'BOOKING', '2017-08-18 00:00:00', 0, NULL, 0, NULL, NULL, 0, 3, 2016, 'N'),
(10, '1502989680', 'Alabama', '098 990 009', NULL, '1970-01-01', '00:15:00', 1, 0, 0, 0, 'BOOKING', '2017-08-17 00:00:00', 0, NULL, 0, NULL, NULL, 0, 3, 3, 'D'),
(11, '1502989740', 'Tennessee', '096 099 334', NULL, '2017-08-10', '00:15:00', 1, 0, 0, 0, 'BOOKING', '2017-08-17 00:00:00', 0, NULL, 0, NULL, NULL, 0, 3, 3, 'N'),
(12, '1502989800', 'Alabama', '099 094 044', NULL, '1970-01-01', '11:00:00', 6, NULL, 0, NULL, 'BOOKING', '2017-08-17 00:00:00', 0, NULL, 0, NULL, NULL, 0, 3, 3, 'D'),
(13, '1502991780', 'Tennessee', '100-934-43_', NULL, '2017-08-16', '04:14:00', 11, 4, 0, 1, 'BOOKING', '2017-08-17 00:00:00', 0, NULL, 0, NULL, NULL, 0, 2016, 3, 'N'),
(14, '1502991780', 'Tennessee', '100-934-43_', NULL, '1970-01-01', '04:14:00', 11, 4, 0, 1, 'BOOKING', '2017-08-17 00:00:00', 0, NULL, 0, NULL, NULL, 0, 2016, 3, 'N');

-- --------------------------------------------------------

--
-- Table structure for table `spa_datadetail`
--

CREATE TABLE IF NOT EXISTS `spa_datadetail` (
  `spd_id` int(11) NOT NULL AUTO_INCREMENT,
  `spa_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `unit_price` float DEFAULT NULL,
  PRIMARY KEY (`spd_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `spa_datadetail`
--

INSERT INTO `spa_datadetail` (`spd_id`, `spa_id`, `service_id`, `qty`, `duration`, `unit_price`) VALUES
(4, 4, 0, 1, 60, 20),
(2, 4, 2, 1, 60, 30),
(3, 4, 5, 1, 60, 10),
(5, 5, 2, 1, 60, 11),
(6, 4, 5, 1, 60, 10),
(7, 5, 5, 1, 60, 234),
(8, 5, 2, 1, 60, 11);

-- --------------------------------------------------------

--
-- Table structure for table `spa_employee`
--

CREATE TABLE IF NOT EXISTS `spa_employee` (
  `EMP_ID` int(11) NOT NULL AUTO_INCREMENT,
  `EMP_CODE` varchar(50) DEFAULT NULL,
  `EMP_NAME` varchar(255) DEFAULT NULL,
  `GENDER` varchar(1) DEFAULT NULL,
  `TEL` varchar(255) DEFAULT NULL,
  `BRANCH_ID` int(11) NOT NULL,
  `ADDRESS` varchar(300) DEFAULT NULL,
  `D_STATUS` varchar(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`EMP_ID`),
  KEY `FK_EMP_BRANCH` (`BRANCH_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `spa_employee`
--

INSERT INTO `spa_employee` (`EMP_ID`, `EMP_CODE`, `EMP_NAME`, `GENDER`, `TEL`, `BRANCH_ID`, `ADDRESS`, `D_STATUS`) VALUES
(1, '001', 'Srey Leave', 'F', '0181888582', 3, 'Khsach Kandal', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `spa_employee_leave`
--

CREATE TABLE IF NOT EXISTS `spa_employee_leave` (
  `LEAVE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `EMP_ID` int(11) NOT NULL,
  `LEAVE_FR` date DEFAULT NULL,
  `LEAVE_TO` date DEFAULT NULL,
  `NO_OF_DAY` float DEFAULT NULL,
  `REASON` varchar(300) DEFAULT NULL,
  `D_STATUS` varchar(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`LEAVE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `spa_employee_leave`
--

INSERT INTO `spa_employee_leave` (`LEAVE_ID`, `EMP_ID`, `LEAVE_FR`, `LEAVE_TO`, `NO_OF_DAY`, `REASON`, `D_STATUS`) VALUES
(1, 1, '2016-10-06', '2016-10-07', 1, 'Sick Leave', 'N'),
(2, 2, '2019-09-01', '2019-09-01', 5, 'Track', 'N'),
(3, 2, '2012-12-31', '2012-12-31', 5, 'dsfsfsdfsd', 'D'),
(4, 2, '2019-09-01', '2019-09-01', 8, 'test', 'N'),
(5, 1, '2012-12-31', '2012-12-31', 4, 'dsfsd', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `spa_permission`
--

CREATE TABLE IF NOT EXISTS `spa_permission` (
  `MODULE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `MODULE_CODE` varchar(3) NOT NULL,
  `MODULE_NAME` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `LEVEL` varchar(1) NOT NULL,
  `ICON` varchar(255) NOT NULL,
  `PARENT_ID` int(11) NOT NULL,
  `D_STATUS` varchar(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`MODULE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `spa_permission`
--

INSERT INTO `spa_permission` (`MODULE_ID`, `MODULE_CODE`, `MODULE_NAME`, `URL`, `LEVEL`, `ICON`, `PARENT_ID`, `D_STATUS`) VALUES
(1, 'HME', 'Home', 'home', 'S', 'fa fa-home', 0, 'N'),
(2, 'CIF', 'Customer', 'customer', 'S', 'fa fa-users', 0, 'D'),
(3, 'SAL', 'SPA & Reservation', 'booking', 'P', 'fa fa-cart-plus', 0, 'N'),
(4, 'BK', 'Booking', 'booking', 'C', 'fa fa-angle-right', 3, 'N'),
(5, 'CHK', 'Walk In', 'checkin', 'C', 'fa fa-angle-right', 3, 'N'),
(6, 'CKO', 'Check Out', 'checkout', 'C', 'fa fa-angle-right', 3, 'B'),
(7, 'HR', 'Human Resource', 'employee', 'P', 'fa fa-user-secret', 0, 'B'),
(8, 'EMP', 'Staff', 'employee', 'S', 'fa fa-user-secret', 0, 'B'),
(9, 'EML', 'Employee Leave', 'employee_leave', 'C', 'fa fa-angle-right', 7, 'B'),
(10, 'UMG', 'User Management', 'user', 'P', 'fa fa-user-md', 0, 'N'),
(11, 'USR', 'User Profile', 'user', 'C', 'fa fa-angle-right', 10, 'N'),
(12, 'RNP', 'Rule and Permission', 'Rulenperm', 'C', 'fa fa-angle-right', 10, 'N'),
(13, 'SEU', 'Setup', 'branch', 'P', 'fa fa-gears', 0, 'N'),
(14, 'BR', 'Branch', 'branch', 'C', 'fa fa-angle-right', 13, 'N'),
(15, 'FLR', 'Floor', 'floor', 'C', 'fa fa-angle-right', 13, 'N'),
(16, 'ROM', 'Room', 'room', 'C', 'fa fa-angle-right', 13, 'N'),
(17, 'SVR', 'Service', 'service', 'C', 'fa fa-angle-right', 13, 'N'),
(18, 'UR', 'User Rule', 'rule', 'C', 'fa fa-angle-right', 13, 'N'),
(19, 'RGN', 'Regional', 'regional', 'C', 'fa fa-angle-right', 13, 'N'),
(20, 'AGT', 'Agency', 'agency', 'C', 'fa fa-angle-right', 13, 'B'),
(21, 'RPT', 'Reporting', 'reporting', 'P', 'fa fa-fa fa-bar-chart', 0, 'N'),
(22, 'RPM', 'Repayment by Branch', 'repay', 'C', 'fa fa-angle-right', 21, 'N');

-- --------------------------------------------------------

--
-- Table structure for table `spa_position`
--

CREATE TABLE IF NOT EXISTS `spa_position` (
  `POS_ID` int(11) NOT NULL AUTO_INCREMENT,
  `POS_NAME` varchar(255) NOT NULL,
  `JOB_DESCRIPTION` varchar(300) NOT NULL,
  PRIMARY KEY (`POS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `spa_regional`
--

CREATE TABLE IF NOT EXISTS `spa_regional` (
  `REGIONAL_ID` int(11) NOT NULL AUTO_INCREMENT,
  `REGIONAL_CODE` varchar(255) NOT NULL,
  `REGIONAL_NAME` varchar(255) DEFAULT NULL,
  `REGIONAL_DES` varchar(500) DEFAULT NULL,
  `D_STATUS` varchar(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`REGIONAL_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `spa_regional`
--

INSERT INTO `spa_regional` (`REGIONAL_ID`, `REGIONAL_CODE`, `REGIONAL_NAME`, `REGIONAL_DES`, `D_STATUS`) VALUES
(21, 'PP', 'Phnom Penh', 'Cover all branchs that located in Phnom Penh.', 'N'),
(22, 'SR', 'Siem Reap', 'Cover all branchs that located in Siem Reap.', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `spa_room`
--

CREATE TABLE IF NOT EXISTS `spa_room` (
  `ROOM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `BRANCH_ID` int(11) DEFAULT NULL,
  `FLOOR` int(11) NOT NULL,
  `ROOM_CODE` varchar(255) NOT NULL,
  `RTYPE_ID` int(11) NOT NULL,
  `ROOM_NAME` varchar(255) DEFAULT NULL,
  `D_STATUS` varchar(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ROOM_ID`),
  KEY `FLOOR_ID` (`FLOOR`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `spa_room`
--

INSERT INTO `spa_room` (`ROOM_ID`, `BRANCH_ID`, `FLOOR`, `ROOM_CODE`, `RTYPE_ID`, `ROOM_NAME`, `D_STATUS`) VALUES
(3, 3, 3, '001', 1, 'FUNAN ', 'N'),
(4, 3, 3, '002', 1, 'LONG VAEK ', 'N'),
(5, 3, 3, '003', 1, 'CHEN LA ', 'N'),
(6, 3, 3, '004', 1, 'KOUK THLORK ', 'N'),
(7, 3, 3, '005', 1, 'NOKOR PHNOM ', 'N'),
(8, 3, 4, '006', 1, 'MEAN BON ', 'N'),
(9, 3, 4, '007', 1, 'MEAN CHEY ', 'N'),
(10, 3, 4, '008', 1, 'MEAN YOUS ', 'N'),
(11, 3, 4, '009', 1, 'MEAN LEAP ', 'N'),
(12, 3, 4, '010', 1, 'MEAN VEASNA ', 'N'),
(13, 3, 5, '011', 2, 'CAMBODIA ', 'N'),
(14, 3, 5, '012', 2, 'THAILAND ', 'N'),
(15, 3, 5, '013', 2, 'VIETNAM ', 'N'),
(16, 3, 5, '014', 1, 'LAOS ', 'N'),
(17, 3, 5, '015', 2, 'CHINA ', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `spa_room_type`
--

CREATE TABLE IF NOT EXISTS `spa_room_type` (
  `RTYPE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `RTYPE_NAME` varchar(255) DEFAULT NULL,
  `D_STATUS` varchar(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`RTYPE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `spa_room_type`
--

INSERT INTO `spa_room_type` (`RTYPE_ID`, `RTYPE_NAME`, `D_STATUS`) VALUES
(1, 'Normal', 'N'),
(2, 'VIP', 'N'),
(3, 'Luxury', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `spa_rule`
--

CREATE TABLE IF NOT EXISTS `spa_rule` (
  `RULE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `RULE_NAME` varchar(255) DEFAULT NULL,
  `D_STATUS` varchar(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`RULE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `spa_rule`
--

INSERT INTO `spa_rule` (`RULE_ID`, `RULE_NAME`, `D_STATUS`) VALUES
(1, 'Power User', 'N'),
(2, 'Normal User', 'N'),
(3, 'Administrator', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `spa_rulenperm`
--

CREATE TABLE IF NOT EXISTS `spa_rulenperm` (
  `RNPM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `RULE_ID` int(11) DEFAULT NULL,
  `MODULE_ID` int(11) DEFAULT NULL,
  `P_VIEW` int(11) DEFAULT '0',
  `P_ADD` int(11) DEFAULT '0',
  `P_EDIT` int(11) DEFAULT '0',
  `P_DELETE` int(11) DEFAULT '0',
  `ASSIGNER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`RNPM_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=134 ;

--
-- Dumping data for table `spa_rulenperm`
--

INSERT INTO `spa_rulenperm` (`RNPM_ID`, `RULE_ID`, `MODULE_ID`, `P_VIEW`, `P_ADD`, `P_EDIT`, `P_DELETE`, `ASSIGNER`) VALUES
(116, 1, 1, 1, 0, 0, 0, '#'),
(117, 1, 3, 1, 0, 0, 0, '#'),
(118, 1, 4, 1, 0, 0, 0, '#'),
(119, 1, 5, 1, 0, 0, 0, '#'),
(120, 1, 6, 1, 0, 0, 0, '#'),
(121, 1, 8, 1, 0, 0, 0, '#'),
(122, 1, 10, 1, 0, 0, 0, '#'),
(123, 1, 11, 1, 0, 0, 0, '#'),
(124, 1, 12, 1, 0, 0, 0, '#'),
(125, 1, 13, 1, 0, 0, 0, '#'),
(126, 1, 14, 1, 0, 0, 0, '#'),
(127, 1, 15, 1, 0, 0, 0, '#'),
(128, 1, 16, 1, 0, 0, 0, '#'),
(129, 1, 17, 1, 0, 0, 0, '#'),
(130, 1, 18, 1, 0, 0, 0, '#'),
(131, 1, 19, 1, 0, 0, 0, '#'),
(132, 1, 20, 1, 0, 0, 0, '#'),
(133, 1, 21, 1, 0, 0, 0, '#');

-- --------------------------------------------------------

--
-- Table structure for table `spa_service`
--

CREATE TABLE IF NOT EXISTS `spa_service` (
  `SERVICE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `SERVICE_NAME` varchar(255) DEFAULT NULL,
  `UNIT` varchar(255) DEFAULT NULL,
  `UNIT_PRICE` float NOT NULL DEFAULT '0',
  `DESCR` varchar(255) DEFAULT NULL,
  `D_STATUS` varchar(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`SERVICE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `spa_service`
--

INSERT INTO `spa_service` (`SERVICE_ID`, `SERVICE_NAME`, `UNIT`, `UNIT_PRICE`, `DESCR`, `D_STATUS`) VALUES
(1, 'd', NULL, 0, 'd', 'D'),
(2, 'Oil massage', NULL, 0, 'Oil massage', 'N'),
(3, 'fsd', NULL, 0, 'fds', 'D'),
(4, 'te', NULL, 0, 'dfs', 'D'),
(5, 'Massage', NULL, 0, 'Provide massage service to cus', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `spa_user`
--

CREATE TABLE IF NOT EXISTS `spa_user` (
  `US_ID` varchar(255) NOT NULL,
  `FULLNAME` varchar(255) DEFAULT NULL,
  `STAFF_ID` varchar(255) DEFAULT NULL,
  `LOGINNAME` varchar(255) DEFAULT NULL,
  `US_PASSWORD` varchar(255) DEFAULT NULL,
  `D_STATUS` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`US_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spa_user`
--

INSERT INTO `spa_user` (`US_ID`, `FULLNAME`, `STAFF_ID`, `LOGINNAME`, `US_PASSWORD`, `D_STATUS`) VALUES
('2016-9-6-13-41-39', 'Leng Ratha', '00781', 'lratha', '202cb962ac59075b964b07152d234b70', 'N'),
('2016-9-6-13-48-6', 'Phon Vuthy', '08749', 'pvuthy', '202cb962ac59075b964b07152d234b70', 'D');

-- --------------------------------------------------------

--
-- Stand-in structure for view `spa_users`
--
CREATE TABLE IF NOT EXISTS `spa_users` (
`US_ID` varchar(255)
,`FULLNAME` varchar(255)
,`STAFF_ID` varchar(255)
,`LOGINNAME` varchar(255)
,`US_PASSWORD` varchar(255)
,`BRANCH_ID` int(11)
,`RULE_ID` int(11)
,`D_STATUS` varchar(1)
,`RULE_NAME` varchar(255)
);
-- --------------------------------------------------------

--
-- Table structure for table `spa_usrnbr`
--

CREATE TABLE IF NOT EXISTS `spa_usrnbr` (
  `USRNBR_ID` int(11) NOT NULL AUTO_INCREMENT,
  `US_ID` varchar(255) DEFAULT NULL,
  `BRANCH_ID` int(11) DEFAULT NULL,
  `ASSIGNED_DATE` date DEFAULT NULL,
  `ASSIGNED_BY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`USRNBR_ID`),
  KEY `FK_USRNBR` (`BRANCH_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `spa_usrnbr`
--

INSERT INTO `spa_usrnbr` (`USRNBR_ID`, `US_ID`, `BRANCH_ID`, `ASSIGNED_DATE`, `ASSIGNED_BY`) VALUES
(1, '2016-9-6-13-41-39', 3, '2016-10-05', 'lratha');

-- --------------------------------------------------------

--
-- Table structure for table `spa_usrnrule`
--

CREATE TABLE IF NOT EXISTS `spa_usrnrule` (
  `USRNRULE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `US_ID` varchar(255) DEFAULT NULL,
  `RULE_ID` int(11) DEFAULT NULL,
  `ASSIGNED_DATE` date DEFAULT NULL,
  `ASSIGNED_BY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`USRNRULE_ID`),
  KEY `US_ID` (`US_ID`),
  KEY `RULE_ID` (`RULE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `spa_usrnrule`
--

INSERT INTO `spa_usrnrule` (`USRNRULE_ID`, `US_ID`, `RULE_ID`, `ASSIGNED_DATE`, `ASSIGNED_BY`) VALUES
(1, '2016-9-6-13-41-39', 1, '2016-09-06', 'get username from session'),
(2, '2016-9-6-13-48-6', 3, '2016-09-06', 'get username from session');

-- --------------------------------------------------------

--
-- Structure for view `spa_users`
--
DROP TABLE IF EXISTS `spa_users`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `spa_users` AS select `usr`.`US_ID` AS `US_ID`,`usr`.`FULLNAME` AS `FULLNAME`,`usr`.`STAFF_ID` AS `STAFF_ID`,`usr`.`LOGINNAME` AS `LOGINNAME`,`usr`.`US_PASSWORD` AS `US_PASSWORD`,`unb`.`BRANCH_ID` AS `BRANCH_ID`,`rule`.`RULE_ID` AS `RULE_ID`,`usr`.`D_STATUS` AS `D_STATUS`,`rule`.`RULE_NAME` AS `RULE_NAME` from (((`spa_user` `usr` join `spa_usrnbr` `unb` on((`unb`.`US_ID` = `usr`.`US_ID`))) left join `spa_usrnrule` `urb` on((`urb`.`US_ID` = `usr`.`US_ID`))) left join `spa_rule` `rule` on((`rule`.`RULE_ID` = `urb`.`RULE_ID`)));

--
-- Constraints for dumped tables
--

--
-- Constraints for table `spa_agentcy`
--
ALTER TABLE `spa_agentcy`
  ADD CONSTRAINT `FK_AGENCY_BRANCH` FOREIGN KEY (`BRANCH_ID`) REFERENCES `spa_branch` (`BRANCH_ID`);

--
-- Constraints for table `spa_booking`
--
ALTER TABLE `spa_booking`
  ADD CONSTRAINT `FK_BOOKING_BRNACH` FOREIGN KEY (`BRANCH_ID`) REFERENCES `spa_branch` (`BRANCH_ID`);

--
-- Constraints for table `spa_checkin`
--
ALTER TABLE `spa_checkin`
  ADD CONSTRAINT `CHECKIN_BOOKING_ID` FOREIGN KEY (`BOOKING_ID`) REFERENCES `spa_booking` (`BOOKING_ID`),
  ADD CONSTRAINT `CHECKIN_ROOM_ID` FOREIGN KEY (`ROOM_ID`) REFERENCES `spa_room` (`ROOM_ID`),
  ADD CONSTRAINT `spa_checkin_therapist` FOREIGN KEY (`THERAPIST`) REFERENCES `spa_employee` (`EMP_ID`);

--
-- Constraints for table `spa_checkin_service`
--
ALTER TABLE `spa_checkin_service`
  ADD CONSTRAINT `CHECKIN_SERVICE_CHECKIN` FOREIGN KEY (`CHECKIN_ID`) REFERENCES `spa_checkin` (`CHECKIN_ID`),
  ADD CONSTRAINT `CHECKIN_SERVICE_ID` FOREIGN KEY (`SERVICE_ID`) REFERENCES `spa_service` (`SERVICE_ID`);

--
-- Constraints for table `spa_checkout`
--
ALTER TABLE `spa_checkout`
  ADD CONSTRAINT `FK_CHECKOUT_CHKIN` FOREIGN KEY (`CHECKIN_ID`) REFERENCES `spa_checkin` (`CHECKIN_ID`) ON UPDATE CASCADE;

--
-- Constraints for table `spa_employee`
--
ALTER TABLE `spa_employee`
  ADD CONSTRAINT `FK_EMP_BRANCH` FOREIGN KEY (`BRANCH_ID`) REFERENCES `spa_branch` (`BRANCH_ID`);

--
-- Constraints for table `spa_usrnbr`
--
ALTER TABLE `spa_usrnbr`
  ADD CONSTRAINT `FK_USRNBR` FOREIGN KEY (`BRANCH_ID`) REFERENCES `spa_branch` (`BRANCH_ID`);

--
-- Constraints for table `spa_usrnrule`
--
ALTER TABLE `spa_usrnrule`
  ADD CONSTRAINT `spa_usrnrule_ibfk_2` FOREIGN KEY (`RULE_ID`) REFERENCES `spa_rule` (`RULE_ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
