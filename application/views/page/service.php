<script>
    $(document).ready(function(){
        $("#search_box").keyup(function () {
            $.ajax({
                type: "post",
                url: "<?php echo base_url('service/get_service')?>",
                data: {
                    key:$('#search_box').val()
                },
                dataType: "json",
                success: function (data) {
                    $('#tbl_regional tbody tr').remove();
                    $('#tbl_regional tbody tr').slideDown('slow');
                    var rg_no=0;
                    if(data.length==0)
                    {
                        $('#tbl_regional tbody').append('<tr><td colspan="4"><img src="<?php echo base_url('assets/dist/img/commons/loading.gif');?>" width="25px" height="25px"> &nbsp; No Item found! </td></tr>')
                    }
                    $.each(data, function (key, value) {
                        rg_no+=1;
                        $('#tbl_regional tbody').append(
                            '<tr><td width="5%">'+rg_no+'</td><td width="25%">'+rg_no+'</td width="40%"><td>'+value['SERVICE_NAME']+'</td><td>'+value['UNIT']+'</td><td>'+value['UNIT_PRICE']+'</td><td><button style="padding: 0 6px;" class="btn btn-primary" onclick="edit_service(\''+value['SERVICE_ID']+'\',\''+value['SERVICE_NAME']+'\',\''+value['UNIT']+'\',\''+value['UNIT_PRICE']+'\',\''+value['DESCR']+'\');"><i class="fa fa-pencil"></i></a></button> | <button style="padding: 0 6px;" class="btn btn-danger" onclick="delete_service(\''+value['SERVICE_ID']+'\')"><i class="fa fa-times"></i></a></button></td></tr>')
                    });
                }
            });
        });
    });

    function edit_service(id, name, unit,unit_price, descr)
    {
        document.getElementById('sv_id').value=id;
        document.getElementById('sv_name').value=name;
        document.getElementById('unit').value=unit;
        document.getElementById('unit_price').value=unit_price;
        document.getElementById('sv_des').value=descr;
    }

    function delete_service(id)
    {
        cfm = confirm('Are you sure you delete this row?');
        if(cfm==true)
        {
            $.ajax({
                type: "post",
                url: "<?php echo base_url('service/delete_service')?>/"+id,
                success: function (data) {
                    location.reload();
                }
            });
        }
    }

</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" >


    <!-- Main content -->
    <section class="content">


        <?php echo $this->session->userdata('msg'); ?>

        <section class="content" style="padding:0; margin:0;height: 100%;">
            <div class="row" style="height: 100%;">
                <div class="col-md-12">
                    <div class="box box-default" style="border: 1px solid #dddddd; box-shadow: none;">
                        <div class="box-header" style="border-bottom: 1px solid #ddd; background: #fafafa; color:#3c8dbc;">
                            <b>Service</b>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-5" style="font-size: 12px;">
                                    <form method="post" enctype="multipart/form-data" action="<?php echo base_url('service/add_service')?>" >

                                        <div class="form-group">
                                            <label>Service/Product Name</label>
                                            <input class="form-control" required type="text" placeholder="Service/product name..." name="sv_name" id="sv_name" />
                                            <input type="hidden" name="sv_id" id="sv_id" value="0" />
                                        </div>

                                        <div class="form-group">
                                            <label>Unit</label>
                                            <input class="form-control" required type="text" placeholder="Unit of measurement..." name="unit" id="unit" />
                                        </div>

                                        <div class="form-group">
                                            <label>Price</label>
                                            <input class="form-control" required type="text" placeholder="Price per unit..." name="unit_price" id="unit_price" />
                                        </div>

                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea class="form-control" name="sv_des" id="sv_des" placeholder="Description..."></textarea>
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Save</button>
                                            <button type="reset" class="btn btn-danger">Reset</button>
                                        </div>
                                    </form>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-7">
                                    <div class="form-group" style="border: 1px solid #dddddd; margin-top: 22px; font-size: 12px;">
                                        <input type="text" id="search_box" class="form-control" placeholder="Search service..." style="font-size: 12px; border-width: 0 0 1px 0;">
                                        <div style="height: 249px; overflow-y: scroll;" >
                                            <table class="table table-responsive" style="font-size: 12px;" id="tbl_regional">
                                                <thead>
                                                    <tr>
                                                        <th>N#</th>
                                                        <th>Code</th>
                                                        <th>Name</th>
                                                        <th>Unit</th>
                                                        <th>Price/Unit</th>
                                                        <th><i class="fa fa-bolt" aria-hidden="true"></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="6"><li class="fa fa-level-up"></li> Find service with box above!</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>

                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <!--<i>Regional Information form</i>-->
                        </div>
                    </div>
                    <!-- /. box -->
                </div>
                <!-- /.col -->
            </div>
        </section>
        <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<script>
    $(document).ready(function(){

            $.ajax({
                type: "post",
                url: "<?php echo base_url('service/get_service')?>",
                data: {
                    key:$('#search_box').val()
                },
                dataType: "json",
                success: function (data) {
                    $('#tbl_regional tbody tr').remove();
                    $('#tbl_regional tbody tr').slideDown('slow');
                    var rg_no=0;
                    if(data.length==0)
                    {
                        $('#tbl_regional tbody').append('<tr><td colspan="6"><img src="<?php echo base_url('assets/dist/img/commons/loading.gif');?>" width="25px" height="25px"> &nbsp; No Item found! </td></tr>')
                    }
                    $.each(data, function (key, value) {
                        rg_no+=1;
                        $('#tbl_regional tbody').append(
                            '<tr><td width="5%">'+rg_no+'</td><td width="25%">'+rg_no+'</td width="40%"><td>'+value['SERVICE_NAME']+'</td><td>'+value['UNIT']+'</td><td>'+value['UNIT_PRICE']+'</td><td><button style="padding: 0 6px;" class="btn btn-primary" onclick="edit_service(\''+value['SERVICE_ID']+'\',\''+value['SERVICE_NAME']+'\',\''+value['UNIT']+'\',\''+value['UNIT_PRICE']+'\',\''+value['DESCR']+'\');"><i class="fa fa-pencil"></i></a></button> | <button style="padding: 0 6px;" class="btn btn-danger" onclick="delete_service(\''+value['SERVICE_ID']+'\')"><i class="fa fa-times"></i></a></button></td></tr>')
                    });
                }
            });
        });
</script>