<script>
    $(document).ready(function(){
        $("#search_box").keyup(function () {
            $.ajax({
                type: "post",
                url: "<?php echo base_url('branch/get_branch')?>",
                data: {
                    key:$('#search_box').val()
                },
                dataType: "json",
                success: function (data) {
                    $('#tbl_regional tbody tr').remove();
                    $('#tbl_regional tbody tr').slideDown('slow');
                    var rg_no=0;
                    if(data.length==0)
                    {
                        $('#tbl_regional tbody').append('<tr><td colspan="4"><img src="<?php echo base_url('assets/dist/img/commons/loading.gif');?>" width="25px" height="25px"> &nbsp; No Item found! </td></tr>')
                    }
                    $.each(data, function (key, value) {
                        rg_no+=1;
                        $('#tbl_regional tbody').append(
                            '<tr><td width="5%">'+rg_no+'</td><td width="25%">'+value['BRANCH_CODE']+'</td width="40%"><td>'+value['BRANCH_NAME']+'</td><td><button style="padding: 0 6px;" class="btn btn-primary" onclick="edit_branch(\''+value['BRANCH_ID']+'\',\''+value['REGIONAL_ID']+'\',\''+value['BRANCH_CODE']+'\',\''+value['BRANCH_NAME']+'\',\''+value['ADDRESS']+'\',\''+value['ESTABLISH_DATE']+'\');"><i class="fa fa-pencil"></i></a></button> | <button style="padding: 0 6px;" class="btn btn-danger" onclick="delete_branch(\''+value['BRANCH_ID']+'\')"><i class="fa fa-times"></i></a></button></td></tr>')
                    });
                }
            });
        });
    });

    function edit_branch(id, rgid, code, name, address, est_date)
    {
        var d = new Date(est_date);
        document.getElementById('br_id').value=id;
        document.getElementById('regional').value=rgid;
        document.getElementById('br_code').value=code;
        document.getElementById('br_name').value=name;
        document.getElementById('br_address').value=address;
        document.getElementById('est_date').value= d.toLocaleFormat('%d-%m-%Y');
    }

    function delete_branch(id)
    {
        cfm = confirm('Are you sure you delete this row?');
        if(cfm==true)
        {
            $.ajax({
                type: "post",
                url: "<?php echo base_url('branch/delete_branch')?>/"+id,
                success: function (data) {
                    location.reload();
                }
            });
        }
    }

</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" >


    <!-- Main content -->
    <section class="content">


        <?php echo $this->session->userdata('msg'); ?>

        <section class="content" style="padding:0; margin:0;height: 100%;">
            <div class="row" style="height: 100%;">
                <div class="col-md-12">
                    <div class="box box-default" style="border: 1px solid #dddddd; box-shadow: none;">
                        <div class="box-header" style="border-bottom: 1px solid #ddd; background: #fafafa; color:#3c8dbc;">
                            <b>Branch</b>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-5" style="font-size: 12px;">
                                    <form method="post" enctype="multipart/form-data" action="<?php echo base_url('branch/add_branch')?>" >
                                        <div class="form-group">
                                            <label>Branch Code</label>
                                            <input class="form-control" required type="text" placeholder="Branch Code..." name="br_code" id="br_code" />
                                            <input type="hidden" id="br_id" name="br_id" />
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label>Branch Name</label>
                                            <input class="form-control" required type="text" placeholder="Branch Name..." name="br_name" id="br_name" />
                                        </div>

                                        <div class="form-group">
                                            <label>Regional</label>
                                            <select class="form-control" required name="regional" id="regional">
                                                <?php
                                                    foreach($regional->result() as $row)
                                                    {
                                                        echo '<option value="'.$row->REGIONAL_ID.'">'.$row->REGIONAL_NAME.'</option>';
                                                    }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Establish Date</label>
                                            <input class="form-control" required type="text" placeholder="dd-mm-yyyy" name="est_date" id="est_date" onblur="validateDOB('est_date');" />
                                        </div>

                                        <div class="form-group">
                                            <label>Address</label>
                                            <textarea class="form-control" placeholder="Address..." required name="br_address" id="br_address"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Save</button>
                                            <button type="reset" class="btn btn-danger">Reset</button>
                                        </div>
                                    </form>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-7">
                                    <div class="form-group" style="border: 1px solid #dddddd; margin-top: 22px; font-size: 12px;">
                                        <input type="text" id="search_box" class="form-control" placeholder="Search branch..." style="font-size: 12px; border-width: 0 0 1px 0;">
                                        <div style="height: 320px; overflow-y: scroll;" >
                                            <table class="table table-responsive" style="font-size: 12px;" id="tbl_regional">
                                                <thead>
                                                    <tr>
                                                        <th>N#</th>
                                                        <th>Code</th>
                                                        <th>Name</th>
                                                        <th><i class="fa fa-bolt" aria-hidden="true"></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="4"><li class="fa fa-level-up"></li> Find branch with box above!</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>

                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <!--<i>Regional Information form</i>-->
                        </div>
                    </div>
                    <!-- /. box -->
                </div>
                <!-- /.col -->
            </div>
        </section>
        <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
    $(document).ready(function(){

            $.ajax({
                type: "post",
                url: "<?php echo base_url('branch/get_branch')?>",
                data: {
                    key:$('#search_box').val()
                },
                dataType: "json",
                success: function (data) {
                    $('#tbl_regional tbody tr').remove();
                    $('#tbl_regional tbody tr').slideDown('slow');
                    var rg_no=0;
                    if(data.length==0)
                    {
                        $('#tbl_regional tbody').append('<tr><td colspan="4"><img src="<?php echo base_url('assets/dist/img/commons/loading.gif');?>" width="25px" height="25px"> &nbsp; No Item found! </td></tr>')
                    }
                    $.each(data, function (key, value) {
                        rg_no+=1;
                        $('#tbl_regional tbody').append(
                            '<tr><td width="5%">'+rg_no+'</td><td width="25%">'+value['BRANCH_CODE']+'</td width="40%"><td>'+value['BRANCH_NAME']+'</td><td><button style="padding: 0 6px;" class="btn btn-primary" onclick="edit_branch(\''+value['BRANCH_ID']+'\',\''+value['REGIONAL_ID']+'\',\''+value['BRANCH_CODE']+'\',\''+value['BRANCH_NAME']+'\',\''+value['ADDRESS']+'\',\''+value['ESTABLISH_DATE']+'\');"><i class="fa fa-pencil"></i></a></button> | <button style="padding: 0 6px;" class="btn btn-danger" onclick="delete_branch(\''+value['BRANCH_ID']+'\')"><i class="fa fa-times"></i></a></button></td></tr>')
                    });
                }
            });

    });
</script>