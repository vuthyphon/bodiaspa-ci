<script>
    $(document).ready(function(){
        $("#search_box").keyup(function () {
            $.ajax({
                type: "post",
                url: "<?php echo base_url('booking/get_booking')?>",
                data: {
                    key:$('#search_box').val()
                },
                dataType: "json",
                success: function (data) {
                    $('#tbl_regional tbody tr').remove();
                    $('#tbl_regional tbody tr').slideDown('slow');
                    var rg_no=0;
                    if(data.length==0)
                    {
                        $('#tbl_regional tbody').append('<tr><td colspan="4"><img src="<?php echo base_url('assets/dist/img/commons/loading.gif');?>" width="25px" height="25px"> &nbsp; No Item found! </td></tr>')
                    }
                    $.each(data, function (key, value) {
                        rg_no+=1;
                        $('#tbl_regional tbody').append(
                            '<tr><td width="5%">'+rg_no+'</td><td width="25%">'+value['BOOKING_ID']+'</td width="40%"><td>'+value['CUS_NAME']+'</td><td><button style="padding: 0 6px;" class="btn btn-primary" onclick="edit_booking(\''+value['BOOKING_ID']+'\',\''+value['CUS_NAME']+'\',\''+value['GENDER']+'\',\''+value['TEL']+'\',\''+value['AGENCY']+'\',\''+value['DATE_IN']+'\');"><i class="fa fa-pencil"></i></a></button> | <button style="padding: 0 6px;" class="btn btn-danger" onclick="delete_booking(\''+value['BOOKING_ID']+'\')"><i class="fa fa-times"></i></a></button></td></tr>')
                    });
                }
            });
        });


    });

    function edit_booking(id, name, sex, tel, travel,dtin)
    {
        var d = new Date(dtin);
        document.getElementById('bookid').value=id;
        document.getElementById('cus_name').value=name;
        document.getElementById('sex').value=sex;
        document.getElementById('tel').value=tel;
        document.getElementById('agency').value=travel;
        document.getElementById('dtin').value= d.toLocaleFormat('%d-%m-%Y');
    }

    function delete_booking(id)
    {
        cfm = confirm('Are you sure you delete this row?');
        if(cfm==true)
        {
            $.ajax({
                type: "post",
                url: "<?php echo base_url('booking/delete_booking')?>/"+id,
                success: function (data) {
                    location.reload();
                }
            });
        }
    }


</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" >


    <!-- Main content -->
    <section class="content">


        <?php echo $this->session->userdata('msg'); ?>

        <section class="content" style="padding:0; margin:0;height: 100%;">
            <div class="row" style="height: 100%;">
                <div class="col-md-12">
                    <div class="box box-default" style="border: 1px solid #dddddd; box-shadow: none;">
                        <div class="box-header" style="border-bottom: 1px solid #ddd; background: #fafafa; color:#3c8dbc;">
                            <b>Booking and Reservation</b>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-5" style="font-size: 12px;">
                                    <form method="post" enctype="multipart/form-data" action="<?php echo base_url('booking/booking')?>" >
                                        <div class="form-group">
                                            <label>Customer name</label>
                                            <input class="form-control smbox" required type="text" placeholder="Customer name" name="cus_name" id="cus_name" />
                                            <input type="hidden" id="bookid" name="bookid" />
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label>Gender</label>
                                            <select class="form-control" name="sex" id="sex" style="height: 25px; padding: 0; font-size: 12px;">
                                                <option value="M">Male</option>
                                                <option value="F">Female</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Tel</label>
                                            <input class="form-control smbox" required type="text" placeholder="Tel" name="tel" id="tel" />
                                        </div>

                                        <div class="form-group">
                                            <label>Travel Agent</label>
                                            <select class="form-control" name="agency" id="agency" style="height: 25px; padding: 0; font-size: 12px;">
                                                <option value="0">--Select Agency--</option>
                                                <?php
                                                foreach($agent->result() as $row)
                                                {
                                                    echo '<option value="'.$row->AGENT_ID.'">'.$row->AGENT_NAME.'</option>';
                                                }
                                                ?>
                                                <option data-toggle="modal" data-target="#agent" value="" style="color:#0000cc;">Add New</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Date In(dd-mm-yyyy)</label>
                                            <input class="form-control smbox" required type="text" placeholder="dd-mm-yyyy" name="dtin" id="dtin" />
                                        </div>

                                        <div class="form-group">
                                            <label>Time In(24 format)</label>
                                            <input class="form-control smbox" required type="text" placeholder="hh:mm:ss" name="timein" id="timein" />
                                        </div>

                                        <div class="form-group">
                                            <label>Duration(Ex: 01:30:00 => 1 hour and 30 minutes)</label>
                                            <input class="form-control smbox" required type="text" placeholder="hh:mm:00" name="duration" id="duration" />
                                        </div>

                                        <div class="form-group">
                                            <label>Available Room</label>
                                            <select class="form-control" required name="room" id="room" style="height: 25px; padding: 0; font-size: 12px;">
                                                <?php foreach($avai_room->result() as $row_room) { ?>
                                                    <option <?php echo ($row_room->ROOM_ID==$_SESSION['BOOKING_ROOM']['0']['ROOM_ID'])?'selected':''?> value="<?php echo $row_room->ROOM_ID?>"><?php echo $row_room->ROOM_CODE.': '.$row_room->ROOM_NAME." | Floor: ".$row_room->FLOOR_ID?></option>
                                                <?php } ?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Save</button>
                                            <button type="reset" class="btn btn-danger">Reset</button>
                                        </div>
                                    </form>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-7">
                                    <div class="form-group" style="border: 1px solid #dddddd; margin-top: 22px; font-size: 12px;">
                                        <input type="text" id="search_box" class="form-control" placeholder="Search branch..." style="font-size: 12px; border-width: 0 0 1px 0;">
                                        <div style="height: 320px; overflow-y: scroll;" >
                                            <table class="table table-responsive" style="font-size: 12px;" id="tbl_regional">
                                                <thead>
                                                    <tr>
                                                        <th>N#</th>
                                                        <th>Code</th>
                                                        <th>Name</th>
                                                        <th><i class="fa fa-bolt" aria-hidden="true"></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="4"><li class="fa fa-level-up"></li> Find branch with box above!</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>

                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <!--<i>Regional Information form</i>-->
                        </div>
                    </div>
                    <!-- /. box -->
                </div>
                <!-- /.col -->
            </div>

            <!-- Modal -->
            <div class="modal fade" id="agent" role="dialog" style="position: absolute; z-index: 100000;">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header bg-primary">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h5 class="modal-title" style="color:#FFF;">Agent</h5>
                        </div>
                        <div class="modal-body" style="font-size: 12px; ">
                            <form method="post" enctype="multipart/form-data" action="<?php echo base_url('booking/addAgent')?>">
                                <div class="form-group">
                                    <label>Agency name</label>
                                    <input class="form-control smbox" required type="text" placeholder="Agency name" name="agent_name" id="agent_name" />
                                </div>

                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea class="form-control" name="address" id="address" style="font-size: 12px;">

                                    </textarea>
                                </div>

                                <div class="form-group">
                                    <label>Tel</label>
                                    <input class="form-control smbox" required type="text" placeholder="Contact" name="agent_tel" id="agent_tel" />
                                </div>

                                <div class="form-group">
                                    <button class="btn btn-primary">Save</button>
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
            <!-- Model -->

        </section>
        <!-- /.content -->
</div>
<!-- /.content-wrapper -->