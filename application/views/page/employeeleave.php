<script>
    $(document).ready(function(){
        $("#search_box").keyup(function () {
            $.ajax({
                type: "post",
                url: "<?php echo base_url('employee_leave/get_employeeleave')?>",
                data: {
                    key:$('#search_box').val()
                },
                dataType: "json",
                success: function (data) {
                    $('#tbl_regional tbody tr').remove();
                    $('#tbl_regional tbody tr').slideDown('slow');
                    var rg_no=0;
                    if(data.length==0)
                    {
                        $('#tbl_regional tbody').append('<tr><td colspan="4"><img src="<?php echo base_url('assets/dist/img/commons/loading.gif');?>" width="25px" height="25px"> &nbsp; No Item found! </td></tr>')
                    }
                    $.each(data, function (key, value) {
                        rg_no+=1;
                        $('#tbl_regional tbody').append(
                            '<tr><td width="5%">'+rg_no+'</td><td width="25%">'+value['EMP_NAME']+'</td width="40%"><td>'+value['LEAVE_FR']+'</td><td><button style="padding: 0 6px;" class="btn btn-primary" onclick="edit_employeeleave(\''+value['LEAVE_ID']+'\',\''+value['EMP_ID']+'\',\''+value['LEAVE_FR']+'\',\''+value['LEAVE_TO']+'\',\''+value['NO_OF_DAY']+'\',\''+value['REASON']+'\');"><i class="fa fa-pencil"></i></a></button> | <button style="padding: 0 6px;" class="btn btn-danger" onclick="delete_employeeleave(\''+value['LEAVE_ID']+'\')"><i class="fa fa-times"></i></a></button></td></tr>')
                    });
                }
            });
        });
    });

    function edit_employeeleave(id, emp, frdt, todt, no_day, reason)
    {
        var fr = new Date(frdt);
        var to = new Date(todt);
        document.getElementById("leaveid").value=id;
        document.getElementById('emp_id').value=emp;
        document.getElementById('frdt').value=fr.toLocaleFormat('%d-%m-%Y');
        document.getElementById('todt').value=to.toLocaleFormat('%d-%m-%Y');
        document.getElementById('no_day').value = no_day;
        document.getElementById('reason').value=reason;
    }

    function delete_employeeleave(id)
    {
        cfm = confirm('Are you sure you delete this row?');
        if(cfm==true)
        {
            $.ajax({
                type: "post",
                url: "<?php echo base_url('employee_leave/delete_employeeleave')?>/"+id,
                success: function (data) {
                    location.reload();
                }
            });
        }
    }

</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" >


    <!-- Main content -->
    <section class="content">


        <?php echo $this->session->userdata('msg'); ?>

        <section class="content" style="padding:0; margin:0;height: 100%;">
            <div class="row" style="height: 100%;">
                <div class="col-md-12">
                    <div class="box box-default" style="border: 1px solid #dddddd; box-shadow: none;">
                        <div class="box-header" style="border-bottom: 1px solid #ddd; background: #fafafa; color:#3c8dbc;">
                            <b>Employee Request Leave</b>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-5" style="font-size: 12px;">
                                    <form method="post" enctype="multipart/form-data" action="<?php echo base_url('employee_leave/add_employeeleave')?>" >

                                        <div class="form-group">
                                            <label>Employee</label>
                                            <select class="form-control" required name="emp_id" id="emp_id">
                                                <?php
                                                foreach($employee->result() as $row)
                                                {
                                                    echo '<option value="'.$row->EMP_ID.'">'.$row->EMP_NAME.'</option>';
                                                }
                                                ?>
                                            </select>
                                            <input type="hidden" name="leaveid" id="leaveid" />
                                        </div>

                                        <div class="form-group">
                                            <label>Leave From Date</label>
                                            <input class="form-control" required type="text" placeholder="dd-mm-yyyy" name="frdt" id="frdt" onblur="validateDOB('frdt');" />
                                        </div>

                                        <div class="form-group">
                                            <label>To Date</label>
                                            <input class="form-control" required type="text" placeholder="dd-mm-yyyy" name="todt" id="todt" onblur="validateDOB('todt');" />
                                        </div>

                                        <div class="form-group">
                                            <label>Amount of day leave</label>
                                            <input class="form-control" maxlength="2" type="text" placeholder="Amount of day leave..." name="no_day" id="no_day" onkeypress="return isNumberKey(event)" />
                                        </div>

                                        <div class="form-group">
                                            <label>Reason</label>
                                            <textarea class="form-control" name="reason" id="reason" placeholder="Reason..."></textarea>
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Save</button>
                                            <button type="reset" class="btn btn-danger">Reset</button>
                                        </div>
                                    </form>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-7">
                                    <div class="form-group" style="border: 1px solid #dddddd; margin-top: 22px; font-size: 12px;">
                                        <input type="text" id="search_box" class="form-control" placeholder="Search employee..." style="font-size: 12px; border-width: 0 0 1px 0;">
                                        <div style="height: 319px; overflow-y: scroll;" >
                                            <table class="table table-responsive" style="font-size: 12px;" id="tbl_regional">
                                                <thead>
                                                    <tr>
                                                        <th>N#</th>
                                                        <th>Employee</th>
                                                        <th>Leave Date</th>
                                                        <th><i class="fa fa-bolt" aria-hidden="true"></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="4"><li class="fa fa-level-up"></li> Find employee's leave with box above!</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>

                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <!--<i>Regional Information form</i>-->
                        </div>
                    </div>
                    <!-- /. box -->
                </div>
                <!-- /.col -->
            </div>
        </section>
        <!-- /.content -->
</div>
<!-- /.content-wrapper -->