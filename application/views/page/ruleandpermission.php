<script>
    $(document).ready(function(){
        $('#rule').change(function(){
            //alert($('#rule').val());
            window.location.href='<?php echo base_url('rulenperm/edit')?>/'+$('#rule').val();
            //$('#rule').val=1;
        });
    });

</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" >


    <!-- Main content -->
    <section class="content">


        <?php //echo $this->session->userdata('msg'); ?>

        <section class="content" style="padding:0; margin:0;height: 100%;">
            <div class="row" style="height: 100%;">
                <div class="col-md-12">
                    <div class="box box-default" style="border: 1px solid #dddddd; box-shadow: none;">
                        <div class="box-header" style="border-bottom: 1px solid #ddd; background: #fafafa; color:#3c8dbc;">
                            <b>Rule and Permission</b>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-5" style="font-size: 12px;">
                                    <form method="post" enctype="multipart/form-data" action="<?php echo base_url('rulenperm/update_perm').'/'.$this->uri->segment(3);?>" >

                                        <div class="form-group">
                                            <label>Rule</label>
                                            <select class="form-control" name="rule" id="rule">
                                                <option selected value="0">--Select Rule--</option>
                                                <?php
                                                    foreach($rule->result() as $rowrule)
                                                    {
                                                        echo '<option '.($rowrule->RULE_ID==$this->uri->segment(3)?' selected ':'').' value="'.$rowrule->RULE_ID.'">'.$rowrule->RULE_NAME.'</option>';
                                                    }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="form-group">

                                            <?php
                                            if(isset($menu_perm))
                                            {
                                                foreach($menu_perm as $k=>$v){

                                                    ?>
                                                    <div class="checkbox">
                                                        <label style="margin-left: 15px;">
                                                            <input <?php echo($v['P_VIEW']==1)?' checked ':''?> type="checkbox" name="<?php echo $v['MODULE_ID']?>"> <?php echo $v['MODULE_NAME']?>
                                                        </label>
                                                    </div>
                                                <?php }}
                                            ?>

                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary" name="submit">Save</button>
                                            <button type="reset" onclick="<?php echo($menu_perm==0)?"":"location.href='".base_url('rulenperm')."'"?>" class="btn btn-danger" name="reset">Reset</button>
                                        </div>


                                    </form>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-7">

                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <!--<i>Regional Information form</i>-->
                        </div>
                    </div>
                    <!-- /. box -->
                </div>
                <!-- /.col -->
            </div>
        </section>
        <!-- /.content -->
</div>
<!-- /.content-wrapper -->