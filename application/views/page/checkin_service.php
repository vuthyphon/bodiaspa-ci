<script>

    getItemToList();

    function getItemToList()
    {
        $.ajax({
            type: "post",
            url: "<?php echo base_url('checkin/getItemToList').'/'.$this->uri->segment(3)?>",
            dataType: "json",
            success: function (data) {
                $('#tbl_regional tbody tr').remove();
                $('#tbl_regional tbody tr').slideDown('slow');
                var rg_no=0;
                if(data.length==0)
                {
                    $('#tbl_regional tbody').append('<tr><td colspan="5"><img src="<?php echo base_url('assets/dist/img/commons/loading.gif');?>" width="25px" height="25px"> &nbsp; No item added! </td></tr>')
                }
                $.each(data, function (key, value) {
                    rg_no+=1;
                    $('#tbl_regional tbody').append(
                        '<tr><td width="5%">'+rg_no+'</td><td width="40%">'+value['SERVICE_NAME']+'</td><td width="20%">'+value['UNIT_PRICE']+'</td><td width="15%">'+value['AMOUNT']+'</td><td><button style="padding: 0 6px;" class="btn btn-primary" onclick="edit_item(\''+value['SERVICE_ID']+'\',\''+value['UNIT_PRICE']+'\',\''+value['AMOUNT']+'\')"><i class="fa fa-pencil"></i></a></button> | <button style="padding: 0 6px;" class="btn btn-danger" onclick="delete_checkinsv(\''+value['CHKSV_ID']+'\')"><i class="fa fa-times"></i></a></button></td></tr>')

                });
            }
        });
    }

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    function getTotalPrice()
    {
        $.ajax({
            type: "post",
            url: "<?php echo base_url('checkin/getTotalPrice').'/'.$this->uri->segment(3)?>",
            dataType: "json",
            success: function (data) {
                $.each(data, function (key, value) {
                    var discount=(document.getElementById('discount').value.length>0)?document.getElementById('discount').value:0;
                    var exchange=(document.getElementById('exchange').value.length>0)?document.getElementById('exchange').value:4000;
                    var x=(value['TOTAL_USD']-(value['TOTAL_USD']*(discount/100)));
                    document.getElementById('total_usd').value=numberWithCommas(x);
                    document.getElementById('total_').value=numberWithCommas(x);
                    document.getElementById('total_khr').value=numberWithCommas(x*exchange);
                    //alert(numberWithCommas(value['TOTAL_USD']*4000));
                    //alert(9200-(9200*(0/100)));
                });
            }
        });
    }

    function addItem()
    {
        var cid=document.getElementById('checkinid').value;
        var svid=document.getElementById('serviceid').value;
        var upr=document.getElementById('unit_price').value;
        var amt=document.getElementById('pamount').value;
        $.ajax({
            type: "post",
            url: "<?php echo base_url('checkin/add_checkin_service')?>",
            data: {
                checkinid:cid,
                serviceid:svid,
                unit_price:upr,
                pamount:amt
            },
            dataType: "text",
            success: function (data) {
                //location.reload();
                getItemToList();
                document.getElementById('serviceid').value='';
                document.getElementById('unit_price').value='';
                document.getElementById('pamount').value='';
            }
        });
    }

    function edit_item(svid,upr,amt)
    {
        document.getElementById('serviceid').value=svid;
        document.getElementById('unit_price').value=upr;
        document.getElementById('pamount').value=amt;
    }

    function delete_checkinsv(id)
    {
        cfm = confirm('Are you sure you want remove this service?');
        if(cfm==true)
        {
            $.ajax({
                type: "post",
                url: "<?php echo base_url('checkin/remove_chsv')?>",
                data: {
                    chsvid:id
                },
                dataType: "text",
                success: function (data) {
                    //location.reload();
                    getItemToList();
                }
            });
        }
    }

</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" >


    <!-- Main content -->
    <section class="content">


        <?php echo $this->session->userdata('msg'); ?>

        <section class="content" style="padding:0; margin:0;height: 100%;">
            <div class="row" style="height: 100%;">
                <div class="col-md-12">
                    <div class="box box-default" style="border: 1px solid #dddddd; box-shadow: none;">
                        <div class="box-header" style="border-bottom: 1px solid #ddd; background: #fafafa; color:#3c8dbc;">
                            <b>Services used by check in code: <?php echo $this->uri->segment(3);?> </b>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-5" style="font-size: 12px;">
                                    <form method="post" enctype="multipart/form-data" onsubmit="return false;" >

                                        <div class="form-group">
                                            <label>Service</label>
                                            <select class="form-control" name="serviceid" required id="serviceid">
                                                <?php foreach($service->result() as $row){ ?>
                                                    <option  value="<?php echo $row->SERVICE_ID?>"><?php echo $row->SERVICE_NAME;?></option>
                                                <?php } ?>
                                            </select>
                                            <input type="hidden" name="checkinid" id="checkinid" value="<?php echo $this->uri->segment(3);?>">
                                        </div>

                                        <div class="form-group">
                                            <label>Unit price</label>
                                            <input name="unit_price" id="unit_price" required type="text" placeholder="0.00" class="form-control" />
                                        </div>

                                        <div class="form-group">
                                            <label>Amount</label>
                                            <input name="pamount" id="pamount" required type="text" placeholder="0" class="form-control" />
                                        </div>

                                        <button type="submit" class="btn btn-primary" onclick="addItem();">Add</button>
                                        <button type="reset" class="btn btn-danger">Reset</button>

                                    </form>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-7">
                                    <div class="form-group" style="border: 1px solid #dddddd; margin-top: 22px; font-size: 12px;">
                                        <div style="height: 200px; overflow-y: scroll;" >
                                            <table class="table table-responsive" style="font-size: 12px;" id="tbl_regional">
                                                <thead>
                                                    <tr>
                                                        <th>N#</th>
                                                        <th>Item</th>
                                                        <th>Unit Price</th>
                                                        <th>Amount</th>
                                                        <th><i class="fa fa-bolt" aria-hidden="true"></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="4"><li class="fa fa-level-up"></li> Find branch with box above!</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>

                                    </div>

                                        <div class="form-group">
                                            <hr>
                                            <button class="btn btn-primary" onclick="getTotalPrice();">Calculate Total Price</button>
                                        </div>
                                        <div class="form-group" style="font-size: 12px;">

                                            <form method="post" enctype="multipart/form-data" action="<?php echo base_url('checkin/check_out').'/'.$this->uri->segment(3)?>">
                                                <div class="row">
                                                    <div class="col-sm-3 form-group">
                                                        <label>Exchange Rate(KHR):</label>
                                                        <input type="text" id="exchange" name="exchange" required class="form-control" value="4000" onblur="getTotalPrice();">
                                                    </div>
                                                    <div class="col-sm-3 form-group">
                                                        <label>Discount(%):</label>
                                                        <input type="text" value="0" required name="discount" id="discount" class="form-control" onblur="getTotalPrice();">
                                                    </div>
                                                    <div class="col-sm-3 form-group">
                                                        <label>Grand Total($):</label>
                                                        <input type="text" id="total_usd" name="total_usd" required disabled class="form-control">
                                                        <input type="hidden" name="total_" id="total_">
                                                    </div>
                                                    <div class="col-sm-3 form-group">
                                                        <label>Grand Total(KHR):</label>
                                                        <input type="text" id="total_khr" required disabled class="form-control">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-12 form-group">
                                                        <button class="btn btn-success" type="submit">Ok Check Out</button>
                                                        <a href="<?php echo base_url('home')?>" class="btn btn-danger">Go to Home</a>
                                                    </div>
                                                </div>

                                            </form>

                                        </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>


                        <!-- /.box-body -->
                        <div class="box-footer">
                            <!--<i>Regional Information form</i>-->
                        </div>
                    </div>


                    <!-- /. box -->
                </div>
                <!-- /.col -->

            </div>


        </section>
        <!-- /.content -->
</div>
<!-- /.content-wrapper -->