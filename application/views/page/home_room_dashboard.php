<script type="text/javascript">

    function search_room_by_date(interval='0')
    {
        $.ajax({
            type: "post",
            url: "<?php echo base_url('home/getRoomByDate')?>",
            data: {
                intv:interval
            },
            dataType: "text",
            success: function (data) {
                location.reload();
            }
        });
    }

    function serch_room_today()
    {
        $.ajax({
            type: "post",
            url: "<?php echo base_url('home/getRoomToday')?>",
            success: function (data) {
                location.reload();
            }
        });
    }

</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">


    <!-- Main content -->
    <section class="content">


        <section class="content" style="padding:0; margin:0;">
            <div class="row" >
                <div class="col-md-12">
                    <div class="box box-danger" style="border:0; box-shadow: none;">
                        <div class="box-body no-padding">
                            <!-- THE CALENDAR -->
                            <div id="calendar" class="fc fc-ltr fc-unthemed">
                                <div class="fc-toolbar" style="font-size: 11px;">
                                    <div class="fc-left">
                                        <div class="fc-button-group">
                                            <button type="button" class="fc-prev-button fc-button fc-state-default fc-corner-left" onclick="search_room_by_date('-1')">
                                                <span class="fc-icon fc-icon-left-single-arrow"></span>
                                            </button>
                                            <button type="button" class="fc-next-button fc-button fc-state-default fc-corner-right" onclick="search_room_by_date('1')">
                                                <span class="fc-icon fc-icon-right-single-arrow"></span>
                                            </button>
                                        </div>
                                        <button type="button" class="fc-today-button fc-button fc-state-default fc-corner-left fc-corner-right fc-state-disabled" onclick="serch_room_today()">Today</button>
                                        <span style="font-size: 12px; padding-top: 3px;">Look up room for <?php echo date_format(date_create($_SESSION['LOOKUP_DATE'][0]['DATE']),'d-M-Y');?> </span>
                                    </div>
                                    
                                    <div class="fc-right">
                                        <div><div style="width: 17px; height: 17px; background:#3c8dbc; border: 1px solid #000; border-radius: 2px; float: left;"></div><span>&nbsp;Available</span></div>
                                        <div><div style="width: 17px; height: 17px; background:#cebd01; border: 1px solid #000; border-radius: 2px; float: left;"></div><span>&nbsp;Booking</span></div>
                                        <div><div style="width: 17px; height: 17px; background:#00a65a; border: 1px solid #000; border-radius: 2px; float: left;"></div><span>&nbsp;In used</span></div>
                                    </div>
                                    <div class="fc-clear"></div>
                                </div>
                                <div class="fc-view-container">
                                    <div class="fc-view fc-agendaDay-view fc-agenda-view">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td class="fc-widget-content">

                                                    <hr class="fc-widget-header">
                                                    <div class="fc-time-grid-container fc-scroller" style="height: 520px;">
                                                        <div class="fc-time-grid">
                                                            <div class="fc-bg">
                                                                <table>
                                                                    <tbody>
                                                                    <tr>
                                                                        <td class="fc-axis fc-widget-content" style="width: 42px;"></td>
                                                                        <td class="fc-day fc-widget-content fc-fri fc-today fc-state-highlight" data-date="2016-09-02"></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="fc-slats">
                                                                <table>
                                                                    <tbody>



                                                                            <tr style=""><td class="fc-axis fc-time fc-widget-content" style="width: 42px; text-align: center; font-weight: bold;">Floor</td><td class="fc-widget-content" align="left" style="font-weight: bold; background: #FFF;">&nbsp;&nbsp;Room List</td></tr>
                                                                            <?php
                                                                            $fid=0;
                                                                            foreach($floor->result() as $row)
                                                                            { $fid++;
                                                                            ?>

                                                                            <tr style="height: 80px;">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width: 42px; text-align: center; background: #fcf8e3;"><?php echo $row->FLOOR_NAME;?></td>
                                                                                <td class="fc-widget-content" style="background: #FFF;">
                                                                                    <?php
                                                                                    $CI =& get_instance();
                                                                                    $getrm=$CI->getRoom($row->FLOOR_ID);
                                                                                    foreach($getrm->result() as $row_rom)
                                                                                    {
                                                                                        echo '<div class="room" style="';
                                                                                        echo ($row_rom->ROOM_STATUS !='In Used' ? ($row_rom->ROOM_STATUS =='Booking' ? "background: url(".base_url('assets/dist/img/commons/booking.png').") 0px 0px no-repeat;" : "background: url(".base_url('assets/dist/img/commons/available.png').") 0px 0px no-repeat;") : "background: url(".base_url('assets/dist/img/commons/inused.png').") 0px 0px no-repeat;" );
                                                                                        echo '" onclick="tooltip.pop(this, '.'\'#'.$row_rom->ROOM_ID.'\''.');" ><span class="room-name">'.$row_rom->ROOM_NAME.'</span><div class="room-no">Nº:'.$row_rom->ROOM_CODE.'</div><div class="booking">'.($row_rom->AMOUNT >0?$row_rom->AMOUNT.' Booked':'').'</div></div>';
                                                                                        echo '<div style="display:none;"><div id="'.$row_rom->ROOM_ID.'"><h4>'.$row_rom->ROOM_CODE.'-'.$row_rom->ROOM_NAME.'</h4><ul style="list-style:none; margin-left:15px; padding:0;"><a href="'.base_url('home/checkinFromDashborad').'/'.$row_rom->ROOM_ID.'" class="'.($row_rom->ROOM_STATUS=='In Used'?"disabled":"").'"><li><i class="fa fa-sign-in"></i> Check In</li></a><a href="'.base_url('checkin/checkin_service').'/'.$row_rom->CHECKIN_ID.'" class="'.($row_rom->ROOM_STATUS=='In Used'?"":"disabled").'"><li><i class="fa fa-sign-out"></i> Check Out</li></a><a href="'.base_url('home/bookFromDashboard').'/'.$row_rom->ROOM_ID.'"><li><i class="fa fa-inbox"></i> Book</li></a><a href="'.base_url('checkin/checkin_service').'/'.$row_rom->CHECKIN_ID.'" class="'.($row_rom->ROOM_STATUS=='In Used'?"":"disabled").'"><li><i class="fa fa-plus-circle"></i> Add Service</li></a><a href="#"><li>See details for a week</li></a></ul></div></div>';
                                                                                    }
                                                                                    ?>
                                                                                    </td>
                                                                            </tr>

                                                                            <?php
                                                                            }
                                                                            ?>



                                                                    <tr style=""><td class="fc-axis fc-time fc-widget-content" style="width: 42px; text-align: center; font-weight: bold;"></td><td class="fc-widget-content" align="center" style="font-weight: bold; background: #FFF;"></td></tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>

                                                            <hr class="fc-widget-header" style="display: none;">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>



                            <!-- Modal -->
                            <div class="modal fade" id="myModal" role="dialog" style="position: absolute; z-index: 100000;">
                                <div class="modal-dialog" style="width: 250px;">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header bg-primary">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title" style="color:#FFF;">Modal Header</h4>
                                        </div>
                                        <div class="modal-body">
                                            <ul style="list-style: none;">
                                                <li><a href="#" class="ach-in">Check In</a></li>
                                                <li><a href="#" class="ach-out">Check Out</a></li>
                                                <li><a href="#" class="vdt ach-info">View Details about </a></li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Model -->



                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /. box -->
                </div>
                <!-- /.col -->
            </div>
        </section>
        <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
    /*$('.room').dblclick(function(){
        $('.modal-title').html($(this).attr('data-code')+' | '+$(this).attr('data-name'));
        $('.vdt').html('View Detail about '+$(this).attr('data-name'));
        $('ach-in')
        $(".ach-in").attr("href", "<?php echo base_url('home/checkin').'/'?>"+$(this).attr('data-id'));
        $(".ach-out").attr("href", "<?php echo base_url('home/checkout').'/'?>"+$(this).attr('data-id'));
    });*/



</script>