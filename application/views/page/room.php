<script>
    $(document).ready(function(){
        $("#search_box").keyup(function () {
            $.ajax({
                type: "post",
                url: "<?php echo base_url('room/get_room')?>",
                data: {
                    key:$('#search_box').val()
                },
                dataType: "json",
                success: function (data) {
                    $('#tbl_regional tbody tr').remove();
                    $('#tbl_regional tbody tr').slideDown('slow');
                    var rg_no=0;
                    if(data.length==0)
                    {
                        $('#tbl_regional tbody').append('<tr><td colspan="4"><img src="<?php echo base_url('assets/dist/img/commons/loading.gif');?>" width="25px" height="25px"> &nbsp; No Item found! </td></tr>')
                    }
                    $.each(data, function (key, value) {
                        rg_no+=1;
                        $('#tbl_regional tbody').append(
                            '<tr><td width="5%">'+rg_no+'</td><td width="25%">'+value['ROOM_CODE']+'</td width="40%"><td>'+value['ROOM_NAME']+'</td><td><button style="padding: 0 6px;" class="btn btn-primary" onclick="edit_room(\''+value['ROOM_ID']+'\',\''+value['FLOOR_ID']+'\',\''+value['ROOM_CODE']+'\',\''+value['RTYPE_ID']+'\',\''+value['ROOM_NAME']+'\');"><i class="fa fa-pencil"></i></a></button> | <button style="padding: 0 6px;" class="btn btn-danger" onclick="delete_room(\''+value['ROOM_ID']+'\')"><i class="fa fa-times"></i></a></button></td></tr>')
                    });
                }
            });
        });
    });

    function edit_room(id, fid, code, rtype, name)
    {
        document.getElementById('rm_id').value=id;
        document.getElementById('floor').value=fid;
        document.getElementById('rm_code').value=code;
        document.getElementById('rtm_id').value=rtype;
        document.getElementById('rm_name').value=name;
    }

    function delete_room(id)
    {
        cfm = confirm('Are you sure you delete this row?');
        if(cfm==true)
        {
            $.ajax({
                type: "post",
                url: "<?php echo base_url('room/delete_room')?>/"+id,
                success: function (data) {
                    location.reload();
                }
            });
        }
    }

</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" >


    <!-- Main content -->
    <section class="content">


        <?php echo $this->session->userdata('msg'); ?>

        <section class="content" style="padding:0; margin:0;height: 100%;">
            <div class="row" style="height: 100%;">
                <div class="col-md-12">
                    <div class="box box-default" style="border: 1px solid #dddddd; box-shadow: none;">
                        <div class="box-header" style="border-bottom: 1px solid #ddd; background: #fafafa; color:#3c8dbc;">
                            <b>Room</b>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-5" style="font-size: 12px;">
                                    <form method="post" enctype="multipart/form-data" action="<?php echo base_url('room/add_room')?>" >

                                        <div class="form-group">
                                            <label>Floor</label>
                                            <select class="form-control" required name="floor" id="floor">
                                                <?php
                                                foreach($floor->result() as $row)
                                                {
                                                    echo '<option value="'.$row->FLOOR_ID.'">'.$row->BRANCH_NAME.' -> '.$row->FLOOR_NAME.'</option>';
                                                }
                                                ?>
                                            </select>
                                            <input type="hidden" name="rm_id" id="rm_id" >
                                        </div>

                                        <div class="form-group">
                                            <label>Room Type</label>
                                            <select class="form-control" required name="rtm_id" id="rtm_id">
                                                <?php
                                                foreach($roomtype->result() as $row)
                                                {
                                                    echo '<option value="'.$row->RTYPE_ID.'">'.$row->RTYPE_NAME.'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Room Code</label>
                                            <input class="form-control" required type="text" placeholder="Room Code..." name="rm_code" id="rm_code" />
                                        </div>

                                        <div class="form-group">
                                            <label>Room Name</label>
                                            <input class="form-control" required type="text" placeholder="Room Name..." name="rm_name" id="rm_name" />
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Save</button>
                                            <button type="reset" class="btn btn-danger">Reset</button>
                                        </div>
                                    </form>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-7">
                                    <div class="form-group" style="border: 1px solid #dddddd; margin-top: 22px; font-size: 12px;">
                                        <input type="text" id="search_box" class="form-control" placeholder="Search room..." style="font-size: 12px; border-width: 0 0 1px 0;">
                                        <div style="height: 249px; overflow-y: scroll;" >
                                            <table class="table table-responsive" style="font-size: 12px;" id="tbl_regional">
                                                <thead>
                                                    <tr>
                                                        <th>N#</th>
                                                        <th>Code</th>
                                                        <th>Name</th>
                                                        <th><i class="fa fa-bolt" aria-hidden="true"></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="4"><li class="fa fa-level-up"></li> Find room with box above!</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>

                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <!--<i>Regional Information form</i>-->
                        </div>
                    </div>
                    <!-- /. box -->
                </div>
                <!-- /.col -->
            </div>
        </section>
        <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
    $(document).ready(function(){

            $.ajax({
                type: "post",
                url: "<?php echo base_url('room/get_room')?>",
                data: {
                    key:$('#search_box').val()
                },
                dataType: "json",
                success: function (data) {
                    $('#tbl_regional tbody tr').remove();
                    $('#tbl_regional tbody tr').slideDown('slow');
                    var rg_no=0;
                    if(data.length==0)
                    {
                        $('#tbl_regional tbody').append('<tr><td colspan="4"><img src="<?php echo base_url('assets/dist/img/commons/loading.gif');?>" width="25px" height="25px"> &nbsp; No Item found! </td></tr>')
                    }
                    $.each(data, function (key, value) {
                        rg_no+=1;
                        $('#tbl_regional tbody').append(
                            '<tr><td width="5%">'+rg_no+'</td><td width="25%">'+value['ROOM_CODE']+'</td width="40%"><td>'+value['ROOM_NAME']+'</td><td><button style="padding: 0 6px;" class="btn btn-primary" onclick="edit_room(\''+value['ROOM_ID']+'\',\''+value['FLOOR_ID']+'\',\''+value['ROOM_CODE']+'\',\''+value['RTYPE_ID']+'\',\''+value['ROOM_NAME']+'\');"><i class="fa fa-pencil"></i></a></button> | <button style="padding: 0 6px;" class="btn btn-danger" onclick="delete_room(\''+value['ROOM_ID']+'\')"><i class="fa fa-times"></i></a></button></td></tr>')
                    });
                }
            });
    });
</script>