<script>
    $(document).ready(function(){
        $("#search_box").keyup(function () {
            $.ajax({
                type: "post",
                url: "<?php echo base_url('employee/get_employee')?>",
                data: {
                    key:$('#search_box').val()
                },
                dataType: "json",
                success: function (data) {
                    $('#tbl_regional tbody tr').remove();
                    $('#tbl_regional tbody tr').slideDown('slow');
                    var rg_no=0;
                    if(data.length==0)
                    {
                        $('#tbl_regional tbody').append('<tr><td colspan="4"><img src="<?php echo base_url('assets/dist/img/commons/loading.gif');?>" width="25px" height="25px"> &nbsp; No Item found! </td></tr>')
                    }
                    $.each(data, function (key, value) {
                        rg_no+=1;
                        $('#tbl_regional tbody').append(
                            '<tr><td width="5%">'+rg_no+'</td><td width="25%">'+value['EMP_CODE']+'</td width="40%"><td>'+value['EMP_NAME']+'</td><td><button style="padding: 0 6px;" class="btn btn-primary" onclick="edit_employee(\''+value['EMP_ID']+'\',\''+value['EMP_CODE']+'\',\''+value['EMP_NAME']+'\',\''+value['GENDER']+'\',\''+value['TEL']+'\',\''+value['BRANCH_ID']+'\',\''+value['ADDRESS']+'\');"><i class="fa fa-pencil"></i></a></button> | <button style="padding: 0 6px;" class="btn btn-danger" onclick="delete_employee(\''+value['EMP_ID']+'\')"><i class="fa fa-times"></i></a></button></td></tr>')
                    });
                }
            });
        });
    });

    function edit_employee(id, empcode, empname, gender, tel, branch, addr)
    {
        document.getElementById("emp_id").value=id;
        document.getElementById('emp_code').value=empcode;
        document.getElementById('emp_name').value=empname;
        document.getElementById('gender').value=gender;
        document.getElementById('tel').value = tel;
        document.getElementById('branch').value=branch;
        document.getElementById('address').value=addr;
    }

    function delete_employee(id)
    {
        cfm = confirm('Are you sure you delete this row?');
        if(cfm==true)
        {
            $.ajax({
                type: "post",
                url: "<?php echo base_url('employee/delete_employee')?>/"+id,
                success: function (data) {
                    location.reload();
                }
            });
        }
    }

</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" >


    <!-- Main content -->
    <section class="content">


        <?php echo $this->session->userdata('msg'); ?>

        <section class="content" style="padding:0; margin:0;height: 100%;">
            <div class="row" style="height: 100%;">
                <div class="col-md-12">
                    <div class="box box-default" style="border: 1px solid #dddddd; box-shadow: none;">
                        <div class="box-header" style="border-bottom: 1px solid #ddd; background: #fafafa; color:#3c8dbc;">
                            <b>Employee Information</b>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-5" style="font-size: 12px;">
                                    <form method="post" enctype="multipart/form-data" action="<?php echo base_url('employee/add_employee')?>" >

                                        <div class="form-group">
                                            <label>Employee ID</label>
                                            <input class="form-control" required type="text" placeholder="Employee Code..." name="emp_code" id="emp_code" />
                                            <input type="hidden" name="emp_id" id="emp_id" />
                                        </div>

                                        <div class="form-group">
                                            <label>Employee Name</label>
                                            <input class="form-control" type="text" placeholder="Employee Name..." name="emp_name" id="emp_name" />
                                        </div>

                                        <div class="form-group">
                                            <label>Gender</label>
                                            <select class="form-control" required name="gender" id="gender">
                                                <option value="M">Male</option>
                                                <option value="F">Female</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Tel</label>
                                            <input class="form-control" maxlength="10" type="text" placeholder="Contact..." name="tel" id="tel" onkeypress="return isNumberKey(event)" />
                                        </div>

                                        <div class="form-group">
                                            <label>Assign to Branch</label>
                                            <select class="form-control" required name="branch" id="branch">
                                                <?php
                                                foreach($branch->result() as $row)
                                                {
                                                    echo '<option value="'.$row->BRANCH_ID.'">'.$row->BRANCH_NAME.'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Address</label>
                                            <textarea class="form-control" name="address" id="address" placeholder="Employee address..."></textarea>
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Save</button>
                                            <button type="reset" class="btn btn-danger">Reset</button>
                                        </div>
                                    </form>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-7">
                                    <div class="form-group" style="border: 1px solid #dddddd; margin-top: 22px; font-size: 12px;">
                                        <input type="text" id="search_box" class="form-control" placeholder="Search employee..." style="font-size: 12px; border-width: 0 0 1px 0;">
                                        <div style="height: 390px; overflow-y: scroll;" >
                                            <table class="table table-responsive" style="font-size: 12px;" id="tbl_regional">
                                                <thead>
                                                    <tr>
                                                        <th>N#</th>
                                                        <th>Code</th>
                                                        <th>Name</th>
                                                        <th><i class="fa fa-bolt" aria-hidden="true"></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="4"><li class="fa fa-level-up"></li> Find employee with box above!</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>

                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <!--<i>Regional Information form</i>-->
                        </div>
                    </div>
                    <!-- /. box -->
                </div>
                <!-- /.col -->
            </div>
        </section>
        <!-- /.content -->
</div>
<!-- /.content-wrapper -->