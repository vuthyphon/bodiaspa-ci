
<!-- This is what you need -->
<script src="<?php echo base_url('assets')?>/dist/dist/sweetalert.js"></script>
<link rel="stylesheet" href="<?php echo base_url('assets')?>/dist/dist/sweetalert.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" >


    <!-- Main content -->
    <section class="content">


        <?php echo $this->session->userdata('msg'); ?>

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Booking and Reservation</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool btn-default" href="#demo" data-toggle="collapse">Add <i class="fa fa-plus"></i></button>
                    <!--<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>-->
                </div>
            </div><!-- /.box-header -->
            <div class="box-body collapse" id="demo">
                <div class="row">
                    <form method="post" action="<?php echo base_url('booking/booking_post')?>">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Customer</label>
                           <input type="text" id="customer" class="form-control" name="customer" placeholder="ឈ្មោះអតិថិជន">
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="text" class="form-control" id="phone" name="phone" required>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label>Booking Date</label>
                            <input type="text" class="form-control datepicker" name="booking_date" required>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Booking Time</label>
                            <input type="text" class="form-control timepicker" name="booking_time" required>
                        </div><!-- /.form-group -->

                        <div class="form-group">
                            <label>Duration</label>
                            <select name="duration" class="form-control">
                                <option value="15">15 Minutes</option>
                                <option value="30">30 Minutes</option>
                                <option value="45">45 Minutes</option>
                                <option value="60">60 Minutes</option>
                                <option value="90">90 Minutes</option>
                                <option value="120">120 Minutes</option>
                            </select>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label>Number of peoples</label>
                            <input type="number" value="1" class="form-control" name="no_person" >
                        </div><!-- /.form-group -->

                    </div><!-- /.col -->

                    <div class="col-md-4">

                        <div class="form-group">
                            <label>Booking Room</label>
                            <select class="form-control" name="room" id="room">
                                <option value="">Chose Room</option>
                                <?php foreach($rooms as $row_room) { ?>
                                    <option value="<?php echo $row_room->ROOM_ID?>"><?php echo $row_room->ROOM_CODE.' '.$row_room->ROOM_NAME;?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Therapist</label>
                            <select class="form-control" name="therapist" id="therapist">
                                <option value="">--Select Therapist--</option>
                                <?php
                                foreach($therapists->result() as $row)
                                {
                                    echo '<option value="'.$row->EMP_ID.'">'.$row->EMP_NAME.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label></label>
                            <div class="clear"></div>
                            <button class="btn btn-primary pull-right"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </div><!-- /.col -->
                    </form><!--form-->
                </div><!-- /.row -->
            </div><!-- /.box-body -->
            <!--<div class="box-footer">
                Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about the plugin.
            </div>-->
        </div><!-- /.box -->

        <section class="content" style="padding:0; margin:0;height: 100%;">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Booking waiting to check-in</h3>

                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive padding" style="min-height: 400px;">
                            <table class="table table-bordered booking" id="eventsTable"

                                   >
                                <thead>
                                    <tr>

                                        <th>Code</th>
                                        <th>Customer</th>
                                        <th>Phone Number</th>
                                        <th>Booking Date</th>
                                        <th>Time</th>
                                        <th>Duration</th>
                                        <th>No Peoples</th>
                                        <th>Room</th>
                                        <th>Therapist</th>
                                        <th>Action</th>

                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($booking_info as $book):?>
                                    <tr>

                                        <td><?php echo $book->spa_code?></td>
                                        <td><?php echo $book->cus_name?></td>
                                        <td><?php echo $book->tel?></td>
                                        <td><?php echo strtotime($book->date_treatment)>0 ? date('d-M-Y',strtotime($book->date_treatment)):''?></td>
                                        <td><?php echo $book->booking_time?></td>
                                        <td><?php echo $book->duration?> minutes</td>
                                        <td><?php echo $book->no_person?></td>
                                        <td><?php echo $book->ROOM_NAME?></td>
                                        <td><?php echo $book->EMP_NAME?></td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-sm btn-default  dropdown-toggle" data-toggle="dropdown">
                                                    <i class="fa fa-pencil"></i> <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right " role="menu">
                                                    <li><a href="#" data-toggle="modal" data-target="#myModal" onclick="edit_booking(<?php echo $book->spa_id;?>)"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a></li>
                                                    <li><a href="#" onclick="delete_booking(<?php echo $book->spa_id;?>)"><i class="fa fa-ban" aria-hidden="true"></i> Cancel</a></li>
                                                    <li><a href="<?php echo base_url('checkin/book_checkin/'.$book->spa_id)?>"><i class="fa fa-check-square-o" aria-hidden="true"></i> Check In</a></li>
                                                </ul>
                                            </div>
                                        </td>

                                    </tr>
                                <?php endforeach;?>
                                </tbody>



                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
            </div>



        </section>
        <!-- /.content -->
</div>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <form method="post" action="<?php echo base_url('booking/booking_put')?>">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Booking</h4>
            </div>
            <div class="modal-body" style="min-height: 300px">

                    <div class="col-md-4">
                        <div class="form-group">

                            <label>Customer</label>
                            <input type="hidden" id="ed_spa_id" name="spa_id">
                            <select class="form-control select2" id="ed_customer" name="customer"  style="width: 100%;" required>
                                <option selected="selected">Alabama</option>
                                <option>Alaska</option>
                                <option>California</option>
                                <option>Delaware</option>
                                <option>Tennessee</option>
                                <option>Texas</option>
                                <option>Washington</option>
                            </select>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="text" class="form-control" id="ed_phone" name="phone" required>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label>Booking Date(mm/dd/yyyy)</label>
                            <input type="text" class="form-control datepicker" placeholder="mm/dd/yyyy" name="booking_date" id="ed_booking_date" required>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Booking Time</label>
                            <input type="text" class="form-control timepicker" placeholder="hh:mn" name="booking_time" id="ed_booking_time" required>
                        </div><!-- /.form-group -->

                        <div class="form-group">
                            <label>Duration</label>
                            <select name="duration" class="form-control" id="ed_duration">
                                <option value="15">15 Minutes</option>
                                <option value="30">30 Minutes</option>
                                <option value="45">45 Minutes</option>
                                <option value="60">60 Minutes</option>
                                <option value="90">90 Minutes</option>
                                <option value="120">120 Minutes</option>
                            </select>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label>Number of peoples</label>
                            <input type="number" value="1" class="form-control" name="no_person" id="ed_no_person" >
                        </div><!-- /.form-group -->

                    </div><!-- /.col -->

                    <div class="col-md-4">

                        <div class="form-group">
                            <label>Booking Room</label>
                            <select class="form-control" name="room" id="ed_room">
                                <option value="">Chose Room</option>
                                <?php foreach($rooms as $row_room) { ?>
                                    <option value="<?php echo $row_room->ROOM_ID?>"><?php echo $row_room->ROOM_CODE.' '.$row_room->ROOM_NAME;?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Therapist</label>
                            <select class="form-control" name="therapist" id="ed_therapist">
                                <option value="">--Select Therapist--</option>
                                <?php
                                foreach($therapists->result() as $row)
                                {
                                    echo '<option value="'.$row->EMP_ID.'">'.$row->EMP_NAME.'</option>';
                                }
                                ?>
                            </select>
                        </div>

                    </div><!-- /.col -->

            </div>
            <div class="modal-footer">
                <button class="btn btn-primary pull-right"><i class="fa fa-save"></i> Save</button>
            </div>
            </form><!--form-->
        </div>

    </div>
</div>

<script src="<?php echo base_url('assets');?>/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url('assets');?>/bootstrap-timepicker/css/bootstrap-timepicker.min.css">

<script src="<?php echo base_url('assets');?>/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url('assets');?>/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url('assets');?>/plugins/input-mask/jquery.inputmask.extensions.js"></script>

<link rel="stylesheet" href="<?php echo base_url('assets');?>/plugins/datepicker/datepicker3.css">
<script src="<?php echo base_url('assets');?>/plugins/datepicker/bootstrap-datepicker.js"></script>



<script>
    //Timepicker
    /*$(".timepicker").timepicker({
        showInputs: false
    });*/

    $('.timepicker').timepicker();

    $(".datepicker").datepicker({dateFormat: 'yy/mm/dd'});
</script>

<script>
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

</script>

<script>
    $(document).ready(function(){
        $("#search_box").keyup(function () {
            $.ajax({
                type: "post",
                url: "<?php echo base_url('booking/get_booking')?>",
                data: {
                    key:$('#search_box').val()
                },
                dataType: "json",
                success: function (data) {
                    $('#tbl_regional tbody tr').remove();
                    $('#tbl_regional tbody tr').slideDown('slow');
                    var rg_no=0;
                    if(data.length==0)
                    {
                        $('#tbl_regional tbody').append('<tr><td colspan="4"><img src="<?php echo base_url('assets/dist/img/commons/loading.gif');?>" width="25px" height="25px"> &nbsp; No Item found! </td></tr>')
                    }
                    $.each(data, function (key, value) {
                        rg_no+=1;
                        $('#tbl_regional tbody').append(
                            '<tr><td width="5%">'+rg_no+'</td><td width="25%">'+value['BOOKING_ID']+'</td width="40%"><td>'+value['CUS_NAME']+'</td><td><button style="padding: 0 6px;" class="btn btn-primary" onclick="edit_booking(\''+value['BOOKING_ID']+'\',\''+value['CUS_NAME']+'\',\''+value['GENDER']+'\',\''+value['TEL']+'\',\''+value['AGENCY']+'\',\''+value['DATE_IN']+'\');"><i class="fa fa-pencil"></i></a></button> | <button style="padding: 0 6px;" class="btn btn-danger" onclick="delete_booking(\''+value['BOOKING_ID']+'\')"><i class="fa fa-times"></i></a></button></td></tr>')
                    });
                }
            });
        });


    });



</script>

<script>
    function edit_booking(spa_id) {
        $.ajax({
            type: "post",
            url: "<?php echo base_url('booking/get_booking/')?>/"+spa_id,
            dataType: "json",
            success: function (data) {
                $("#ed_customer").val(data.cus_name);
                $("#ed_phone").val(data.tel);
                $("#ed_booking_date").val(data.book_date);
                $("#ed_booking_time").val(data.start_time);
                $("#ed_no_person").val(data.no_person);
                $("#ed_duration").val(data.duration);
                $("#ed_therapist").val(data.emp_id);
                $("#ed_room").val(data.room_id);
                $("#ed_spa_id").val(data.spa_id);

            }
        });
    }
</script>
<!-- DataTables -->
<script>
    function delete_booking(spa_id)
    {
        swal({
                title: "លុបការកក់?",
                text: "ទិន្នន័យលុបហើយ មិនអាចយកមកវិញបានទេ!!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-danger',
                confirmButtonText: 'លុបបាន!',
                cancelButtonText: "បោះបង់!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){
                if (isConfirm){
                    window.location="<?php echo base_url('booking/booking_cancel')?>/"+spa_id;

                } else {
                    swal("Cancelled", "អ្នកបានបោះបង់ការលុប", "error",2000,false);
                }
            });
    }
</script>



<script src="<?php echo base_url('assets')?>/lib/jquery/jquery.js"></script>
<script src="<?php echo base_url('assets')?>/lib/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url('assets')?>/lib/jquery.dataTables.js"></script>
<link rel="stylesheet" href="<?php echo base_url('assets')?>/lib/dataTables.bootstrap.css">
<script>
    $("#eventsTable").dataTable();
</script>