<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" >
    <!-- Main content -->
    <section class="content">


        <?php echo $this->session->userdata('msg'); ?>

        <section class="content" style="padding:0; margin:0;height: 100%;">
            <div class="row" style="height: 100%;">
                <div class="col-md-12">
                    <form method="post" action="<?php echo base_url('checkin/checkin_reservation')?>">
                    <div class="box box-default" style="border: 1px solid #dddddd; box-shadow: none;">
                        <div class="box-header" style="border-bottom: 1px solid #ddd; background: #fafafa; color:#3c8dbc;">
                            <b>Check In</b>
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">
                            <div class="row">

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Booking Code</label>
                                            <input type="hidden" value="<?php echo $booking->spa_id?>" name="spa_id">
                                            <input type="text" id="customer" value="<?php echo $booking->spa_code;?>" readonly class="form-control" name="booking_code" placeholder="ឈ្មោះអតិថិជន">
                                        </div><!-- /.form-group -->
                                        <div class="form-group">
                                            <label>Customer</label>
                                            <input type="text" id="customer" value="<?php echo $booking->cus_name;?>" class="form-control" name="customer" placeholder="ឈ្មោះអតិថិជន">
                                        </div><!-- /.form-group -->
                                        <div class="form-group">
                                            <label>Checkin Date</label>
                                            <input type="text" class="form-control datepicker" value="<?php echo $booking->checkin_date?>" name="checkin_date" required>
                                        </div><!-- /.form-group -->
                                        <div class="form-group">
                                            <label>Room</label>
                                            <select class="form-control" name="room" id="room">
                                                <option value="">Chose Room</option>
                                                <?php foreach($rooms as $row_room) { ?>
                                                    <option value="<?php echo $row_room->ROOM_ID?>" <?php echo $row_room->ROOM_ID==$booking->room_id ? 'selected':''?>><?php echo $row_room->ROOM_CODE.' '.$row_room->ROOM_NAME;?></option>
                                                <?php } ?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Therapist</label>
                                            <select class="form-control" name="therapist" id="therapist">
                                                <option value="">--Select Therapist--</option>
                                                <?php
                                                foreach($therapists->result() as $row){?>

                                                    <option value="<?php echo $row->EMP_ID;?>" <?php echo $row->EMP_ID==$booking->emp_id ? 'selected':''?> > <?php echo $row->EMP_NAME;?></option>;
                                                <?php }
                                                ?>
                                            </select>
                                        </div>

                                    </div><!-- /.col -->
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="box">
                                                    <div class="box-header">
                                                        <h3 class="box-title">Add Services</h3>

                                                    </div><!-- /.box-header -->
                                                    <div class="box-body table-responsive padding">
                                                        <table class="table table-bordered booking">
                                                            <thead>
                                                            <tr>
                                                                <th>Services</th>
                                                                <th>Duration</th>
                                                                <th>Qty</th>
                                                                <th>Unit Price</th>
                                                                <th>Total Amount</th>
                                                                <th><button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal">Add Services <i class="fa fa-plus"></i> </button> </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="all_services">
                                                                <?php if(isset($checkin_detail)){
                                                                    foreach ($checkin_detail as $chk){?>
                                                                <tr>
                                                                    <td><?php echo $chk->service_name;?></td>
                                                                    <td><?php echo $chk->duration;?></td>
                                                                    <td><?php echo $chk->qty;?></td>
                                                                    <td><?php echo $chk->unit_price;?></td>
                                                                    <td><?php echo $chk->qty * $chk->unit_price;?></td>
                                                                    <td><button type='button'><i class='fa fa-remove'></i> </button></td>
                                                                </tr>
                                                                <?php
                                                                    }
                                                                }?>
                                                            </tbody>



                                                        </table>
                                                    </div><!-- /.box-body -->
                                                </div><!-- /.box -->
                                            </div>
                                        </div>
                                    </div><!-- /.col -->


                            </div><!-- /.row -->
                        </div>


                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button class="btn-primary btn pull-right" ><i class="fa fa-check-circle"></i>  Check In</button>
                        </div>

                    </div>
                    </form><!--form-->

                    <!-- /. box -->
                </div>
                <!-- /.col -->

            </div>
        </section>
        <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript" src="<?php echo base_url('assets'); ?>/plugins/daterangepicker/moment.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url('assets'); ?>/plugins/daterangepicker/daterangepicker.css" />
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/plugins/daterangepicker/daterangepicker.js"></script>

<script>
    //var start_date = new Date().increment('day', 7);
    //alert(start_date);
    // alert(start_date);
    var currentdate = new Date();
    var datetime = currentdate.getFullYear()+"/"+ (currentdate.getMonth()+1) + "/" +currentdate.getDate()+" " + currentdate.getHours() + ":" + currentdate.getMinutes();
    $('input[name="checkin_date"]').daterangepicker({
        autoUpdateInput: false,
        "singleDatePicker": true,
        "timePicker": true,
        locale: {
            cancelLabel: 'Clear',
            "format": "YYYY/MM/DD HH:mm"

        },
        "startDate":datetime
    });

    $('input[name="checkin_date"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD HH:mm'));
    });

    $('input[name="checkin_date"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });


</script>


<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <form id="frm_services" method="post">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body" style="min-height: 300px;">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Services</label>
                        <select name="services" id="service_id" class="form-control">
                            <!--<option value="">Chose Service</option>-->
                            <?php foreach ($services as $ser){
                                echo "<option value='{$ser->SERVICE_ID}'>$ser->SERVICE_NAME</option>";
                            }?>
                        </select>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label>Duration</label>
                        <select name="duration" id="duration" class="form-control">
                            <option value="15">15 Minute</option>
                            <option value="30">30 Minute</option>
                            <option value="60" selected>60 Minute</option>
                            <option value="90">90 Minute</option>
                            <option value="120">120 Minute</option>
                        </select>
                    </div><!-- /.form-group -->

                    <div class="form-group">
                        <label>Qty</label>
                        <input type="number" name="qty" id="qty" value="1" class="form-control">
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <label>Unit Price</label>
                        <input type="number" name="unit_price" id="unit_price" class="form-control">
                    </div><!-- /.form-group -->
                    <input type="hidden" value="<?php echo $booking->spa_id;?>" id="spa_id" name="spa_id">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="add_service">Save</button>
            </div>
        </form>
        </div>

    </div>
</div>

<script>
    $("#add_service").click(function () {
        var frm_data=$("#frm_services").serializeArray();
        var service_name=$("#service_id option:selected").text();
        var service_id=$("#service_id").val();
        var duration=$("#duration").val();
        var qty=$("#qty").val();
        var unit_price=$("#unit_price").val();
        var total_amount = qty * unit_price;
        $.ajax({
            type: "post",
            url: "<?php echo base_url('checkin/add_temp_service/')?>",
            data:frm_data,
            success: function (data) {
                var tbl="<tr id='row_"+service_id+"'>" +
                        "<td>"+service_name+"</td>"+
                        "<td>"+duration+"</td>"+
                        "<td>"+qty+"</td>"+
                        "<td>"+unit_price+"</td>"+
                        "<td>"+total_amount+"</td>"+
                        "<td><button type='button' onclick='remove_temp_service("+service_id+")'><i class='fa fa-remove'></i> </button> </td>"+
                    "</tr>";

                $("#all_services").append(tbl);
                $('#myModal').modal('hide');
                $("#frm_services").trigger('reset');
            }
        });
    });
</script>

<script>
    function remove_temp_service(spa_id) {
        $.ajax({
            type: "post",
            url: "<?php echo base_url('checkin/remove_temp_service/')?>/" + spa_id,
            success: function (data) {
                $("#row_" + spa_id).remove();
            }
        });
    }
</script>