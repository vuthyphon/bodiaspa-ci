<script>
    $(document).ready(function(){
        $("#search_box").keyup(function () {
            $.ajax({
                type: "post",
                url: "<?php echo base_url('user/get_user')?>",
                data: {
                    key:$('#search_box').val()
                },
                dataType: "json",
                success: function (data) {
                    $('#tbl_regional tbody tr').remove();
                    $('#tbl_regional tbody tr').slideDown('slow');
                    var rg_no=0;
                    if(data.length==0)
                    {
                        $('#tbl_regional tbody').append('<tr><td colspan="4"><img src="<?php echo base_url('assets/dist/img/commons/loading.gif');?>" width="25px" height="25px"> &nbsp; No Item found! </td></tr>')
                    }
                    $.each(data, function (key, value) {
                        rg_no+=1;
                        $('#tbl_regional tbody').append(
                            '<tr><td width="5%">'+rg_no+'</td><td width="25%">'+value['FULLNAME']+'</td width="40%"><td>'+value['LOGINNAME']+'</td><td><button style="padding: 0 6px;" class="btn btn-primary" onclick="edit_user(\''+value['US_ID']+'\',\''+value['FULLNAME']+'\',\''+value['STAFF_ID']+'\',\''+value['LOGINNAME']+'\',\''+value['BRANCH_ID']+'\',\''+value['RULE_ID']+'\');"><i class="fa fa-pencil"></i></a></button> | <button style="padding: 0 6px;" class="btn btn-danger" onclick="delete_user(\''+value['US_ID']+'\')"><i class="fa fa-times"></i></a></button></td></tr>')
                    });
                }
            });
        });
    });

    function edit_user(id, fname, stid, logname, brid, rule)
    {
        document.getElementById("upassword").placeholder="ds";
        document.getElementById('uid').value=id;
        document.getElementById('fname').value=fname;
        document.getElementById('stid').value=stid;
        document.getElementById('usname').value = logname;
        document.getElementById('branch').value=brid;
        document.getElementById('rule').value=rule;
    }

    function delete_user(id)
    {
        cfm = confirm('Are you sure you delete this row?');
        if(cfm==true)
        {
            $.ajax({
                type: "post",
                url: "<?php echo base_url('user/delete_user')?>/"+id,
                success: function (data) {
                    location.reload();
                }
            });
        }
    }

</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" >


    <!-- Main content -->
    <section class="content">


        <?php echo $this->session->userdata('msg'); ?>

        <section class="content" style="padding:0; margin:0;height: 100%;">
            <div class="row" style="height: 100%;">
                <div class="col-md-12">
                    <div class="box box-default" style="border: 1px solid #dddddd; box-shadow: none;">
                        <div class="box-header" style="border-bottom: 1px solid #ddd; background: #fafafa; color:#3c8dbc;">
                            <b>User Information</b>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-5" style="font-size: 12px;">
                                    <form method="post" enctype="multipart/form-data" action="<?php echo base_url('user/add_user')?>" >

                                        <div class="form-group">
                                            <label>Staff Fullname</label>
                                            <input class="form-control" required type="text" placeholder="FullName..." name="fname" id="fname" />
                                            <input type="hidden" name="uid" id="uid" />
                                        </div>

                                        <div class="form-group">
                                            <label>Staff ID</label>
                                            <input class="form-control" type="text" placeholder="Staff ID..." name="stid" id="stid" />
                                        </div>

                                        <div class="form-group">
                                            <label>Username (account name)</label>
                                            <input class="form-control" required type="text" placeholder="Username..." name="usname" id="usname" />
                                        </div>

                                        <div class="form-group">
                                            <label>Password</label>
                                            <input class="form-control" required type="password" placeholder="Password..." name="upassword" id="upassword" />
                                        </div>

                                        <div class="form-group">
                                            <label>Assign to Branch</label>
                                            <select class="form-control" required name="branch" id="branch">
                                                <?php
                                                foreach($branch->result() as $row)
                                                {
                                                    echo '<option value="'.$row->BRANCH_ID.'">'.$row->BRANCH_NAME.'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Assign to Rule</label>
                                            <select class="form-control" required name="rule" id="rule">
                                                <?php
                                                foreach($rule->result() as $row)
                                                {
                                                    echo '<option value="'.$row->RULE_ID.'">'.$row->RULE_NAME.'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>


                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Save</button>
                                            <button type="reset" class="btn btn-danger">Reset</button>
                                        </div>
                                    </form>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-7">
                                    <div class="form-group" style="border: 1px solid #dddddd; margin-top: 22px; font-size: 12px;">
                                        <input type="text" id="search_box" class="form-control" placeholder="Search user..." style="font-size: 12px; border-width: 0 0 1px 0;">
                                        <div style="height: 353px; overflow-y: scroll;" >
                                            <table class="table table-responsive" style="font-size: 12px;" id="tbl_regional">
                                                <thead>
                                                    <tr>
                                                        <th>N#</th>
                                                        <th>Code</th>
                                                        <th>Name</th>
                                                        <th><i class="fa fa-bolt" aria-hidden="true"></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="4"><li class="fa fa-level-up"></li> Find user with box above!</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>

                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <!--<i>Regional Information form</i>-->
                        </div>
                    </div>
                    <!-- /. box -->
                </div>
                <!-- /.col -->
            </div>
        </section>
        <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
    $(document).ready(function(){

            $.ajax({
                type: "post",
                url: "<?php echo base_url('user/get_user')?>",
                data: {
                    key:$('#search_box').val()
                },
                dataType: "json",
                success: function (data) {
                    $('#tbl_regional tbody tr').remove();
                    $('#tbl_regional tbody tr').slideDown('slow');
                    var rg_no=0;
                    if(data.length==0)
                    {
                        $('#tbl_regional tbody').append('<tr><td colspan="4"><img src="<?php echo base_url('assets/dist/img/commons/loading.gif');?>" width="25px" height="25px"> &nbsp; No Item found! </td></tr>')
                    }
                    $.each(data, function (key, value) {
                        rg_no+=1;
                        $('#tbl_regional tbody').append(
                            '<tr><td width="5%">'+rg_no+'</td><td width="25%">'+value['FULLNAME']+'</td width="40%"><td>'+value['LOGINNAME']+'</td><td><button style="padding: 0 6px;" class="btn btn-primary" onclick="edit_user(\''+value['US_ID']+'\',\''+value['FULLNAME']+'\',\''+value['STAFF_ID']+'\',\''+value['LOGINNAME']+'\',\''+value['BRANCH_ID']+'\',\''+value['RULE_ID']+'\');"><i class="fa fa-pencil"></i></a></button> | <button style="padding: 0 6px;" class="btn btn-danger" onclick="delete_user(\''+value['US_ID']+'\')"><i class="fa fa-times"></i></a></button></td></tr>')
                    });
                }
            });

    });
</script>