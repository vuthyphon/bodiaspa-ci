<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title;?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url('assets');?>/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url('assets');?>/font-awesome-4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="<?php echo base_url('assets');?>/plugins/fullcalendar/fullcalendar.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets');?>/plugins/fullcalendar/fullcalendar.print.css" media="print">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('assets');?>/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url('assets');?>/dist/css/skins/_all-skins.min.css">

    <link rel="stylesheet" href="<?php echo base_url('assets');?>/dist/css/tooltip.css">
    <!--datepicker-->


    <style>
        .room{
            /*height: 78px;*/
            height:82px;
            /*width: 126px;*/
            width:115px;
            /*border: 1px solid #d7d7d7;*/
            float: left;
            margin: 1px 0 1px 1px;
            color:#FFF;
        }
        .room .room-name{font-size: 11px; margin: 11px 0 0 10px; float: left; cursor: default;}
        .room .room-no{ width: 115px; font-size: 13px; float: left; text-align: center; background: #CC0000; margin-top:8.5%; font-weight: bold; cursor: default;}
        .room .booking{ height: 20px; float: left; font-size: 11px; margin-top: 6%; width: 95%; text-align: right; padding: 1px 5% 0 0;}
        .smbox
        {
            height: 25px;
            font-size: 12px;
            padding: 0 10px;
            color:#000;
            margin: 0;
        }
    </style>

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url('assets');?>/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url('assets');?>/bootstrap/js/bootstrap.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php echo base_url('assets');?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url('assets');?>/plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url('assets');?>/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url('assets');?>/dist/js/demo.js"></script>
    <script src="<?php echo base_url('assets');?>/dist/js/my.js"></script>
    <script src="<?php echo base_url('assets')?>/dist/js/tooltip.js"></script>

    <![endif]-->

    <script>
        $(document).ready(function(){
            $('.disabled').click(function(e){
                e.preventDefault();
            });
        });
    </script>

</head>
<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">

    <header class="main-header">

        <!-- Logo -->
        <a href="index2.html" class="logo" style="background:#0B0E0F; display:none;">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">Bodia Spa</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"></span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo base_url('assets');?>/dist/img/commons/anonym.png" class="user-image" alt="User Image">
                            <span class="hidden-xs"><?php echo $this->session->userdata('LOGINNAME')?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="<?php echo base_url('logout');?>" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
                    <li class="dropdown messages-menu">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Booking on">
                            <i class="fa fa-bookmark"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header" style="font-size: 13px;"></li>
                            <li>
                                <!-- inner menu: contains the actual data -->


                                <ul class="menu">
                                    <li><!-- start message -->
                                        <a href="eck in now" class="">
                                            <div class="pull-left text-center">
                                                <i class="fa fa-bookmark"></i>
                                            </div>
                                            <h4>

                                                <small><i class="fa fa-clock-o"></i></small>
                                            </h4>
                                            <p>Room Reserved:</p>
                                        </a>
                                    </li><!-- end message -->

                                </ul>

                            </li>
                            <li class="footer"><a href="#">See All</a></li>
                        </ul>
                    </li>
                    <!-- Notifications: style can be found in dropdown.less -->
                    <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Old Booking not check in">
                            <i class="fa fa-history"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header" style="font-size: 12px;">Booking not yet check in</li>

                            <li class="footer"><a href="#">View all</a></li>
                        </ul>
                    </li>


                    <!-- Notifications: style can be found in dropdown.less -->
                    <li class="dropdown notifications-menu">

                    </li>



                </ul>
            </div>
        </nav>
    </header>

    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->

            <!-- System's logo -->
            <div class="user-panel" style="background:#222d32;">
                <div style="text-align:center;">
                    <img src="<?php echo base_url('assets');?>/dist/img/bodia-logo2.png" class="img-responsive" alt="User Image" >
                </div>
            </div>









            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">

                <li class="header">MODULES</li>

                <?php
                    foreach($par_menu->result() as $row)
                    {
                ?>

                    <li class="<?php echo ($row->LEVEL=='P')?'treeview':'';?> <?php echo ($row->URL==$this->uri->segment(1))?' active':'';?>">
                        <a href="<?php echo ($row->LEVEL=='P')?'#':base_url($row->URL);?>">
                            <i class="<?php echo $row->ICON?>"></i>
                            <span><?php echo $row->MODULE_NAME;?></span>
                            <span class="pull-right-container">
                                <?php echo ($row->LEVEL=='P')?'<i class="fa fa-angle-left pull-right"></i>':'';?>
                            </span>
                        </a>

                        <?php
                        if($row->LEVEL=='P') {
                            $CI =& get_instance();
                            $getChild = $CI->getChildMenu($row->MODULE_ID);
                            echo '<ul class="treeview-menu">';
                            foreach ($getChild->result() as $row_child) {
                                echo '<li><a href="' . base_url($row_child->URL) . '"><i class="fa fa-angle-right"></i>' . $row_child->MODULE_NAME . '</a></li>';
                            }
                            echo '</ul>';
                        }
                        ?>

                    </li>

                <?php
                    }

                ?>







            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- header -->