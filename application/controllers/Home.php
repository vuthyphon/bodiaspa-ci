<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH . 'controllers/My_Controller.php');
class Home extends My_Controller {


    function __construct()
    {
        parent::__construct();
        $this->load->model('m_general');
        $this->load->model('m_reservation');
    }

	public function index()
	{
        if($this->session->userdata('RULE_ID')=='')
        {
            redirect(base_url('login'));
        }
		$data['main_page']='home';
        $data['booking']=$this->get_all_booking();
        $data['par_menu'] = $this->m_general->getParent();
        $data['title']='Discussion board | Home';
		$this->load->view('templates/template',$data);
      //  print_r($data['booking']);
	}

    function bookFromDashboard($room_id)
    {
        $_SESSION['BOOKING_ROOM']['0'] = array(
            'ROOM_ID' =>$room_id
        );
        redirect('booking');
    }

    function checkinFromDashborad($room_id)
    {
        $_SESSION['CHECKIN_ROOM']['0'] = array(
            'ROOM_ID' =>$room_id
        );
        redirect('checkin');
    }

    function checkinFromBookingNotif($par)
    {
        $TMPVAL=explode("SPACE",$par);
        $booking_id=$TMPVAL[0];
        $room_id=$TMPVAL[1];
        $_SESSION['EXISTING_BOOK']['0'] = array(
            'BOOKING_ID' =>$booking_id,
            'ROOM_ID' =>$room_id
        );

        $_SESSION['CHECKIN_ROOM']['0'] = array(
            'ROOM_ID' =>$room_id
        );

        redirect('checkin');
    }

    function getChildMenu($parent)
    {
        return $this->m_general->getChild($parent);
    }

    function getRoom($floor)
    {
        //$new_date=($date=='')?date('Y-m-d'):date_format(date_create($date),'Y-m-d');
        return $this->m_general->getRoom($this->session->userdata('BRANCH_ID'),$floor,$_SESSION['LOOKUP_DATE'][0]['DATE']);
    }

    function getRoomByDate()
    {
        $date1=date_create($_SESSION['LOOKUP_DATE'][0]['DATE']);
        $date2=date_create(date('Y-m-d'));
        $diff=date_diff($date2,$date1);
        if($diff->format("%R%a")>=0)
        {
            $old_date=date_add(date_create($_SESSION['LOOKUP_DATE'][0]['DATE']),date_interval_create_from_date_string("{$this->input->post('intv')} days"));
            $_SESSION['LOOKUP_DATE']['0']=array(
                'DATE'=>date_format($old_date,"Y-m-d")
            );
        }
        else
        {
            $_SESSION['LOOKUP_DATE']['0']=array(
                'DATE'=>date('Y-m-d')
            );
        }
    }

    function getRoomToday()
    {
        $_SESSION['LOOKUP_DATE']['0']=array(
            'DATE'=>date("Y-m-d")
        );
    }

}


