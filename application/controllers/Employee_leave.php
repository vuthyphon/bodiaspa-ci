<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Employee_leave extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_employeeleave');
    }

    public function index()
    {
        $data['main_page']='employeeleave';
        $data['title']='Bodiaspa | Employee Leave';
        $data['employee'] = $this->m_employeeleave->get_employee(1);
        $data['booking_notif'] = $this->m_general->getCurrentBooking($this->session->userdata('BRANCH_ID'), $_SESSION['LOOKUP_DATE'][0]['DATE']);
        $data['getOldBooking'] = $this->m_general->getOldBooking($this->session->usredata('BRANCH_ID'),date('Y-m-d'));
        $data['should_checkout'] = $this->m_general->checkInYesterday($this->session->userdata('BRANCH_ID'),date('Y-m-d'));
        $this->load->view('templates/template',$data);
    }

    function add_employeeleave()
    {
        if($this->input->post('leaveid')=='' || $this->input->post('leaveid')==null)
        {
            $data = array(
                'EMP_ID' => $this->input->post('emp_id'),
                'LEAVE_FR' => date_format(date_create($this->input->post('frdt')),'Y-m-d'),
                'LEAVE_TO' => date_format(date_create($this->input->post('todt')),'Y-m-d'),
                'NO_OF_DAY' => $this->input->post('no_day'),
                'REASON' => $this->input->post('reason')
            );
            $this->m_employeeleave->add_employeeleave($data);
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%; right: 0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-save"></i>&nbsp;You are append about 1 row!</div>');
        }
        else
        {
            $data = array(
                'EMP_ID' => $this->input->post('emp_id'),
                'LEAVE_FR' => date_format(date_create($this->input->post('frdt')),'Y-m-d'),
                'LEAVE_TO' => date_format(date_create($this->input->post('todt')),'Y-m-d'),
                'NO_OF_DAY' => $this->input->post('no_day'),
                'REASON' => $this->input->post('reason')
            );
            $this->m_employeeleave->update_employeeleave($this->input->post('leaveid'), $data);
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%;  right:0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-save"></i>&nbsp;You are update about 1 row!</div>');
        }

        redirect('employee_leave');
    }

    function delete_employeeleave($id)
    {
        $data = array(
            'D_STATUS' => 'D'
        );
        $this->m_employeeleave->update_employeeleave($id,$data);
        $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%;  right:0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-trash-o"></i>&nbsp;1 Row deleted successful!</div>');
    }

    function get_employeeleave()
    {
        $key = $this->input->post('key');
        $data = $this->m_employeeleave->get_employeeleave($key);
        echo json_encode($data);
    }

}


