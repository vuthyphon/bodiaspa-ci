<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Employee extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_employee');
        $this->load->model('m_general');
    }

    public function index()
    {
        if($this->session->userdata('RULE_ID')=='')
        {
            redirect(base_url('login'));
        }
        $data['main_page']='employee';
        $data['title']='Bodiaspa | Employee';
        $data['branch'] = $this->m_employee->get_branch();
        $data['par_menu'] = $this->m_general->getParent();
        $data['booking_notif'] = $this->m_general->getCurrentBooking($this->session->userdata('BRANCH_ID'), $_SESSION['LOOKUP_DATE'][0]['DATE']);
        $data['getOldBooking'] = $this->m_general->getOldBooking($this->session->usredata('BRANCH_ID'),date('Y-m-d'));
        $data['should_checkout'] = $this->m_general->checkInYesterday($this->session->userdata('BRANCH_ID'),date('Y-m-d'));
        $this->load->view('templates/template',$data);
    }

    function add_employee()
    {
        if($this->input->post('emp_id')=='' || $this->input->post('emp_id')==null)
        {
            $data = array(
                'EMP_CODE' => $this->input->post('emp_code'),
                'EMP_NAME' => $this->input->post('emp_name'),
                'GENDER' => $this->input->post('gender'),
                'TEL' => $this->input->post('tel'),
                'BRANCH_ID' => $this->input->post('branch'),
                'ADDRESS' => $this->input->post('address')
            );
            $this->m_employee->add_employee($data);
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%; right: 0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-save"></i>&nbsp;You are append about 1 row!</div>');
        }
        else
        {
            $data = array(
                'EMP_CODE' => $this->input->post('emp_code'),
                'EMP_NAME' => $this->input->post('emp_name'),
                'GENDER' => $this->input->post('gender'),
                'TEL' => $this->input->post('tel'),
                'BRANCH_ID' => $this->input->post('branch'),
                'ADDRESS' => $this->input->post('address')
            );
            $this->m_employee->update_employee($this->input->post('emp_id'), $data);
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%;  right:0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-save"></i>&nbsp;You are update about 1 row!</div>');
        }

        redirect('employee');
    }

    function delete_employee($id)
    {
        $data = array(
            'D_STATUS' => 'D'
        );
        $this->m_employee->update_employee($id,$data);
        $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%;  right:0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-trash-o"></i>&nbsp;1 Row deleted successful!</div>');
    }

    function get_employee()
    {
        $key = $this->input->post('key');
        $data = $this->m_employee->get_employee($key);
        echo json_encode($data);
    }

    function getChildMenu($parent)
    {
        return $this->m_general->getChild($parent);
    }

}


