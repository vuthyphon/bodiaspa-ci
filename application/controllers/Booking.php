<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH . 'controllers/My_Controller.php');

class Booking extends My_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_general');
        $this->load->model('m_reservation');
        $this->load->model('m_booking');
        $this->load->model('m_agent');
    }

    public function index()
    {
        if($this->session->userdata('RULE_ID')=='')
        {
            redirect(base_url('login'));
        }
        $data['main_page']='booking';
        $data['title']='Discussion board | Branch';
        $data['par_menu'] = $this->m_general->getParent();
        $data['therapists'] = $this->m_booking->getTherapist($this->session->userdata('BRANCH_ID'));
        $data['rooms']=$this->get_all_room_by_branch($this->session->userdata('BRANCH_ID'));
        $data['booking_info']=$this->m_booking->get_booking_branch($this->session->userdata('BRANCH_ID'));
        $this->load->view('templates/template',$data);
    }

    function booking_post()
    {
        $code=$this->generate_code();
        $data=array('spa_code' => $code,'cus_name' => $this->input->post('customer'),'tel' => $this->input->post('phone'),'e_mail' => NULL,'date_treatment' => $this->convert_date($this->input->post('booking_date')),'duration'=>$this->input->post('duration')
        ,'start_time' => $this->convert_time($this->input->post('booking_time')),'no_person' => $this->input->post('no_person'),'room_id' => $this->input->post('room'),'emp_id' => $this->input->post('therapist'),'record_type' => 'BOOKING','record_date' => date('Y-m-d'),'branch_id' => $this->session->userdata('BRANCH_ID')
        ,'bookby' => $this->session->userdata('US_ID'));

       $this->db->insert('spa_data',$data);
       $this->redirect_page(base_url('booking'),"Record saved","success");
    }

    function booking_put()
    {
        $data=array('cus_name' => $this->input->post('customer'),'tel' => $this->input->post('phone'),'e_mail' => NULL,'date_treatment' => $this->convert_date($this->input->post('booking_date')),'duration'=>$this->input->post('duration')
        ,'start_time' => $this->convert_time($this->input->post('booking_time')),'no_person' => $this->input->post('no_person'),'room_id' => $this->input->post('room'),'emp_id' => $this->input->post('therapist'),'record_type' => 'BOOKING','record_date' => date('Y-m-d'),'branch_id' => $this->session->userdata('BRANCH_ID')
        ,'bookby' => $this->session->userdata('US_ID'));

        // echo $this->convert_time($this->input->post('booking_time'));

        //print_r($data);
        $this->db->where('spa_id',$this->input->post('spa_id'));
        $this->db->update('spa_data',$data);
        //echo $this->db->last_query();
        $this->redirect_page(base_url('booking'),"Record updated","success");
    }

    function booking_cancel($spa_id){
        $this->db->where('spa_id',$spa_id);
        $data=array('d_status'=>'D');
        $this->db->update('spa_data',$data);
        //echo $this->db->last_query();
        $this->redirect_page(base_url('booking'),"Booking has been cancel","success");
    }

    function delete_booking($id)
    {
        $data = array(
            'D_STATUS' => 'DELETED'
        );
        $this->m_booking->update_booking($id,$data);
        $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%;  right:0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-warning"></i>&nbsp;1 Row deleted successful!</div>');
        //redirect('regional');
    }

    function get_booking($spa_id)
    {
        $this->input->post('key');
        $data = $this->m_booking->get_booking($spa_id);
        echo json_encode($data);
    }

    function getChildMenu($parent)
    {
        return $this->m_general->getChild($parent);
    }

}


