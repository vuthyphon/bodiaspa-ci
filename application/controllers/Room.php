<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Room extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_general');
        $this->load->model('m_room');
    }

    public function index()
    {
        if($this->session->userdata('RULE_ID')=='')
        {
            redirect(base_url('login'));
        }
        $data['main_page']='room';
        $data['title']='Discussion board | Room';
        $data['booking_notif'] = $this->m_general->getCurrentBooking($this->session->userdata('BRANCH_ID'), $_SESSION['LOOKUP_DATE'][0]['DATE']);
        $data['should_checkout'] = $this->m_general->checkInYesterday($this->session->userdata('BRANCH_ID'),date('Y-m-d'));
        $data['getOldBooking'] = $this->m_general->getCurrentBooking($this->session->userdata('BRANCH_ID'),date('Y-m-d'));
        $data['floor'] = $this->m_room->get_floor();
        $data['par_menu'] = $this->m_general->getParent();
        $data['roomtype'] = $this->m_room->get_roomtype();
        $this->load->view('templates/template',$data);
    }

    function add_room()
    {
        if($this->input->post('rm_id')=='' || $this->input->post('rm_id')==null)
        {
            $data = array(
                'FLOOR_ID' => $this->input->post('floor'),
                'ROOM_CODE' => $this->input->post('rm_code'),
                'RTYPE_ID' => $this->input->post('rtm_id'),
                'ROOM_NAME' => $this->input->post('rm_name')
            );
            $this->m_room->add_room($data);
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%; right: 0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-save"></i>&nbsp;You are append about 1 row!</div>');
        }
        else
        {
            $data = array(
                'FLOOR_ID' => $this->input->post('floor'),
                'ROOM_CODE' => $this->input->post('rm_code'),
                'RTYPE_ID' => $this->input->post('rtm_id'),
                'ROOM_NAME' => $this->input->post('rm_name')
            );
            $this->m_room->update_room($this->input->post('rm_id'),$data);
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%;  right:0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-save"></i>&nbsp;You are update about 1 row!</div>');
        }

        redirect('room');
    }

    function delete_room($id)
    {
        $data = array(
            'D_STATUS' => 'D'
        );
        $this->m_room->update_room($id,$data);
        $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%;  right:0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-trash-o"></i>&nbsp;1 Row deleted successful!</div>');
    }

    function get_room()
    {
        $key = $this->input->post('key');
        $data = $this->m_room->get_room($key);
        echo json_encode($data);
    }

    function getChildMenu($parent)
    {
        return $this->m_general->getChild($parent);
    }

}


