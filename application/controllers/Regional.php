<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Regional extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_regional');
        $this->load->model('m_general');
    }

    public function index()
    {
        if($this->session->userdata('RULE_ID')=='')
        {
            redirect(base_url('login'));
        }
        $data['main_page']='regional';
        $data['title']='Discussion board | Home';
        $data['booking_notif'] = $this->m_general->getCurrentBooking($this->session->userdata('BRANCH_ID'), $_SESSION['LOOKUP_DATE'][0]['DATE']);
        $data['should_checkout'] = $this->m_general->checkInYesterday($this->session->userdata('BRANCH_ID'),date('Y-m-d'));
        $data['getOldBooking'] = $this->m_general->getOldBooking($this->session->userdata('BRANCH_ID'),date('Y-m-d'));
        $data['par_menu'] = $this->m_general->getParent();
        $this->load->view('templates/template',$data);
    }

    function add_regional()
    {
        if($this->input->post('rg_id')=='' || $this->input->post('rg_id')==null)
        {
            $data = array(
                'REGIONAL_CODE' => $this->input->post('rg_code'),
                'REGIONAL_NAME' => $this->input->post('rg_name'),
                'REGIONAL_DES' => $this->input->post('rg_des')
            );
            $this->m_regional->add_regional($data);
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%; right: 0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-warning"></i>&nbsp;You are append about 1 row!</div>');
        }
        else
        {
            $data = array(
                'REGIONAL_CODE' => $this->input->post('rg_code'),
                'REGIONAL_NAME' => $this->input->post('rg_name'),
                'REGIONAL_DES' => $this->input->post('rg_des')
            );
            $this->m_regional->update_regional($this->input->post('rg_id'),$data);
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%;  right:0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-warning"></i>&nbsp;You are update about 1 row!</div>');
        }

        redirect('regional');
    }

    function delete_regional($id)
    {
        $data = array(
            'D_STATUS' => 'D'
        );
        $this->m_regional->update_regional($id,$data);
        $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%;  right:0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-warning"></i>&nbsp;1 Row deleted successful!</div>');
        //redirect('regional');
    }

    function get_regional()
    {
        $key = $this->input->post('key');
        $data = $this->m_regional->get_regional($key);
        echo json_encode($data);
    }

    function getChildMenu($parent)
    {
        return $this->m_general->getChild($parent);
    }

}


