<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Service extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_service');
        $this->load->model('m_general');
    }

    public function index()
    {
        if($this->session->userdata('RULE_ID')=='')
        {
            redirect(base_url('login'));
        }
        $data['main_page']='service';
        $data['title']='Discussion board | Service';
        $data['par_menu'] = $this->m_general->getParent();
        $data['booking_notif'] = $this->m_general->getCurrentBooking($this->session->userdata('BRANCH_ID'), $_SESSION['LOOKUP_DATE'][0]['DATE']);
        $data['should_checkout'] = $this->m_general->checkInYesterday($this->session->userdata('BRANCH_ID'),date('Y-m-d'));
        $data['getOldBooking'] = $this->m_general->getOldBooking($this->session->userdata('BRANCH_ID'),date('Y-m-d'));
        $this->load->view('templates/template',$data);
    }

    function add_service()
    {
        if($this->input->post('sv_id')=='' || $this->input->post('sv_id')==0)
        {
            $data = array(
                'SERVICE_NAME' => $this->input->post('sv_name'),
                'UNIT' => $this->input->post('unit'),
                'UNIT_PRICE' => $this->input->post('unit_price'),
                'DESCR' => $this->input->post('sv_des')
            );
            $this->m_service->add_service($data);
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%; right: 0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-save"></i>&nbsp;You are append about 1 row!</div>');
        }
        else
        {
            $data = array(
                'SERVICE_NAME' => $this->input->post('sv_name'),
                'UNIT' => $this->input->post('unit'),
                'UNIT_PRICE' => $this->input->post('unit_price'),
                'DESCR' => $this->input->post('sv_des')
            );
            $this->m_service->update_service($this->input->post('sv_id'),$data);
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%;  right:0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-save"></i>&nbsp;You are update about 1 row!</div>');
        }

        redirect('service');
    }

    function delete_service($id)
    {
        $data = array(
            'D_STATUS' => 'D'
        );
        $this->m_service->update_service($id,$data);
        $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%;  right:0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-trash-o"></i>&nbsp;1 Row deleted successful!</div>');
    }

    function get_service()
    {
        $key = $this->input->post('key');
        $data = $this->m_service->get_service($key);
        echo json_encode($data);
    }

    function getChildMenu($parent)
    {
        return $this->m_general->getChild($parent);
    }

}


