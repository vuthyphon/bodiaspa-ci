<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Room_type extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_roomtype');
    }

    public function index()
    {
        if($this->session->userdata('RULE_ID')=='')
        {
            redirect(base_url('login'));
        }
        $data['main_page']='roomtype';
        $data['title']='Discussion board | Room Type';
        $this->load->view('templates/template',$data);
    }

    function add_roomtype()
    {
        if($this->input->post('rtm_id')=='' || $this->input->post('rtm_id')==null)
        {
            $data = array(
                'RTYPE_NAME' => $this->input->post('rtm_name')
            );
            $this->m_roomtype->add_roomtype($data);
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%; right: 0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-floppy-o"></i>&nbsp;You are append about 1 row!</div>');
        }
        else
        {
            $data = array(
                'RTYPE_NAME' => $this->input->post('rtm_name')
            );
            $this->m_roomtype->update_roomtype($this->input->post('rtm_id'),$data);
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%;  right:0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-floppy-o"></i>&nbsp;You are update about 1 row!</div>');
        }

        redirect('room_type');
    }

    function delete_roomtype($id)
    {
        $data = array(
            'D_STATUS' => 'D'
        );
        $this->m_roomtype->update_roomtype($id,$data);
        $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%;  right:0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-trash-o"></i>&nbsp;1 Row deleted successful!</div>');
    }

    function get_roomtype()
    {
        $key = $this->input->post('key');
        $data = $this->m_roomtype->get_roomtype($key);
        echo json_encode($data);
    }

}


