<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
include_once(APPPATH . 'controllers/My_Controller.php');
class Checkin extends My_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_general');
        $this->load->model('m_booking');
        $this->load->model('m_reservation');
    }

    public function index()
    {
        if($this->session->userdata('RULE_ID')=='')
        {
            redirect(base_url('login'));
        }
        $data['main_page']='checkin';
        $data['title']='Discussion board | Branch';
        $data['services']=$this->get_all_service();
        $data['par_menu'] = $this->m_general->getParent();
        $data['rooms']=$this->get_all_room_by_branch($this->session->userdata('BRANCH_ID'));
        $data['therapists'] = $this->m_booking->getTherapist($this->session->userdata('BRANCH_ID'));
        $this->load->view('templates/template',$data);
    }

    function book_checkin($spa_id)
    {
        $data['par_menu'] = $this->m_general->getParent();
        $data['rooms']=$this->get_all_room_by_branch($this->session->userdata('BRANCH_ID'));
        $data['therapists'] = $this->m_booking->getTherapist($this->session->userdata('BRANCH_ID'));
        $data['services']=$this->get_all_service();
        $data['main_page']='checkin';
        $data['booking']=$this->m_booking->get_booking($spa_id);
        $data['checkin_detail']=$this->m_booking->get_checkin_detail($spa_id);
        $data['title']='Spa | Check In';
        $this->load->view('templates/template',$data);
    }

    function add_temp_service()
    {
       $_SESSION['temp_services'][$this->input->post('services')]=array('service_id'=>$this->input->post('services'),'duration'=>$this->input->post('duration'),'qty'=>$this->input->post('qty')
        ,'unit_price'=>$this->input->post('unit_price'),'spa_id'=>$this->input->post('spa_id'));
    }

    function checkin_reservation()
    {
        $data=array('cus_name' => $this->input->post('customer'),'checkin_date' => $this->input->post('checkin_date'),
        'room_id' => $this->input->post('room'),'emp_id' => $this->input->post('therapist'),'is_checkin'=>1);
        $this->db->where('spa_id',$this->input->post('spa_id'));
        $this->db->update('spa_data',$data);
        echo $this->db->last_query();
        if(isset($_SESSION['temp_services'])){
            foreach ($_SESSION['temp_services'] as $temp){
                $dt=array('spa_id'=>$temp['spa_id'],'service_id'=>$temp['service_id'],'qty'=>$temp['qty'],'duration'=>$temp['duration'],'unit_price'=>$temp['unit_price']);
                $this->db->insert('spa_datadetail',$dt);
            }
            unset($_SESSION['temp_services']);
        }

        //$this->redirect_page(base_url('booking'),"Record has been check in","success");

    }

    function remove_temp_service($service_id)
    {
        unset($_SESSION['temp_services'][$service_id]);
    }

    function print_()
    {
        print_r($_SESSION['temp_services']);
    }



    function check_out($cid)
    {
       $this->m_booking->checkout(
                                    $this->session->userdata('BRANCH_ID'),
                                    $cid,
                                    str_replace(',','',$this->input->post('exchange')),
                                    $this->input->post('discount'),
                                    str_replace(',','',$this->input->post('total_'))
                                );
        $data=array('D_STATUS'=>'CHECK OUT');
        $this->m_booking->update_checkin($cid,$data);
    }

    /*
     * function get left menu and append it to it's parents
     */

    function getChildMenu($parent)
    {
        return $this->m_general->getChild($parent);
    }
}


