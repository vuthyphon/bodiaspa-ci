<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rulenperm extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_rulenpermission');
        $this->load->model('m_general');
    }

    public function index()
    {
        if($this->session->userdata('RULE_ID')=='')
        {
            redirect(base_url('login'));
        }
        $data['main_page']='ruleandpermission';
        $data['title']='Bodiaspa | test';
        $data['par_menu'] = $this->m_general->getParent();
        $data['rule'] = $this->m_rulenpermission->getRule();
        $data['menu_perm'] = $this->m_rulenpermission->edit_rule_perm(0);
        $data['booking_notif'] = $this->m_general->getCurrentBooking($this->session->userdata('BRANCH_ID'), $_SESSION['LOOKUP_DATE'][0]['DATE']);
        $data['getOldBooking'] = $this->m_general->getOldBooking($this->session->userdata('BRANCH_ID'),date('Y-m-d'));
        $data['should_checkout'] = $this->m_general->checkInYesterday($this->session->userdata('BRANCH_ID'),date('Y-m-d'));
        $this->load->view('templates/template',$data);
    }

    function edit($id)
    {
        if($this->session->userdata('RULE_ID')=='')
        {
            redirect(base_url('login'));
        }
        $data['main_page']='ruleandpermission';
        $data['title']='Bodiaspa | test';
        $data['par_menu'] = $this->m_general->getParent();
        $data['rule'] = $this->m_rulenpermission->getRule();
        $data['menu_perm'] = $this->m_rulenpermission->edit_rule_perm($id);
        $data['booking_notif'] = $this->m_general->getCurrentBooking($this->session->userdata('BRANCH_ID'), $_SESSION['LOOKUP_DATE'][0]['DATE']);
        $data['getOldBooking'] = $this->m_general->getOldBooking($this->session->userdata('BRANCH_ID'),date('Y-m-d'));
        $data['should_checkout'] = $this->m_general->checkInYesterday($this->session->userdata('BRANCH_ID'),date('Y-m-d'));
        $this->load->view('templates/template',$data);
    }

    function update_perm($id)
    {
        $this->m_rulenpermission->delete_rule_per($id);
        foreach($this->m_rulenpermission->edit_rule_perm($id) as $k=>$v)
        {
            if($this->input->post($v['MODULE_ID']))
            {
                $data=array(
                    'RULE_ID' =>$id,
                    'MODULE_ID' =>$v['MODULE_ID'],
                    'P_VIEW' =>1,
                    'P_ADD' =>0,
                    'P_EDIT' =>0,
                    'P_DELETE' =>0,
                    'ASSIGNER' => '#'
                );
                $this->m_rulenpermission->add_rule_perm($data);
            }
            else
            {
                $data=array(
                    'RULE_ID' =>$id,
                    'MODULE_ID' =>$v['MODULE_ID'],
                    'P_VIEW' =>0,
                    'P_ADD' =>0,
                    'P_EDIT' =>0,
                    'P_DELETE' =>0,
                    'ASSIGNER' => '#'
                );
                $this->m_rulenpermission->add_rule_perm($data);
            }
        }
        redirect(base_url('rulenperm'));
    }

    function getRuleRow($id)
    {
        return $this->m_rulenpermission->getRuleRow($id);
    }

    function getChildMenu($parent)
    {
        return $this->m_general->getChild($parent);
    }

}


