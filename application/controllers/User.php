<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_user');
        $this->load->model('m_general');
    }

    public function index()
    {
        if($this->session->userdata('RULE_ID')=='')
        {
            redirect(base_url('login'));
        }
        $data['main_page']='user';
        $data['title']='Discussion board | User';
        $data['branch'] = $this->m_user->get_branch();
        $data['par_menu'] = $this->m_general->getParent();
        $data['booking_notif'] = $this->m_general->getCurrentBooking($this->session->userdata('BRANCH_ID'), $_SESSION['LOOKUP_DATE'][0]['DATE']);
        $data['getOldBooking'] = $this->m_general->getOldBooking($this->session->userdata('BRANCH_ID'),date('Y-m-d'));
        $data['should_checkout'] = $this->m_general->checkInYesterday($this->session->userdata('BRANCH_ID'),date('Y-m-d'));
        $data['rule'] = $this->m_user->get_rule();
        $this->load->view('templates/template',$data);
    }

    function add_user()
    {
        if($this->input->post('uid')=='' || $this->input->post('uid')==null)
        {
            $us_id=$this->m_user->add_user($this->input->post('fname'),$this->input->post('stid'),$this->input->post('usname'),md5($this->input->post('upassword')));
            if(strlen($us_id)>0)
            {
                $this->m_user->usrnbr($us_id, $this->input->post('branch'),'get username from session');
                $this->m_user->usrnr($us_id, $this->input->post('rule'), 'get username from session');
            }
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%; right: 0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-save"></i>&nbsp;You are append about 1 row!</div>');
        }
        else
        {
            $data = array(
                'FULLNAME' => $this->input->post('fname'),
                'STAFF_ID' => $this->input->post('stid'),
                'LOGINNAME' => $this->input->post('usname')
            );
            $this->m_user->update_user($this->input->post('uid'), $data);
            $this->m_user->usrnbr($this->input->post('uid'), $this->input->post('branch'),'get username from session');
            $this->m_user->usrnr($this->input->post('uid'), $this->input->post('rule'), 'get username from session');
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%;  right:0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-save"></i>&nbsp;You are update about 1 row!</div>');
        }

        redirect('user');
    }

    function delete_user($id)
    {
        $data = array(
            'D_STATUS' => 'D'
        );
        $this->m_user->update_user($id,$data);
        $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%;  right:0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-trash-o"></i>&nbsp;1 Row deleted successful!</div>');
    }

    function get_user()
    {
        $key = $this->input->post('key');
        $data = $this->m_user->get_user($key);
        echo json_encode($data);
    }

    function getChildMenu($parent)
    {
        return $this->m_general->getChild($parent);
    }

}


