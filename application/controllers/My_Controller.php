<?php

/**
 * Created by PhpStorm.
 * User: Vuthy
 * Date: 17-08-2017
 * Time: 2:01 PM
 */
class My_Controller extends CI_Controller
{

    function get_all_room(){
        $this->db->where('D_STATUS','N');
        $data=$this->db->get('spa_room');
        return $data->result();
    }

    function get_all_room_by_branch($branch_id){
        $this->db->where('D_STATUS','N');
        $this->db->where('BRANCH_ID',$branch_id);
        $data=$this->db->get('spa_room');
        return $data->result();
    }

    function convert_date($date1)
    {
        $date = str_replace('-', '/', $date1);
        $date = date("Y-m-d", strtotime($date));
        return $date;
    }

    function convert_time($time)
    {
        $n_time = date( "H:i", strtotime($time) );
        //$n_time=date('T',strtotime($time));
        return $n_time;
    }

    function generate_code()
    {
        return strtotime(date('H:i'));
    }

    function redirect_page($url,$msg,$msg_type="success")
    {
        if($msg_type=='warning'){
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%;  right:0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-warning"></i>&nbsp;'.$msg.'</div>');
        }
        elseif($msg_type=='danger'){
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%;  right:0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-trash-o"></i>&nbsp;'.$msg.'</div>');
        }
        else{
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%;  right:0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-check-circle"></i>&nbsp;'.$msg.'</div>');
        }
        redirect($url);
    }

    function get_all_service()
    {
        $this->db->where('D_STATUS','N');
        $rs=$this->db->get('spa_service');
        return $rs->result();
    }

    function get_all_booking()
    {
        $sql="SELECT sp.spa_id,@bk_date:=CONCAT(DATE(sp.date_treatment),' ',sp.start_time) AS book_date,DATE_ADD(@bk_date, INTERVAL 60 MINUTE) AS end_date,
sp.cus_name,sp.tel,ro.`ROOM_NAME`

FROM spa_data sp
LEFT JOIN spa_room ro ON sp.`room_id`=ro.`ROOM_ID`
WHERE sp.record_type='BOOKING'
AND sp.is_checkin=0 AND sp.d_status='N'

UNION

SELECT sp.spa_id,@bk_date:=CONCAT(DATE(sp.date_treatment),' ',sp.start_time) AS book_date,DATE_ADD(@bk_date, INTERVAL 60 MINUTE) AS end_date,
sp.cus_name,sp.tel,ro.`ROOM_NAME`
FROM spa_data sp
LEFT JOIN spa_room ro ON sp.`room_id`=ro.`ROOM_ID`
WHERE sp.record_type='WALKIN'
AND sp.is_out=0 AND sp.d_status='N'";
        $r=$this->db->query($sql);
        $rs=$r->result_array();


       $arr=array();

        foreach($rs as $k=>$v){
            $a=array("title"=>"Customer :".$v['cus_name'] ." Room :".$v['ROOM_NAME'],"start"=>$v['book_date'],"end"=>$v['end_date'],"allDay"=> false, "backgroundColor"=>"#0073b7", //Blue
                    "borderColor"=>"#0023b7");
            array_push($arr,$a);
        }
        return json_encode($arr);

    }

}