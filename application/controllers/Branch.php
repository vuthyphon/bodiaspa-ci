<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Branch extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_branch');
        $this->load->model('m_general');
    }

    public function index()
    {
        if($this->session->userdata('RULE_ID')=='')
        {
            redirect(base_url('login'));
        }
        $data['main_page']='branch';
        $data['title']='Discussion board | Branch';
        $data['regional'] = $this->m_branch->get_regional();
        $data['par_menu'] = $this->m_general->getParent();
        $data['booking_notif'] = $this->m_general->getCurrentBooking($this->session->userdata('BRANCH_ID'), $_SESSION['LOOKUP_DATE'][0]['DATE']);
        $data['getOldBooking'] = $this->m_general->getOldBooking($this->session->userdata('BRANCH_ID'),date('Y-m-d'));
        $data['should_checkout'] = $this->m_general->checkInYesterday($this->session->userdata('BRANCH_ID'),date('Y-m-d'));
        $this->load->view('templates/template',$data);
    }

    function add_branch()
    {
        if($this->input->post('br_id')=='' || $this->input->post('br_id')==null)
        {
            $data = array(
                'REGIONAL_ID' => $this->input->post('regional'),
                'BRANCH_CODE' => $this->input->post('br_code'),
                'BRANCH_NAME' => $this->input->post('br_name'),
                'ADDRESS' => $this->input->post('br_address'),
                'ESTABLISH_DATE' => date_format(date_create($this->input->post('est_date')),'Y-m-d')
            );
            $this->m_branch->add_branch($data);
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%; right: 0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-warning"></i>&nbsp;You are append about 1 row!</div>');
        }
        else
        {
            $data = array(
                'REGIONAL_ID' => $this->input->post('regional'),
                'BRANCH_CODE' => $this->input->post('br_code'),
                'BRANCH_NAME' => $this->input->post('br_name'),
                'ADDRESS' => $this->input->post('br_address'),
                'ESTABLISH_DATE' => date_format(date_create($this->input->post('est_date')),'Y-m-d')
            );
            $this->m_branch->update_branch($this->input->post('br_id'),$data);
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%;  right:0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-warning"></i>&nbsp;You are update about 1 row!</div>');
        }

        redirect('branch');
    }

    function delete_branch($id)
    {
        $data = array(
            'D_STATUS' => 'D'
        );
        $this->m_branch->update_branch($id,$data);
        $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable" style="position: absolute; z-index: 100; width:20%;  right:0;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       <i class="icon fa fa-warning"></i>&nbsp;1 Row deleted successful!</div>');
        //redirect('regional');
    }

    function get_branch()
    {
        $key = $this->input->post('key');
        $data = $this->m_branch->get_branch($key);
        echo json_encode($data);
    }

    function getChildMenu($parent)
    {
        return $this->m_general->getChild($parent);
    }

}


