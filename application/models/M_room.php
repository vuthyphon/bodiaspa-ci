<?php defined('BASEPATH') OR exit('No direct script access allowed');
class M_room extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    function add_room($data)
    {
        $this->db->insert('spa_room',$data);
    }

    public function update_room($id, $data)
    {
        $this->db->where('ROOM_ID',$id);
        $this->db->update('spa_room',$data);
    }

    public function get_room($keyword) {
        $this->db->order_by('ROOM_ID', 'ASC');
        $cond=" D_STATUS = 'N' AND (LOWER(ROOM_CODE) LIKE '%".strtolower($keyword)."%' OR LOWER(ROOM_NAME) LIKE '%".strtolower($keyword)."%') ";
        $this->db->where($cond);
        //$this->db->like("LOWER(ROOM_CODE)", strtolower($keyword));
        //$this->db->or_like('LOWER(ROOM_NAME)', strtolower($keyword));
        $this->db->limit(10, 0);
        return $this->db->get('spa_room')->result_array();
    }

    public function get_floor()
    {
        $sql="SELECT * FROM spa_floor FL INNER JOIN spa_branch BR ON BR.BRANCH_ID = FL.BRANCH_ID WHERE BR.D_STATUS='N'";
        return $this->db->query($sql);
    }

    public function get_roomtype()
    {
        $sql="SELECT * FROM  spa_room_type RTM WHERE RTM.D_STATUS='N'";
        return $this->db->query($sql);
    }


}