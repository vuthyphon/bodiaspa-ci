<?php defined('BASEPATH') OR exit('No direct script access allowed');
class M_user extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    function add_user($fullname,$staffid='',$loginname,$password)
    {
        $return ='';
        $data=$this->db->query("SELECT FN_SPA_USER_INSERT('{$fullname}','{$staffid}','{$loginname}','{$password}') AS US_ID");
        foreach ($data->result() as $row) {
            $return = $row->US_ID;
        }
        return $return;
    }

    public function update_user($id, $data)
    {
        $this->db->where('US_ID',$id);
        $this->db->update('spa_user',$data);
    }

    function usrnbr($usid, $br, $us)
    {
        $this->db->query("CALL SP_USRNB_ASSIGN('{$usid}','{$br}','{$us}')");
    }

    function usrnr($usid, $rule, $us)
    {
        $this->db->query("CALL SP_USRNR_ASSIGN('{$usid}','{$rule}','{$us}')");
    }

    public function get_user($keyword) {
        $this->db->order_by('US_ID', 'ASC');
        $this->db->like("LOWER(FULLNAME)", strtolower($keyword));
        $this->db->or_like('LOWER(LOGINNAME)', strtolower($keyword));
        $this->db->limit(10, 0);
        return $this->db->get('spa_users')->result_array();
    }

    public function get_branch()
    {
        $sql="SELECT * FROM spa_branch BR INNER JOIN spa_regional RG ON RG.REGIONAL_ID = BR.REGIONAL_ID WHERE BR.D_STATUS='N'";
        return $this->db->query($sql);
    }

    public function get_rule()
    {
        $sql="SELECT * FROM spa_rule WHERE D_STATUS='N'";
        return $this->db->query($sql);
    }

}