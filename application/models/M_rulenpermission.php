<?php defined('BASEPATH') OR exit('No direct script access allowed');
class M_rulenpermission extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getRule()
    {
        $this->db->where('D_STATUS','N');
        $data=$this->db->get('spa_rule');
        return $data;
    }

    function getRuleRow($id)
    {
        $this->db->where('RULE_ID',$id);
        $this->db->where('D_STATUS','N');
        $data=$this->db->get('spa_rule');
        return $data->result_array();
    }

    function edit_rule_perm($id=0)
    {
        $data=$this->db->query("
                             SELECT
                              MD.MODULE_ID,
                              MD.MODULE_CODE,
                              MD.MODULE_NAME,
                              MD.URL,
                              LEVEL,
                              ICON,
                              P_VIEW
                            FROM spa_permission MD
                            LEFT JOIN
                            (
                              SELECT
                                M.MODULE_ID,
                                PM.RULE_ID,
                                PM.P_VIEW
                              FROM spa_permission AS M
                              LEFT JOIN spa_rulenperm PM
                              ON PM.MODULE_ID = M.MODULE_ID
                              WHERE PM.RULE_ID = ".$id."
                              AND M.D_STATUS='N'
                            ) RNP ON RNP.MODULE_ID = MD.MODULE_ID WHERE MD.D_STATUS='N' ORDER BY MD.MODULE_ID ASC ");
        return $data->result_array();
    }

    function add_rule_perm($data)
    {
        $this->db->insert('spa_rulenperm',$data);
    }

    function delete_rule_per($id)
    {
        $this->db->where('RULE_ID',$id);
        $this->db->delete('spa_rulenperm');
    }



}