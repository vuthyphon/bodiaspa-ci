<?php defined('BASEPATH') OR exit('No direct script access allowed');
class M_employeeleave extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    function add_employeeleave($data)
    {
        $this->db->insert('spa_employee_leave',$data);
    }

    public function update_employeeleave($id, $data)
    {
        $this->db->where('LEAVE_ID',$id);
        $this->db->update('spa_employee_leave',$data);
    }

    public function get_employeeleave($keyword) {
        return $this->db->query("SELECT
                                lv.*,
                                EMP_NAME
                                FROM spa_employee_leave lv
                                left join spa_employee emp
                                ON emp.EMP_ID = lv.EMP_ID
                                WHERE LOWER(EMP_NAME) LIKE '%".strtolower($keyword)."%'
                                ORDER BY LEAVE_FR DESC LIMIT 10
                                ")->result_array();
    }

    function get_employee($br)
    {
        $this->db->where('BRANCH_ID',$br);
        return $this->db->get('spa_employee');
    }

}