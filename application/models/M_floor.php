<?php defined('BASEPATH') OR exit('No direct script access allowed');
class M_floor extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    function add_floor($data)
    {
        $this->db->insert('spa_floor',$data);
    }

    public function update_floor($id, $data)
    {
        $this->db->where('FLOOR_ID',$id);
        $this->db->update('spa_floor',$data);
    }

    public function get_floor($keyword) {
        $this->db->order_by('FLOOR_ID', 'ASC');
        $cond=" D_STATUS='N' AND(LOWER(FLOOR_NAME) LIKE '%".strtolower($keyword)."%' OR LOWER(FLOOR_DES) LIKE '%".strtolower($keyword)."%')";
        $this->db->where($cond);
        $this->db->limit(10, 0);
        return $this->db->get('spa_floor')->result_array();
    }

    public function get_branch()
    {
        $sql="SELECT * FROM spa_branch BR INNER JOIN spa_regional RG ON RG.REGIONAL_ID = BR.REGIONAL_ID WHERE BR.D_STATUS='N'";
        return $this->db->query($sql);
    }


}