<?php defined('BASEPATH') OR exit('No direct script access allowed');
class M_roomtype extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    function add_roomtype($data)
    {
        $this->db->insert('spa_room_type',$data);
    }

    public function update_roomtype($id, $data)
    {
        $this->db->where('RTYPE_ID',$id);
        $this->db->update('spa_room_type',$data);
    }

    public function get_roomtype($keyword)
    {
        $this->db->order_by('RTYPE_ID', 'ASC');
        $this->db->where('D_STATUS','N');
        $this->db->like("LOWER(RTYPE_NAME)", strtolower($keyword));
        $this->db->limit(10, 0);
        return $this->db->get('spa_room_type')->result_array();
    }

}