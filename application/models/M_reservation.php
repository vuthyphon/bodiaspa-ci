<?php defined('BASEPATH') OR exit('No direct script access allowed');
class M_reservation extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    function add_customer($cus_name,$gender,$tel,$agen,$agentel,$nop,$dtin,$paytype,$therapist,$tuk)
    {
        $return ='';
        $data=$this->db->query("SELECT FN_SPA_BOOKING('{$cus_name}', '{$gender}', '{$tel}', {$agen}, '{$agentel}', {$nop}, '{$dtin}', '{$paytype}', {$therapist}, {$tuk}) AS BOOKING_ID");
        foreach($data->result() as $row)
        {
            $return = $row->BOOKING_ID;
        }
        return $return;
    }

    function getAgent($branch)
    {
        $this->db->where('BRANCH_ID',$branch);
        $this->db->where('D_STATUS','N');
        $data=$this->db->get('spa_agentcy');
        return $data;
    }

    function getTherapist($branch)
    {
        $this->db->where('BRANCH_ID',$branch);
        $this->db->where('D_STATUS','N');
        $data=$this->db->get('spa_employee');
        return $data;
    }

}