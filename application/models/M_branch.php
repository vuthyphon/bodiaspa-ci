<?php defined('BASEPATH') OR exit('No direct script access allowed');
class M_branch extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    function add_branch($data)
    {
        $this->db->insert('spa_branch',$data);
    }

    public function update_branch($id, $data)
    {
        $this->db->where('BRANCH_ID',$id);
        $this->db->update('spa_branch',$data);
    }

    public function get_branch($keyword) {
        $this->db->order_by('BRANCH_ID', 'ASC');

        $like=" D_STATUS = 'N' AND (LOWER(BRANCH_CODE) LIKE '%".strtolower($keyword)."%' OR LOWER(BRANCH_NAME) LIKE '%".strtolower($keyword)."%')";
        $this->db->where($like);
        $this->db->limit(10, 0);
        return $this->db->get('spa_branch')->result_array();
    }

    public function get_regional()
    {
        $this->db->where('D_STATUS','N');
        return $this->db->get('spa_regional');
    }


}