<?php defined('BASEPATH') OR exit('No direct script access allowed');
class M_employee extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    function add_employee($data)
    {
        $this->db->insert('spa_employee',$data);
    }

    public function update_employee($id, $data)
    {
        $this->db->where('EMP_ID',$id);
        $this->db->update('spa_employee',$data);
    }

    public function get_employee($keyword) {
        $this->db->order_by('EMP_ID', 'ASC');
        $this->db->like("LOWER(EMP_CODE)", strtolower($keyword));
        $this->db->or_like('LOWER(EMP_NAME)', strtolower($keyword));
        $this->db->limit(10, 0);
        return $this->db->get('spa_employee')->result_array();
    }

    public function get_branch()
    {
        $sql="SELECT * FROM spa_branch BR INNER JOIN spa_regional RG ON RG.REGIONAL_ID = BR.REGIONAL_ID WHERE BR.D_STATUS='N'";
        return $this->db->query($sql);
    }


}