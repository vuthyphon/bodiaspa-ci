<?php defined('BASEPATH') OR exit('No direct script access allowed');
class M_rule extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    function add_rule($data)
    {
        $this->db->insert('spa_rule',$data);
    }

    public function update_rule($id, $data)
    {
        $this->db->where('RULE_ID',$id);
        $this->db->update('spa_rule',$data);
    }

    public function get_rule($keyword) {
        $this->db->order_by('RULE_ID', 'ASC');
        $this->db->where('D_STATUS','N');
        $this->db->like("LOWER(RULE_NAME)", strtolower($keyword));
        $this->db->limit(10, 0);
        return $this->db->get('spa_rule')->result_array();
    }

}