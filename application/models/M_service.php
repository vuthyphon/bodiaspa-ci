<?php defined('BASEPATH') OR exit('No direct script access allowed');
class M_service extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    function add_service($data)
    {
        $this->db->insert('spa_service',$data);
    }

    public function update_service($id, $data)
    {
        $this->db->where('SERVICE_ID',$id);
        $this->db->update('spa_service',$data);
    }

    public function get_service($keyword) {
        $this->db->order_by('SERVICE_ID', 'ASC');
        $cond=" D_STATUS='N' AND(LOWER(SERVICE_NAME) LIKE '%".strtolower($keyword)."%' OR LOWER(DESCR) LIKE '%".strtolower($keyword)."%') ";
        $this->db->where($cond);
        //$this->db->like("LOWER(SERVICE_NAME)", strtolower($keyword));
        //$this->db->or_like('LOWER(DESCR)', strtolower($keyword));
        $this->db->limit(10, 0);
        return $this->db->get('spa_service')->result_array();
    }

}