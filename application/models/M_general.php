
<?php defined('BASEPATH') OR exit('No direct script access allowed');
class M_general extends CI_Model
{

    function getParent()
    {
        $data=$this->db->query("SELECT
                          RNP.MODULE_ID,
                          MODULE_CODE,
                          MODULE_NAME,
                          URL,
                          LEVEL,
                          ICON,
                          PARENT_ID
                        FROM spa_rulenperm AS RNP
                        INNER JOIN spa_rule AS RL
                        ON RL.RULE_ID = RNP.RULE_ID
                        INNER JOIN spa_permission PS
                        ON PS.MODULE_ID = RNP.MODULE_ID WHERE RL.RULE_ID=1 AND RNP.P_VIEW=1 AND PS.D_STATUS='N' AND LEVEL NOT IN('C') ORDER BY PS.MODULE_ID ASC");
        return $data;
    }

    function getChild($parent)
    {
        $data=$this->db->query("SELECT
                          RNP.MODULE_ID,
                          MODULE_CODE,
                          MODULE_NAME,
                          URL,
                          LEVEL,
                          ICON,
                          PARENT_ID
                        FROM spa_rulenperm AS RNP
                        INNER JOIN spa_rule AS RL
                        ON RL.RULE_ID = RNP.RULE_ID
                        INNER JOIN spa_permission PS
                        ON PS.MODULE_ID = RNP.MODULE_ID WHERE RL.RULE_ID=1 AND PS.D_STATUS='N' AND PARENT_ID >0 AND RNP.P_VIEW=1 AND LEVEL='C' AND PARENT_ID =".$parent." ORDER BY PS.MODULE_ID ASC");
        return $data;
    }










}