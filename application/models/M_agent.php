<?php defined('BASEPATH') OR exit('No direct script access allowed');
class M_agent extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    function add_agent($data)
    {
        $this->db->insert('spa_agentcy',$data);
    }

    public function update_agent($id, $data)
    {
        $this->db->where('AGENT_ID',$id);
        $this->db->update('spa_agentcy',$data);
    }

    public function get_agent($keyword,$branch) {
        $this->db->order_by('AGENT_ID', 'ASC');
        $like=" BRANCH_ID = ".$branch." AND D_STATUS = 'N' AND (LOWER(AGENT_NAME) LIKE '%".strtolower($keyword)."%' OR LOWER(TEL) LIKE '%".strtolower($keyword)."%')";
        $this->db->where($like);
        $this->db->limit(10, 0);
        return $this->db->get('spa_branch')->result_array();
    }


}