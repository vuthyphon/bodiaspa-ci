<?php defined('BASEPATH') OR exit('No direct script access allowed');
class M_booking extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    function get_booking_branch($branch_id)
    {
        $sql="SELECT bk.*,ro.ROOM_NAME,emp.EMP_NAME,DATE_FORMAT(bk.start_time, '%h:%i %p') as booking_time FROM spa_data bk
LEFT JOIN spa_room ro ON ro.ROOM_ID=bk.room_id
LEFT JOIN spa_employee emp ON bk.`emp_id`=emp.EMP_ID
WHERE bk.record_type='BOOKING' AND bk.branch_id={$branch_id} AND bk.d_status='N'
";

        $rs=$this->db->query($sql);
        return $rs->result();
    }

    function get_booking($spa_id)
    {
        $sql="SELECT *,DATE_FORMAT(date_treatment, '%m/%d/%Y') as book_date FROM spa_data
WHERE spa_id=$spa_id
";
        $rs=$this->db->query($sql);
        return $rs->row();
    }

    function get_checkin_detail($spa_id)
    {
        $sql="SELECT spd.*,ser.service_name from spa_datadetail spd
        inner join spa_service ser on spd.service_id=ser.service_id
WHERE spa_id=$spa_id
";
        $rs=$this->db->query($sql);
        return $rs->result();
    }






    /********** Check In *********/

    function checkIn($bookId,$dtin,$tmin,$roomId,$therapist,$branch)
    {
        $this->db->query("CALL SP_CHECKIN('{$bookId}','{$dtin}','{$tmin}',{$roomId},{$therapist},{$branch});");
    }

    function update_checkin($id,$data)
    {
        $this->db->where('CHECKIN_ID',$id);
        $this->db->update('spa_checkin',$data);
    }

    function get_checkIn($keyword,$branch)
    {
        $this->db->select('*');
        $this->db->from('spa_booking');
        $this->db->join('spa_checkin', 'spa_checkin.BOOKING_ID = spa_booking.BOOKING_ID');
        $cond=" spa_booking.BRANCH_ID={$branch} AND spa_booking.D_STATUS = 'CHECKED' AND (LOWER(CUS_NAME) LIKE '%".strtolower($keyword)."%' OR LOWER(TEL) LIKE '%".strtolower($keyword)."%')";
        $this->db->where($cond);
        $this->db->limit(10, 0);
        return $this->db->get()->result_array();
    }

    function getPendingBook($branch)
    {
        $cond=array('DATE_IN' => date('Y-m-d'),'ENT_TYPE' => 'BOOKING', 'D_STATUS' => 'PENDING', 'BRANCH_ID' => $branch);
        return $this->db->get_where('spa_booking',$cond);
    }

    /*function getAvailableRoom($branch)
    {
        $data = $this->db->query("SELECT
                  F.FLOOR_ID,
                  F.FLOOR_NAME,
                  R.ROOM_CODE,
                  R.ROOM_ID,
                  R.ROOM_NAME
                FROM spa_room R
                INNER JOIN spa_floor F
                ON F.FLOOR_ID = R.FLOOR_ID
                WHERE F.BRANCH_ID = {$branch}
                AND ROOM_ID NOT IN(SELECT
                    ROOM_ID
                  FROM spa_checkin
                  WHERE D_STATUS ='IN USED') ORDER BY F.FLOOR_ID, ROOM_ID ASC");
        return $data;
    }*/

    function getTherapist($branch)
    {
        $cond=array('D_STATUS' => 'N', 'BRANCH_ID' =>$branch);
        return $this->db->get_where('spa_employee',$cond);
    }

    /*
        for check in service
    */
    function getService()
    {
        $this->db->where('D_STATUS','N');
        return $this->db->get('spa_service');
    }

    function add_chservice($branch,$cid,$svid,$upr,$amt)
    {
        $this->db->query("CALL SP_ADD_CHSERVICE({$branch}, '{$cid}', {$svid}, {$upr}, {$amt})");
    }

    function getItemToList($cid)
    {
        $this->db->select('*');
        $this->db->from('spa_checkin_service');
        $this->db->join('spa_service', 'spa_service.SERVICE_ID = spa_checkin_service.SERVICE_ID');
        $cond = " CHECKIN_ID='{$cid}' AND spa_checkin_service.D_STATUS='N'";
        $this->db->where($cond);
        return $this->db->get()->result_array();
    }

    function update_chservice($chsvid,$data)
    {
        $this->db->where('CHKSV_ID',$chsvid);
        $this->db->update('spa_checkin_service',$data);
    }

    function getTotalPrice($cid)
    {
        $data=$this->db->query("SELECT
                          SUM(UNIT_PRICE * AMOUNT) AS TOTAL_USD
                          FROM spa_checkin_service
                          WHERE D_STATUS='N'
                          AND CHECKIN_ID='{$cid}'");
        return $data->result_array();
    }

    /*
     **** Check out ****
     */

    function checkout($br,$cid,$rate,$discount,$total)
    {
        $this->db->query("CALL SP_CHECKOUT({$br}, '{$cid}', {$rate}, {$discount}, {$total});");
    }

}