<?php defined('BASEPATH') OR exit('No direct script access allowed');
class M_regional extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    function add_regional($data)
    {
        $this->db->insert('spa_regional',$data);
    }

    public function update_regional($id, $data)
    {
        $this->db->where('REGIONAL_ID',$id);
        $this->db->update('spa_regional',$data);
    }

    public function get_regional($keyword) {
        $this->db->order_by('REGIONAL_ID', 'ASC');
        $this->db->where('D_STATUS','N');
        $this->db->like("LOWER(REGIONAL_CODE)", strtolower($keyword));
        $this->db->or_like('LOWER(REGIONAL_NAME)', strtolower($keyword));
        $this->db->or_like('LOWER(REGIONAL_DES)', strtolower($keyword));
        $this->db->limit(10, 0);
        return $this->db->get('spa_regional')->result_array();
    }


}