function validateDOB(id)
{
    var dob = document.getElementById(id).value;
    var pattern = /^([0-9]{2})-([0-9]{2})-([0-9]{4})$/;
    if (dob == null || dob == "" || !pattern.test(dob)) {
        document.getElementById(id).value='';
        document.getElementById(id).style.borderColor = "red";
    }
    else {
        document.getElementById(id).style.borderColor = "silver";
    }
}

function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;

}