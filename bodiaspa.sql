-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 16, 2017 at 10:00 AM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bodiaspa`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ADD_CHSERVICE` (`PBRANCH_ID` INT(11), `PCHECKIN_ID` VARCHAR(255) CHARACTER SET UTF8, `PSERVICE_ID` INT(11), `PUNITPRICE` FLOAT, `PAMT` FLOAT)  BEGIN

  DECLARE LV_AUTO_SERVICE_ID VARCHAR(255);
  SET LV_AUTO_SERVICE_ID=CONCAT(YEAR(CURDATE()),'-',MONTH(CURDATE()),'-',DAY(CURDATE()),'-',HOUR(CURTIME()),'-',MINUTE(CURTIME()),'-',SECOND(CURTIME()),'-',PBRANCH_ID);

  IF NOT EXISTS(SELECT SERVICE_ID FROM spa_checkin_service WHERE CHECKIN_ID=PCHECKIN_ID AND SERVICE_ID=PSERVICE_ID) THEN
    INSERT INTO spa_checkin_service
    (
     	CHKSV_ID,
      CHECKIN_ID,
      SERVICE_ID,
      UNIT_PRICE,
      AMOUNT
    )
    VALUES
    (
      LV_AUTO_SERVICE_ID,
      PCHECKIN_ID,
      PSERVICE_ID,
      PUNITPRICE,
      PAMT
    );
  ELSE
  
  UPDATE spa_checkin_service
    SET AMOUNT=PAMT,
        UNIT_PRICE=PUNITPRICE
  WHERE CHECKIN_ID = PCHECKIN_ID
  AND SERVICE_ID = PSERVICE_ID;
  
  END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_CHECKIN` (`PBOOKING_ID` VARCHAR(255) CHARACTER SET UTF8, `PDATE_IN` DATE, `PTIME_IN` DATETIME, `PROOM_ID` INT(11), `PTHERAPIST` INT(11), `PBRANCH_ID` INT(11))  BEGIN
DECLARE LV_AUTO_ID VARCHAR(255);

SET LV_AUTO_ID=CONCAT(YEAR(CURDATE()),'-',MONTH(CURDATE()),'-',DAY(CURDATE()),'-',HOUR(CURTIME()),'-',MINUTE(CURTIME()),'-',SECOND(CURTIME()),'-',PBRANCH_ID);
IF EXISTS(SELECT * FROM spa_booking WHERE BOOKING_ID=PBOOKING_ID) THEN
  INSERT INTO spa_checkin
  (
    CHECKIN_ID,
    BOOKING_ID,
    DATE_IN,
    TIME_IN,
    ROOM_ID,
    THERAPIST
  )
  VALUES
  (
    LV_AUTO_ID,
    PBOOKING_ID,
    PDATE_IN,
    PTIME_IN,
    PROOM_ID,
    PTHERAPIST
  );
END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_CHECKOUT` (`PBRANCH_ID` INT(11), `PCHECKIN_ID` VARCHAR(255) CHARACTER SET UTF8, `PEXCHANGE_RATE` FLOAT, `PDISCOUNT` FLOAT, `PTOTAL` FLOAT)  BEGIN

  DECLARE LV_AUTO_CHECKOUT_ID VARCHAR(255);
  SET LV_AUTO_CHECKOUT_ID=CONCAT(YEAR(CURDATE()),'-',MONTH(CURDATE()),'-',DAY(CURDATE()),'-',HOUR(CURTIME()),'-',MINUTE(CURTIME()),'-',SECOND(CURTIME()),'-',PBRANCH_ID);

    INSERT INTO spa_checkout
    (
     	CHECKOUT_ID,
      CHECKIN_ID,
      EXCHANGE_RATE,
      DISCOUNT,
      GRAND_TOTAL
    )
    VALUES
    (
      LV_AUTO_CHECKOUT_ID,
      PCHECKIN_ID,
      PEXCHANGE_RATE,
      PDISCOUNT,
      PTOTAL
    );
   


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_USRNB_ASSIGN` (`PUS_ID` VARCHAR(255) CHARACTER SET UTF8, `PBRANCH_ID` INT(11), `PUSNAME` VARCHAR(255) CHARACTER SET UTF8)  BEGIN


IF(NOT EXISTS(SELECT * FROM spa_usrnbr WHERE US_ID = PUS_ID)) THEN
  INSERT INTO spa_usrnbr(US_ID, BRANCH_ID, ASSIGNED_DATE, ASSIGNED_BY)
  VALUES(PUS_ID, PBRANCH_ID, NOW(), PUSNAME);
ELSE
  UPDATE spa_usrnbr
    SET BRANCH_ID = PBRANCH_ID,
        ASSIGNED_DATE = NOW(),
        ASSIGNED_BY = PUSNAME
    WHERE US_ID = PUS_ID;
END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_USRNR_ASSIGN` (`PUS_ID` VARCHAR(255) CHARACTER SET UTF8, `PRULE_ID` INT(11), `PUSNAME` VARCHAR(255) CHARACTER SET UTF8)  BEGIN


IF(NOT EXISTS(SELECT * FROM spa_usrnrule WHERE US_ID = PUS_ID)) THEN
  INSERT INTO spa_usrnrule(US_ID, RULE_ID, ASSIGNED_DATE, ASSIGNED_BY)
  VALUES(PUS_ID, PRULE_ID, NOW(), PUSNAME);
ELSE
  UPDATE spa_usrnrule
    SET RULE_ID = PRULE_ID,
        ASSIGNED_DATE = NOW(),
        ASSIGNED_BY = PUSNAME
    WHERE US_ID = PUS_ID;
END IF;

END$$

--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `FN_SPA_BOOKING` (`PCUS_NAME` VARCHAR(255) CHARACTER SET UTF8, `PGENDER` VARCHAR(1) CHARACTER SET UTF8, `PTEL` VARCHAR(11) CHARACTER SET UTF8, `PAGENCY_ID` INT(11), `PNUM_OF_PEX` INT(11), `PDATE_IN` DATE, `PTIME_IN` TIME, `PHOUR` FLOAT, `PROOM_ID` INT(11), `PTHERAPIST` INT(11), `PTUK` INT(11), `PENT_TYPE` VARCHAR(10) CHARACTER SET UTF8, `PBRANCH_ID` INT(11)) RETURNS VARCHAR(255) CHARSET utf8 BEGIN

DECLARE LV_AUTO_ID VARCHAR(255);

SET LV_AUTO_ID=CONCAT(YEAR(CURDATE()),'-',MONTH(CURDATE()),'-',DAY(CURDATE()),'-',HOUR(CURTIME()),'-',MINUTE(CURTIME()),'-',SECOND(CURTIME()),'-',PBRANCH_ID);

INSERT INTO spa_booking
(
  BOOKING_ID,
  CUS_NAME,
  GENDER,
  TEL,
  AGENCY,
  NUM_OF_PEX,
  DATE_BOOKING,
  DATE_IN,
  TIME_IN,
  TOTAL_HOUR,
  TMP_ROOM_ID,
  THERAPIST,
  TUKTUK,
  ENT_TYPE,
  BRANCH_ID
)
VALUES
(
  LV_AUTO_ID,
  PCUS_NAME,
  PGENDER,
  PTEL,
  PAGENCY_ID,
  PNUM_OF_PEX,
  NOW(),
  PDATE_IN,
  PTIME_IN,
  PHOUR,
  PROOM_ID,
  PTHERAPIST,
  PTUK,
  PENT_TYPE,
  PBRANCH_ID
);

RETURN LV_AUTO_ID; 

END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `FN_SPA_USER_INSERT` (`PFULL_NAME` VARCHAR(255) CHARACTER SET UTF8, `PSTAFF_ID` VARCHAR(255) CHARACTER SET UTF8, `PLOGINNAME` VARCHAR(255) CHARACTER SET UTF8, `PUS_PASSWORD` VARCHAR(255) CHARACTER SET UTF8) RETURNS VARCHAR(255) CHARSET utf8 BEGIN

DECLARE LV_AUTO_ID VARCHAR(255);

SET LV_AUTO_ID=CONCAT(YEAR(CURDATE()),'-',MONTH(CURDATE()),'-',DAY(CURDATE()),'-',HOUR(CURTIME()),'-',MINUTE(CURTIME()),'-',SECOND(CURTIME()));

INSERT INTO spa_user(US_ID, FULLNAME, STAFF_ID, LOGINNAME, US_PASSWORD)
VALUES
(
  LV_AUTO_ID,
  PFULL_NAME,
  PSTAFF_ID,
  PLOGINNAME,
  PUS_PASSWORD  
);

RETURN LV_AUTO_ID; 

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `spa_agentcy`
--

CREATE TABLE `spa_agentcy` (
  `AGENT_ID` int(11) NOT NULL,
  `AGENT_NAME` varchar(255) DEFAULT NULL,
  `ADDRESS` varchar(255) DEFAULT NULL,
  `TEL` varchar(11) DEFAULT NULL,
  `BRANCH_ID` int(11) DEFAULT NULL,
  `D_STATUS` varchar(1) DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spa_agentcy`
--

INSERT INTO `spa_agentcy` (`AGENT_ID`, `AGENT_NAME`, `ADDRESS`, `TEL`, `BRANCH_ID`, `D_STATUS`) VALUES
(4, 'Capito', 'Phnom Penh\r\n                                    ', '012365623', 3, 'N'),
(5, 'Soriya', 'Phnom Penh\r\n                                    ', '012375727', 3, 'N');

-- --------------------------------------------------------

--
-- Table structure for table `spa_booking`
--

CREATE TABLE `spa_booking` (
  `BOOKING_ID` varchar(255) NOT NULL DEFAULT '',
  `CUS_NAME` varchar(255) DEFAULT NULL,
  `GENDER` varchar(1) DEFAULT NULL,
  `TEL` varchar(11) DEFAULT NULL,
  `AGENCY` int(11) DEFAULT NULL,
  `NUM_OF_PEX` int(11) DEFAULT NULL,
  `DATE_BOOKING` date DEFAULT NULL,
  `DATE_IN` date DEFAULT NULL,
  `TMP_ROOM_ID` int(11) DEFAULT NULL,
  `TIME_IN` varchar(255) DEFAULT '01:00:00',
  `TOTAL_HOUR` float DEFAULT '0',
  `THERAPIST` int(11) DEFAULT NULL,
  `TUKTUK` int(11) DEFAULT '0',
  `ENT_TYPE` varchar(10) NOT NULL DEFAULT 'CHECKIN',
  `BRANCH_ID` int(11) NOT NULL,
  `D_STATUS` varchar(15) DEFAULT 'PENDING'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `spa_branch`
--

CREATE TABLE `spa_branch` (
  `BRANCH_ID` int(11) NOT NULL,
  `REGIONAL_ID` int(11) NOT NULL,
  `BRANCH_CODE` varchar(255) NOT NULL,
  `BRANCH_NAME` varchar(255) DEFAULT NULL,
  `ADDRESS` varchar(500) DEFAULT NULL,
  `ESTABLISH_DATE` date DEFAULT NULL,
  `D_STATUS` varchar(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spa_branch`
--

INSERT INTO `spa_branch` (`BRANCH_ID`, `REGIONAL_ID`, `BRANCH_CODE`, `BRANCH_NAME`, `ADDRESS`, `ESTABLISH_DATE`, `D_STATUS`) VALUES
(3, 21, 'PP', 'Phnom Penh', '#313, str 271, phnon penh', '2016-09-28', 'N'),
(4, 21, 'KDL', 'Kandal Province', 'Takmao, Kandal Province', '2016-09-28', 'N'),
(5, 21, 'KCH', 'Kampong Cham', 'Suong Vill, Kampong Cham', '2016-09-28', 'N'),
(6, 22, 'SMR', 'Seam Riep', 'Seam Rieap, Cambodia.', '2016-09-28', 'N'),
(7, 21, 'SVR', 'Svay Reang', 'Svay Reang, Cambodia.', '2016-09-28', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `spa_checkin`
--

CREATE TABLE `spa_checkin` (
  `CHECKIN_ID` varchar(255) NOT NULL DEFAULT '',
  `BOOKING_ID` varchar(255) DEFAULT NULL,
  `DATE_IN` date DEFAULT NULL,
  `TIME_IN` datetime DEFAULT NULL,
  `ROOM_ID` int(11) DEFAULT NULL,
  `THERAPIST` int(11) DEFAULT NULL,
  `D_STATUS` varchar(255) DEFAULT 'IN USED'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `spa_checkin_service`
--

CREATE TABLE `spa_checkin_service` (
  `CHKSV_ID` varchar(255) NOT NULL,
  `CHECKIN_ID` varchar(255) DEFAULT NULL,
  `SERVICE_ID` int(11) NOT NULL,
  `UNIT_PRICE` float NOT NULL DEFAULT '0',
  `AMOUNT` float NOT NULL DEFAULT '0',
  `D_STATUS` varchar(1) DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `spa_checkout`
--

CREATE TABLE `spa_checkout` (
  `CHECKOUT_ID` varchar(255) NOT NULL DEFAULT '',
  `CHECKIN_ID` varchar(255) DEFAULT NULL,
  `EXCHANGE_RATE` float DEFAULT NULL,
  `DISCOUNT` float DEFAULT '0',
  `GRAND_TOTAL` float DEFAULT NULL,
  `D_STATUS` varchar(1) DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `spa_data`
--

CREATE TABLE `spa_data` (
  `spa_id` int(11) NOT NULL,
  `spa_code` varchar(255) DEFAULT NULL,
  `cus_name` varchar(255) DEFAULT NULL,
  `tel` varchar(11) DEFAULT NULL,
  `e_mail` varchar(255) DEFAULT NULL,
  `date_treatment` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `no_person` int(11) DEFAULT '0',
  `room_id` int(11) NOT NULL DEFAULT '0',
  `emp_id` int(11) NOT NULL,
  `record_type` varchar(10) DEFAULT 'BOOKING',
  `record_date` datetime DEFAULT NULL,
  `d_status` varchar(1) DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `spa_datadetail`
--

CREATE TABLE `spa_datadetail` (
  `spd_id` int(11) NOT NULL,
  `spa_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `unit_price` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `spa_employee`
--

CREATE TABLE `spa_employee` (
  `EMP_ID` int(11) NOT NULL,
  `EMP_CODE` varchar(50) DEFAULT NULL,
  `EMP_NAME` varchar(255) DEFAULT NULL,
  `GENDER` varchar(1) DEFAULT NULL,
  `TEL` varchar(255) DEFAULT NULL,
  `BRANCH_ID` int(11) NOT NULL,
  `ADDRESS` varchar(300) DEFAULT NULL,
  `D_STATUS` varchar(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spa_employee`
--

INSERT INTO `spa_employee` (`EMP_ID`, `EMP_CODE`, `EMP_NAME`, `GENDER`, `TEL`, `BRANCH_ID`, `ADDRESS`, `D_STATUS`) VALUES
(1, '001', 'Srey Leave', 'F', '0181888582', 3, 'Khsach Kandal', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `spa_employee_leave`
--

CREATE TABLE `spa_employee_leave` (
  `LEAVE_ID` int(11) NOT NULL,
  `EMP_ID` int(11) NOT NULL,
  `LEAVE_FR` date DEFAULT NULL,
  `LEAVE_TO` date DEFAULT NULL,
  `NO_OF_DAY` float DEFAULT NULL,
  `REASON` varchar(300) DEFAULT NULL,
  `D_STATUS` varchar(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spa_employee_leave`
--

INSERT INTO `spa_employee_leave` (`LEAVE_ID`, `EMP_ID`, `LEAVE_FR`, `LEAVE_TO`, `NO_OF_DAY`, `REASON`, `D_STATUS`) VALUES
(1, 1, '2016-10-06', '2016-10-07', 1, 'Sick Leave', 'N'),
(2, 2, '2019-09-01', '2019-09-01', 5, 'Track', 'N'),
(3, 2, '2012-12-31', '2012-12-31', 5, 'dsfsfsdfsd', 'D'),
(4, 2, '2019-09-01', '2019-09-01', 8, 'test', 'N'),
(5, 1, '2012-12-31', '2012-12-31', 4, 'dsfsd', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `spa_floor`
--

CREATE TABLE `spa_floor` (
  `FLOOR_ID` int(11) NOT NULL,
  `BRANCH_ID` int(11) NOT NULL,
  `FLOOR_CODE` varchar(255) NOT NULL,
  `FLOOR_NAME` varchar(255) DEFAULT NULL,
  `FLOOR_DES` varchar(500) NOT NULL,
  `D_STATUS` varchar(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spa_floor`
--

INSERT INTO `spa_floor` (`FLOOR_ID`, `BRANCH_ID`, `FLOOR_CODE`, `FLOOR_NAME`, `FLOOR_DES`, `D_STATUS`) VALUES
(3, 3, '001', '1', 'Floor 1 of Phnom Penh branch', 'N'),
(4, 3, '002', '2', 'Second floor of pnom penh branch', 'N'),
(5, 3, '003', '3', 'Third floor of phnom penh branch', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `spa_permission`
--

CREATE TABLE `spa_permission` (
  `MODULE_ID` int(11) NOT NULL,
  `MODULE_CODE` varchar(3) NOT NULL,
  `MODULE_NAME` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `LEVEL` varchar(1) NOT NULL,
  `ICON` varchar(255) NOT NULL,
  `PARENT_ID` int(11) NOT NULL,
  `D_STATUS` varchar(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spa_permission`
--

INSERT INTO `spa_permission` (`MODULE_ID`, `MODULE_CODE`, `MODULE_NAME`, `URL`, `LEVEL`, `ICON`, `PARENT_ID`, `D_STATUS`) VALUES
(1, 'HME', 'Home', 'home', 'S', 'fa fa-home', 0, 'N'),
(2, 'CIF', 'Customer', 'customer', 'S', 'fa fa-users', 0, 'D'),
(3, 'SAL', 'SPA & Reservation', 'booking', 'P', 'fa fa-cart-plus', 0, 'N'),
(4, 'BK', 'Booking', 'booking', 'C', 'fa fa-angle-right', 3, 'N'),
(5, 'CHK', 'Walk In', 'checkin', 'C', 'fa fa-angle-right', 3, 'N'),
(6, 'CKO', 'Check Out', 'checkout', 'C', 'fa fa-angle-right', 3, 'B'),
(7, 'HR', 'Human Resource', 'employee', 'P', 'fa fa-user-secret', 0, 'B'),
(8, 'EMP', 'Staff', 'employee', 'S', 'fa fa-user-secret', 0, 'B'),
(9, 'EML', 'Employee Leave', 'employee_leave', 'C', 'fa fa-angle-right', 7, 'B'),
(10, 'UMG', 'User Management', 'user', 'P', 'fa fa-user-md', 0, 'N'),
(11, 'USR', 'User Profile', 'user', 'C', 'fa fa-angle-right', 10, 'N'),
(12, 'RNP', 'Rule and Permission', 'Rulenperm', 'C', 'fa fa-angle-right', 10, 'N'),
(13, 'SEU', 'Setup', 'branch', 'P', 'fa fa-gears', 0, 'N'),
(14, 'BR', 'Branch', 'branch', 'C', 'fa fa-angle-right', 13, 'N'),
(15, 'FLR', 'Floor', 'floor', 'C', 'fa fa-angle-right', 13, 'N'),
(16, 'ROM', 'Room', 'room', 'C', 'fa fa-angle-right', 13, 'N'),
(17, 'SVR', 'Service', 'service', 'C', 'fa fa-angle-right', 13, 'N'),
(18, 'UR', 'User Rule', 'rule', 'C', 'fa fa-angle-right', 13, 'N'),
(19, 'RGN', 'Regional', 'regional', 'C', 'fa fa-angle-right', 13, 'N'),
(20, 'AGT', 'Agency', 'agency', 'C', 'fa fa-angle-right', 13, 'B'),
(21, 'RPT', 'Reporting', 'reporting', 'P', 'fa fa-fa fa-bar-chart', 0, 'N'),
(22, 'RPM', 'Repayment by Branch', 'repay', 'C', 'fa fa-angle-right', 21, 'N');

-- --------------------------------------------------------

--
-- Table structure for table `spa_position`
--

CREATE TABLE `spa_position` (
  `POS_ID` int(11) NOT NULL,
  `POS_NAME` varchar(255) NOT NULL,
  `JOB_DESCRIPTION` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `spa_regional`
--

CREATE TABLE `spa_regional` (
  `REGIONAL_ID` int(11) NOT NULL,
  `REGIONAL_CODE` varchar(255) NOT NULL,
  `REGIONAL_NAME` varchar(255) DEFAULT NULL,
  `REGIONAL_DES` varchar(500) DEFAULT NULL,
  `D_STATUS` varchar(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spa_regional`
--

INSERT INTO `spa_regional` (`REGIONAL_ID`, `REGIONAL_CODE`, `REGIONAL_NAME`, `REGIONAL_DES`, `D_STATUS`) VALUES
(21, 'PP', 'Phnom Penh', 'Cover all branchs that located in Phnom Penh.', 'N'),
(22, 'SR', 'Siem Reap', 'Cover all branchs that located in Siem Reap.', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `spa_room`
--

CREATE TABLE `spa_room` (
  `ROOM_ID` int(11) NOT NULL,
  `FLOOR_ID` int(11) NOT NULL,
  `ROOM_CODE` varchar(255) NOT NULL,
  `RTYPE_ID` int(11) NOT NULL,
  `ROOM_NAME` varchar(255) DEFAULT NULL,
  `D_STATUS` varchar(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spa_room`
--

INSERT INTO `spa_room` (`ROOM_ID`, `FLOOR_ID`, `ROOM_CODE`, `RTYPE_ID`, `ROOM_NAME`, `D_STATUS`) VALUES
(3, 3, '001', 1, 'FUNAN ', 'N'),
(4, 3, '002', 1, 'LONG VAEK ', 'N'),
(5, 3, '003', 1, 'CHEN LA ', 'N'),
(6, 3, '004', 1, 'KOUK THLORK ', 'N'),
(7, 3, '005', 1, 'NOKOR PHNOM ', 'N'),
(8, 4, '006', 1, 'MEAN BON ', 'N'),
(9, 4, '007', 1, 'MEAN CHEY ', 'N'),
(10, 4, '008', 1, 'MEAN YOUS ', 'N'),
(11, 4, '009', 1, 'MEAN LEAP ', 'N'),
(12, 4, '010', 1, 'MEAN VEASNA ', 'N'),
(13, 5, '011', 2, 'CAMBODIA ', 'N'),
(14, 5, '012', 2, 'THAILAND ', 'N'),
(15, 5, '013', 2, 'VIETNAM ', 'N'),
(16, 5, '014', 1, 'LAOS ', 'N'),
(17, 5, '015', 2, 'CHINA ', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `spa_room_type`
--

CREATE TABLE `spa_room_type` (
  `RTYPE_ID` int(11) NOT NULL,
  `RTYPE_NAME` varchar(255) DEFAULT NULL,
  `D_STATUS` varchar(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spa_room_type`
--

INSERT INTO `spa_room_type` (`RTYPE_ID`, `RTYPE_NAME`, `D_STATUS`) VALUES
(1, 'Normal', 'N'),
(2, 'VIP', 'N'),
(3, 'Luxury', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `spa_rule`
--

CREATE TABLE `spa_rule` (
  `RULE_ID` int(11) NOT NULL,
  `RULE_NAME` varchar(255) DEFAULT NULL,
  `D_STATUS` varchar(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spa_rule`
--

INSERT INTO `spa_rule` (`RULE_ID`, `RULE_NAME`, `D_STATUS`) VALUES
(1, 'Power User', 'N'),
(2, 'Normal User', 'N'),
(3, 'Administrator', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `spa_rulenperm`
--

CREATE TABLE `spa_rulenperm` (
  `RNPM_ID` int(11) NOT NULL,
  `RULE_ID` int(11) DEFAULT NULL,
  `MODULE_ID` int(11) DEFAULT NULL,
  `P_VIEW` int(11) DEFAULT '0',
  `P_ADD` int(11) DEFAULT '0',
  `P_EDIT` int(11) DEFAULT '0',
  `P_DELETE` int(11) DEFAULT '0',
  `ASSIGNER` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spa_rulenperm`
--

INSERT INTO `spa_rulenperm` (`RNPM_ID`, `RULE_ID`, `MODULE_ID`, `P_VIEW`, `P_ADD`, `P_EDIT`, `P_DELETE`, `ASSIGNER`) VALUES
(116, 1, 1, 1, 0, 0, 0, '#'),
(117, 1, 3, 1, 0, 0, 0, '#'),
(118, 1, 4, 1, 0, 0, 0, '#'),
(119, 1, 5, 1, 0, 0, 0, '#'),
(120, 1, 6, 1, 0, 0, 0, '#'),
(121, 1, 8, 1, 0, 0, 0, '#'),
(122, 1, 10, 1, 0, 0, 0, '#'),
(123, 1, 11, 1, 0, 0, 0, '#'),
(124, 1, 12, 1, 0, 0, 0, '#'),
(125, 1, 13, 1, 0, 0, 0, '#'),
(126, 1, 14, 1, 0, 0, 0, '#'),
(127, 1, 15, 1, 0, 0, 0, '#'),
(128, 1, 16, 1, 0, 0, 0, '#'),
(129, 1, 17, 1, 0, 0, 0, '#'),
(130, 1, 18, 1, 0, 0, 0, '#'),
(131, 1, 19, 1, 0, 0, 0, '#'),
(132, 1, 20, 1, 0, 0, 0, '#'),
(133, 1, 21, 1, 0, 0, 0, '#');

-- --------------------------------------------------------

--
-- Table structure for table `spa_service`
--

CREATE TABLE `spa_service` (
  `SERVICE_ID` int(11) NOT NULL,
  `SERVICE_NAME` varchar(255) DEFAULT NULL,
  `UNIT` varchar(255) DEFAULT NULL,
  `UNIT_PRICE` float NOT NULL DEFAULT '0',
  `DESCR` varchar(255) DEFAULT NULL,
  `D_STATUS` varchar(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spa_service`
--

INSERT INTO `spa_service` (`SERVICE_ID`, `SERVICE_NAME`, `UNIT`, `UNIT_PRICE`, `DESCR`, `D_STATUS`) VALUES
(1, 'd', NULL, 0, 'd', 'D'),
(2, 'Oil massage', NULL, 0, 'Oil massage', 'D'),
(3, 'fsd', NULL, 0, 'fds', 'D'),
(4, 'te', NULL, 0, 'dfs', 'D'),
(5, 'Massage', NULL, 0, 'Provide massage service to cus', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `spa_user`
--

CREATE TABLE `spa_user` (
  `US_ID` varchar(255) NOT NULL,
  `FULLNAME` varchar(255) DEFAULT NULL,
  `STAFF_ID` varchar(255) DEFAULT NULL,
  `LOGINNAME` varchar(255) DEFAULT NULL,
  `US_PASSWORD` varchar(255) DEFAULT NULL,
  `D_STATUS` varchar(1) DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spa_user`
--

INSERT INTO `spa_user` (`US_ID`, `FULLNAME`, `STAFF_ID`, `LOGINNAME`, `US_PASSWORD`, `D_STATUS`) VALUES
('2016-9-6-13-41-39', 'Leng Ratha', '00781', 'lratha', '202cb962ac59075b964b07152d234b70', 'N'),
('2016-9-6-13-48-6', 'Phon Vuthy', '08749', 'pvuthy', '202cb962ac59075b964b07152d234b70', 'D');

-- --------------------------------------------------------

--
-- Stand-in structure for view `spa_users`
-- (See below for the actual view)
--
CREATE TABLE `spa_users` (
`US_ID` varchar(255)
,`FULLNAME` varchar(255)
,`STAFF_ID` varchar(255)
,`LOGINNAME` varchar(255)
,`US_PASSWORD` varchar(255)
,`BRANCH_ID` int(11)
,`RULE_ID` int(11)
,`D_STATUS` varchar(1)
,`RULE_NAME` varchar(255)
);

-- --------------------------------------------------------

--
-- Table structure for table `spa_usrnbr`
--

CREATE TABLE `spa_usrnbr` (
  `USRNBR_ID` int(11) NOT NULL,
  `US_ID` varchar(255) DEFAULT NULL,
  `BRANCH_ID` int(11) DEFAULT NULL,
  `ASSIGNED_DATE` date DEFAULT NULL,
  `ASSIGNED_BY` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spa_usrnbr`
--

INSERT INTO `spa_usrnbr` (`USRNBR_ID`, `US_ID`, `BRANCH_ID`, `ASSIGNED_DATE`, `ASSIGNED_BY`) VALUES
(1, '2016-9-6-13-41-39', 3, '2016-10-05', 'lratha');

-- --------------------------------------------------------

--
-- Table structure for table `spa_usrnrule`
--

CREATE TABLE `spa_usrnrule` (
  `USRNRULE_ID` int(11) NOT NULL,
  `US_ID` varchar(255) DEFAULT NULL,
  `RULE_ID` int(11) DEFAULT NULL,
  `ASSIGNED_DATE` date DEFAULT NULL,
  `ASSIGNED_BY` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spa_usrnrule`
--

INSERT INTO `spa_usrnrule` (`USRNRULE_ID`, `US_ID`, `RULE_ID`, `ASSIGNED_DATE`, `ASSIGNED_BY`) VALUES
(1, '2016-9-6-13-41-39', 1, '2016-09-06', 'get username from session'),
(2, '2016-9-6-13-48-6', 3, '2016-09-06', 'get username from session');

-- --------------------------------------------------------

--
-- Structure for view `spa_users`
--
DROP TABLE IF EXISTS `spa_users`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `spa_users`  AS  select `usr`.`US_ID` AS `US_ID`,`usr`.`FULLNAME` AS `FULLNAME`,`usr`.`STAFF_ID` AS `STAFF_ID`,`usr`.`LOGINNAME` AS `LOGINNAME`,`usr`.`US_PASSWORD` AS `US_PASSWORD`,`unb`.`BRANCH_ID` AS `BRANCH_ID`,`rule`.`RULE_ID` AS `RULE_ID`,`usr`.`D_STATUS` AS `D_STATUS`,`rule`.`RULE_NAME` AS `RULE_NAME` from (((`spa_user` `usr` join `spa_usrnbr` `unb` on((`unb`.`US_ID` = `usr`.`US_ID`))) left join `spa_usrnrule` `urb` on((`urb`.`US_ID` = `usr`.`US_ID`))) left join `spa_rule` `rule` on((`rule`.`RULE_ID` = `urb`.`RULE_ID`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `spa_agentcy`
--
ALTER TABLE `spa_agentcy`
  ADD PRIMARY KEY (`AGENT_ID`),
  ADD KEY `FK_AGENCY_BRANCH` (`BRANCH_ID`);

--
-- Indexes for table `spa_booking`
--
ALTER TABLE `spa_booking`
  ADD PRIMARY KEY (`BOOKING_ID`),
  ADD KEY `FK_BOOKING_BRNACH` (`BRANCH_ID`),
  ADD KEY `TMP_ROOM_ID` (`TMP_ROOM_ID`);

--
-- Indexes for table `spa_branch`
--
ALTER TABLE `spa_branch`
  ADD PRIMARY KEY (`BRANCH_ID`);

--
-- Indexes for table `spa_checkin`
--
ALTER TABLE `spa_checkin`
  ADD PRIMARY KEY (`CHECKIN_ID`),
  ADD KEY `CHECKIN_BOOKING_ID` (`BOOKING_ID`),
  ADD KEY `CHECKIN_ROOM_ID` (`ROOM_ID`),
  ADD KEY `spa_checkin_therapist` (`THERAPIST`);

--
-- Indexes for table `spa_checkin_service`
--
ALTER TABLE `spa_checkin_service`
  ADD PRIMARY KEY (`CHKSV_ID`),
  ADD KEY `CHECKIN_SERVICE_CHECKIN` (`CHECKIN_ID`),
  ADD KEY `CHECKIN_SERVICE_ID` (`SERVICE_ID`);

--
-- Indexes for table `spa_checkout`
--
ALTER TABLE `spa_checkout`
  ADD PRIMARY KEY (`CHECKOUT_ID`),
  ADD KEY `FK_CHECKOUT_CHKIN` (`CHECKIN_ID`);

--
-- Indexes for table `spa_data`
--
ALTER TABLE `spa_data`
  ADD PRIMARY KEY (`spa_id`);

--
-- Indexes for table `spa_datadetail`
--
ALTER TABLE `spa_datadetail`
  ADD PRIMARY KEY (`spd_id`);

--
-- Indexes for table `spa_employee`
--
ALTER TABLE `spa_employee`
  ADD PRIMARY KEY (`EMP_ID`),
  ADD KEY `FK_EMP_BRANCH` (`BRANCH_ID`);

--
-- Indexes for table `spa_employee_leave`
--
ALTER TABLE `spa_employee_leave`
  ADD PRIMARY KEY (`LEAVE_ID`);

--
-- Indexes for table `spa_floor`
--
ALTER TABLE `spa_floor`
  ADD PRIMARY KEY (`FLOOR_ID`),
  ADD UNIQUE KEY `FLOOR_ID` (`FLOOR_ID`),
  ADD KEY `BRANCH_ID` (`BRANCH_ID`);

--
-- Indexes for table `spa_permission`
--
ALTER TABLE `spa_permission`
  ADD PRIMARY KEY (`MODULE_ID`);

--
-- Indexes for table `spa_position`
--
ALTER TABLE `spa_position`
  ADD PRIMARY KEY (`POS_ID`);

--
-- Indexes for table `spa_regional`
--
ALTER TABLE `spa_regional`
  ADD PRIMARY KEY (`REGIONAL_ID`);

--
-- Indexes for table `spa_room`
--
ALTER TABLE `spa_room`
  ADD PRIMARY KEY (`ROOM_ID`),
  ADD KEY `FLOOR_ID` (`FLOOR_ID`);

--
-- Indexes for table `spa_room_type`
--
ALTER TABLE `spa_room_type`
  ADD PRIMARY KEY (`RTYPE_ID`);

--
-- Indexes for table `spa_rule`
--
ALTER TABLE `spa_rule`
  ADD PRIMARY KEY (`RULE_ID`);

--
-- Indexes for table `spa_rulenperm`
--
ALTER TABLE `spa_rulenperm`
  ADD PRIMARY KEY (`RNPM_ID`);

--
-- Indexes for table `spa_service`
--
ALTER TABLE `spa_service`
  ADD PRIMARY KEY (`SERVICE_ID`);

--
-- Indexes for table `spa_user`
--
ALTER TABLE `spa_user`
  ADD PRIMARY KEY (`US_ID`);

--
-- Indexes for table `spa_usrnbr`
--
ALTER TABLE `spa_usrnbr`
  ADD PRIMARY KEY (`USRNBR_ID`),
  ADD KEY `FK_USRNBR` (`BRANCH_ID`);

--
-- Indexes for table `spa_usrnrule`
--
ALTER TABLE `spa_usrnrule`
  ADD PRIMARY KEY (`USRNRULE_ID`),
  ADD KEY `US_ID` (`US_ID`),
  ADD KEY `RULE_ID` (`RULE_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `spa_agentcy`
--
ALTER TABLE `spa_agentcy`
  MODIFY `AGENT_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `spa_branch`
--
ALTER TABLE `spa_branch`
  MODIFY `BRANCH_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `spa_data`
--
ALTER TABLE `spa_data`
  MODIFY `spa_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `spa_datadetail`
--
ALTER TABLE `spa_datadetail`
  MODIFY `spd_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `spa_employee`
--
ALTER TABLE `spa_employee`
  MODIFY `EMP_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `spa_employee_leave`
--
ALTER TABLE `spa_employee_leave`
  MODIFY `LEAVE_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `spa_floor`
--
ALTER TABLE `spa_floor`
  MODIFY `FLOOR_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `spa_permission`
--
ALTER TABLE `spa_permission`
  MODIFY `MODULE_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `spa_position`
--
ALTER TABLE `spa_position`
  MODIFY `POS_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `spa_regional`
--
ALTER TABLE `spa_regional`
  MODIFY `REGIONAL_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `spa_room`
--
ALTER TABLE `spa_room`
  MODIFY `ROOM_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `spa_room_type`
--
ALTER TABLE `spa_room_type`
  MODIFY `RTYPE_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `spa_rule`
--
ALTER TABLE `spa_rule`
  MODIFY `RULE_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `spa_rulenperm`
--
ALTER TABLE `spa_rulenperm`
  MODIFY `RNPM_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;
--
-- AUTO_INCREMENT for table `spa_service`
--
ALTER TABLE `spa_service`
  MODIFY `SERVICE_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `spa_usrnbr`
--
ALTER TABLE `spa_usrnbr`
  MODIFY `USRNBR_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `spa_usrnrule`
--
ALTER TABLE `spa_usrnrule`
  MODIFY `USRNRULE_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `spa_agentcy`
--
ALTER TABLE `spa_agentcy`
  ADD CONSTRAINT `FK_AGENCY_BRANCH` FOREIGN KEY (`BRANCH_ID`) REFERENCES `spa_branch` (`BRANCH_ID`);

--
-- Constraints for table `spa_booking`
--
ALTER TABLE `spa_booking`
  ADD CONSTRAINT `FK_BOOKING_BRNACH` FOREIGN KEY (`BRANCH_ID`) REFERENCES `spa_branch` (`BRANCH_ID`);

--
-- Constraints for table `spa_checkin`
--
ALTER TABLE `spa_checkin`
  ADD CONSTRAINT `CHECKIN_BOOKING_ID` FOREIGN KEY (`BOOKING_ID`) REFERENCES `spa_booking` (`BOOKING_ID`),
  ADD CONSTRAINT `CHECKIN_ROOM_ID` FOREIGN KEY (`ROOM_ID`) REFERENCES `spa_room` (`ROOM_ID`),
  ADD CONSTRAINT `spa_checkin_therapist` FOREIGN KEY (`THERAPIST`) REFERENCES `spa_employee` (`EMP_ID`);

--
-- Constraints for table `spa_checkin_service`
--
ALTER TABLE `spa_checkin_service`
  ADD CONSTRAINT `CHECKIN_SERVICE_CHECKIN` FOREIGN KEY (`CHECKIN_ID`) REFERENCES `spa_checkin` (`CHECKIN_ID`),
  ADD CONSTRAINT `CHECKIN_SERVICE_ID` FOREIGN KEY (`SERVICE_ID`) REFERENCES `spa_service` (`SERVICE_ID`);

--
-- Constraints for table `spa_checkout`
--
ALTER TABLE `spa_checkout`
  ADD CONSTRAINT `FK_CHECKOUT_CHKIN` FOREIGN KEY (`CHECKIN_ID`) REFERENCES `spa_checkin` (`CHECKIN_ID`) ON UPDATE CASCADE;

--
-- Constraints for table `spa_employee`
--
ALTER TABLE `spa_employee`
  ADD CONSTRAINT `FK_EMP_BRANCH` FOREIGN KEY (`BRANCH_ID`) REFERENCES `spa_branch` (`BRANCH_ID`);

--
-- Constraints for table `spa_floor`
--
ALTER TABLE `spa_floor`
  ADD CONSTRAINT `spa_floor_ibfk_1` FOREIGN KEY (`BRANCH_ID`) REFERENCES `spa_branch` (`BRANCH_ID`);

--
-- Constraints for table `spa_room`
--
ALTER TABLE `spa_room`
  ADD CONSTRAINT `spa_room_ibfk_1` FOREIGN KEY (`FLOOR_ID`) REFERENCES `spa_floor` (`FLOOR_ID`);

--
-- Constraints for table `spa_usrnbr`
--
ALTER TABLE `spa_usrnbr`
  ADD CONSTRAINT `FK_USRNBR` FOREIGN KEY (`BRANCH_ID`) REFERENCES `spa_branch` (`BRANCH_ID`);

--
-- Constraints for table `spa_usrnrule`
--
ALTER TABLE `spa_usrnrule`
  ADD CONSTRAINT `spa_usrnrule_ibfk_2` FOREIGN KEY (`RULE_ID`) REFERENCES `spa_rule` (`RULE_ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
